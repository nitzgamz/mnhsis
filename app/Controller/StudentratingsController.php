<?php
App::uses('AppController', 'Controller');
/**
 * Studentratings Controller
 *
 * @property Studentrating $Studentrating
 */
class StudentratingsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Studentrating->recursive = 0;
		$this->set('studentratings', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Studentrating->id = $id;
		if (!$this->Studentrating->exists()) {
			throw new NotFoundException(__('Invalid studentrating'));
		}
		$this->set('studentrating', $this->Studentrating->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id=null, $grade, $section, $sy) {
		
		if ($this->request->is('post')) {
			
			$studentid = $this->data['Studentrating']['student_id'];
			$ratingid =  $this->data['Studentrating']['rating_id'];
			$quarterid =  $this->data['Studentrating']['quarter'];
			$grade =  $this->data['Studentrating']['gradelevel_id'];
			$section =  $this->data['Studentrating']['gradesection_id'];
			$sy =  $this->data['Studentrating']['sy'];
			
			if($this->dataalreadyexists($id, $ratingid, $quarterid, $grade, $section, $sy)){
				$this->Session->setFlash(__('The same rating already exists.'), 'error_message');
			}else{
				$this->Studentrating->create();
				if ($this->Studentrating->save($this->request->data)) {
					$this->Session->setFlash(__('The rating has been saved'), 'success_message');
					$this->redirect(array('action' => 'add', $id, $grade, $section, $sy));
				} else {
					$this->Session->setFlash(__('The rating could not be saved. Please, try again.'), 'error_message');
				}
			}
		}
		$this->set('student', $this->Studentrating->Student->Studentschoolyear->find(
			'first', array(	
				'conditions' => array(
					'Studentschoolyear.student_id' => $id,
					'Studentschoolyear.gradesection_id' => $section,
					'Studentschoolyear.gradelevel_id' => $grade,
					'Studentschoolyear.sy' => $sy
				)
			)
		));
		
		$ratings = $this->Studentrating->Rating->find('list');
		$gradelevels = $this->Studentrating->Gradelevel->find('list');
		$gradesections = $this->Studentrating->Gradesection->find('list');
		$this->set(compact('ratings', 'gradelevels', 'gradesections'));
	}
	
	public function dataalreadyexists($studentid, $ratingid, $quarterid, $grade, $section, $sy){
		$ratings = $this->Studentrating->find('first', array('conditions' => array(
			'Studentrating.student_id' => $studentid,
			'Studentrating.rating_id' => $ratingid,
			'Studentrating.quarter' => $quarterid,
			'Studentrating.gradelevel_id' => $grade,
			'Studentrating.gradesection_id' => $section,
			'Studentrating.sy' => $sy
		)));
		
		if(!empty($ratings)){
			return true;
		}else{
			return false;
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Studentrating->id = $id;
		if (!$this->Studentrating->exists()) {
			throw new NotFoundException(__('Invalid studentrating'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Studentrating->save($this->request->data)) {
				$this->Session->setFlash(__('The studentrating has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The studentrating could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Studentrating->read(null, $id);
		}
		$students = $this->Studentrating->Student->find('list');
		$ratings = $this->Studentrating->Rating->find('list');
		$gradelevels = $this->Studentrating->Gradelevel->find('list');
		$gradesections = $this->Studentrating->Gradesection->find('list');
		$this->set(compact('students', 'ratings', 'gradelevels', 'gradesections'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Studentrating->id = $id;
		if (!$this->Studentrating->exists()) {
			throw new NotFoundException(__('Invalid studentrating'));
		}
		if ($this->Studentrating->delete()) {
			$this->Session->setFlash(__('Studentrating deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Studentrating was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
