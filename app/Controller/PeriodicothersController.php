<?php
App::uses('AppController', 'Controller');
/**
 * Periodicothers Controller
 *
 * @property Periodicother $Periodicother
 * @property PaginatorComponent $Paginator
 */
class PeriodicothersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Periodicother->recursive = 0;
		$this->set('periodicothers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Periodicother->exists($id)) {
			throw new NotFoundException(__('Invalid periodicother'));
		}
		$options = array('conditions' => array('Periodicother.' . $this->Periodicother->primaryKey => $id));
		$this->set('periodicother', $this->Periodicother->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Periodicother->create();
			if ($this->Periodicother->save($this->request->data)) {
				$this->Session->setFlash(__('The periodicother has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The periodicother could not be saved. Please, try again.'));
			}
		}
		$students = $this->Periodicother->Student->find('list');
		$gradelevels = $this->Periodicother->Gradelevel->find('list');
		$gradesections = $this->Periodicother->Gradesection->find('list');
		$schoolyears = $this->Periodicother->Schoolyear->find('list');
		$teachers = $this->Periodicother->Teacher->find('list');
		$this->set(compact('students', 'gradelevels', 'gradesections', 'schoolyears', 'teachers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Periodicother->exists($id)) {
			throw new NotFoundException(__('Invalid periodicother'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Periodicother->save($this->request->data)) {
				$this->Session->setFlash(__('The periodicother has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The periodicother could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Periodicother.' . $this->Periodicother->primaryKey => $id));
			$this->request->data = $this->Periodicother->find('first', $options);
		}
		$students = $this->Periodicother->Student->find('list');
		$gradelevels = $this->Periodicother->Gradelevel->find('list');
		$gradesections = $this->Periodicother->Gradesection->find('list');
		$schoolyears = $this->Periodicother->Schoolyear->find('list');
		$teachers = $this->Periodicother->Teacher->find('list');
		$this->set(compact('students', 'gradelevels', 'gradesections', 'schoolyears', 'teachers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Periodicother->id = $id;
		if (!$this->Periodicother->exists()) {
			throw new NotFoundException(__('Invalid periodicother'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Periodicother->delete()) {
			$this->Session->setFlash(__('The periodicother has been deleted.'));
		} else {
			$this->Session->setFlash(__('The periodicother could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
