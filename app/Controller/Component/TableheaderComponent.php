<?php
	class TableheaderComponent extends Component{		
		
		public function getHeader($headers =null){
			switch($headers){
				case "Users":
				$headers = array(
					"Account", 
					"Firstname", 
					"Middlename/MI",
					"Lastname",					
					"Department",
					"Last Access",
					"Last IP",
					"Actions"
				);
				break;
				case "Groups":
				$headers = array(
					"Access Name", 
					"Description",
					"Total Users",
					"Actions"
				);
				break;
				case "Department":
				$headers = array(
					"Department", 
					"Shortname",
					"Total Users",
					"Actions",
				);
				break;
				case "Region":
				$headers = array(
					"Region Name",
					"Reional Manager",
					"Email",
					"Contact",
					"Examinations",
					"Applicants",									
					"Actions",
				);
				break;
				case "Regionalmanager":
				$headers = array(
					"Region", "RM Firstname", "RM Lastname", "RM Email", "Contact Number", "Actions"
				);
				break;
				case "Area":
				$headers = array(
					"Region Name", "Area", "Area Manager", "Email", "Contact", "Applicants", "Examinations", "Actions"
				);
				break;
				case "Area Manager":
				$headers = array(
					"Region Name", "Area", "AM Firstname", "AM Lastname", "Email Address", "Contact", "Actions"
				);
				break;
				case "Course Category":
				$headers = array(
					"Name", "Description", "Actions"
				);
				break;
				case "Course":
				$headers = array(
					"Category", "Course Name", "Shortname", "Applicants", "Actions"
				);
				break;
				case "Applicant":
				$headers = array(
					"Fullname", "Region", "Area", "Gender", "Age", "Birthdate", "Course", "Comment", "Action"
				);
				break;
				case "Attempt":
				$headers = array(
					"Started", "Completed", "Time Taken", "Score", "Feedback"
				);
				break;
				case "Examination":
				$headers = array(
					"Status", "Description", "Flag Color", "Actions"
				);
				break;
				case "Examinations":
				$headers = array(
					"Status", "Region", "Area", "Area Manager", "Examination Date", "Banned Until", "Applicants", "Created", "Started By", "Actions"
				);
				break;
				case "Examination Type":
				$headers = array(
					"Type", "Description", "Passing Score", "Percentage", "Failed Score", "Percentage", "Total", "Keyword", "Actions"
				);
				break;
				case "Extract Examination":
				$headers = array(
					"Status", "Fullname", "Started", "Completed", "Time Taken", "Score", "Feedback"
				);
				break;
				case "Extract Examination Personality":
				$headers = array(
					"Status", "Fullname", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12", "S13", "S14", "S15", "F1", "F2", "F3", "F4", "F5"
				);
				
				break;
				case "Personality Scales":
				$headers = array(
					"Name", "Left Description", "Right Description", "Actions"
				);
				break;
				case "Personality Sten Scales":
				$headers = array(
					"Scale", "Rate Score", "Actions"
				);
				break;
			}
			
			return $headers;
		}
		
	}
?>