<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
	
	
	public $components = array();
	
	public function beforeFilter(){
		parent::beforeFilter();
	}
	
	
	public function settings(){
		
	}
	
	public function loggeduser($id=null){
		$id = $this->Auth->user('id');
		$user = $this->User->findById($id);			
		if(isset($this->params['requested'])){
			return $user;
		}
	}
	
	public function login() {
		$this->layout = ('login');
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {											
				$this->redirect(array('controller' => 'students', 'action' => 'homepage'));
			} else {
				$this->Session->setFlash($this->Message->getMessage("login_error"), 'error_message');
			}
		}
	}
	
	public function logout(){
		if($this->Auth->logout()){ $this->redirect(array('action' => 'login')); }
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}
	
	public function myprofile(){
		$userid = $this->Auth->user('id');
		$teacherkey = $this->Auth->user('teacherkey');
		$this->set('user', $this->User->findById($userid));
		$this->User->Teacher->recursive = 2;
		$this->set('teacher', $this->User->Teacher->findByTeacherkey($teacherkey));
	}
	
	
	public function changepassword(){
		$userid = $this->Auth->user('id');
		$this->set('group', $this->Auth->user('group_id'));

		if ($this->request->is('post')) {
			
			$oldpass = $this->data['User']['oldpassword'];
			$newpass = $this->data['User']['newpassword'];
			$confirm = $this->data['User']['conpassword'];
			
					
						if($newpass!==$confirm || empty($oldpass) || empty($newpass) || empty($confirm)){
							$this->Session->setFlash(__('Details do not match. Please, try again.'), 'error_message');
						}else{
							$oldpass = AuthComponent::password($oldpass);
							$exists  = $this->User->findByPassword($oldpass);
							if(!empty($exists)){
								$this->User->id = $userid;
								if($this->User->saveField('password', $newpass)){
									
									$this->Session->setFlash(__('Changes has been saved.'), 'success_message');
									$this->redirect(array('action' => 'changepassword'));
								}else{
									$this->Session->setFlash(__('Details do not match. Please, try again.'), 'error_message');
								}
							}else{
								$this->Session->setFlash(__('Details do not match. Please, try again.'), 'error_message');
							}
						}
		} 
	}
/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
