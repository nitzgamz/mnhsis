<?php
App::uses('AppController', 'Controller');
/**
 * Otherdetails Controller
 *
 * @property Otherdetail $Otherdetail
 */
class OtherdetailsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Otherdetail->recursive = 0;
		$this->set('otherdetails', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Otherdetail->id = $id;
		if (!$this->Otherdetail->exists()) {
			throw new NotFoundException(__('Invalid otherdetail'));
		}
		$this->set('otherdetail', $this->Otherdetail->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id=null, $grade, $section, $sy) {
	
		if ($this->request->is('post')) {
			
			$studentid = $this->data['Otherdetail']['student_id'];		
			$grade =  $this->data['Otherdetail']['gradelevel_id'];
			$section =  $this->data['Otherdetail']['gradesection_id'];
			$sy =  $this->data['Otherdetail']['sy'];
			
			if($this->dataalreadyexists($id, $grade, $section, $sy)){
				$this->Session->setFlash(__('The same information already exists.'), 'error_message');
			}else{
				$this->Otherdetail->create();
				if ($this->Otherdetail->save($this->request->data)) {
					$this->Session->setFlash(__('The detail has been saved'), 'success_message');
					$this->redirect(array('action' => 'add', $id, $grade, $section, $sy));
				} else {
					$this->Session->setFlash(__('The detail could not be saved. Please, try again.'), 'error_message');
				}
			}
		}
		
		$this->set('student', $this->Otherdetail->Student->Studentschoolyear->find(
			'first', array(	
				'conditions' => array(
					'Studentschoolyear.student_id' => $id,
					'Studentschoolyear.gradesection_id' => $section,
					'Studentschoolyear.gradelevel_id' => $grade,
					'Studentschoolyear.sy' => $sy
				)
			)
		));
		
		
		$students = $this->Otherdetail->Student->find('list');
		$gradelevels = $this->Otherdetail->Gradelevel->find('list');
		$gradesections = $this->Otherdetail->Gradesection->find('list');
		$this->set(compact('students', 'gradelevels', 'gradesections'));
	}
	
	
	public function dataalreadyexists($studentid, $grade, $section, $sy){
		$ratings = $this->Otherdetail->find('first', array('conditions' => array(
			'Otherdetail.student_id' => $studentid,		
			'Otherdetail.gradelevel_id' => $grade,
			'Otherdetail.gradesection_id' => $section,
			'Otherdetail.sy' => $sy
		)));
		
		if(!empty($ratings)){
			return true;
		}else{
			return false;
		}
	}
	
/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Otherdetail->id = $id;
		if (!$this->Otherdetail->exists()) {
			throw new NotFoundException(__('Invalid otherdetail'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Otherdetail->save($this->request->data)) {
				$this->Session->setFlash(__('The otherdetail has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The otherdetail could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Otherdetail->read(null, $id);
		}
		$students = $this->Otherdetail->Student->find('list');
		$gradelevels = $this->Otherdetail->Gradelevel->find('list');
		$gradesections = $this->Otherdetail->Gradesection->find('list');
		$this->set(compact('students', 'gradelevels', 'gradesections'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Otherdetail->id = $id;
		if (!$this->Otherdetail->exists()) {
			throw new NotFoundException(__('Invalid otherdetail'));
		}
		if ($this->Otherdetail->delete()) {
			$this->Session->setFlash(__('Otherdetail deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Otherdetail was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
