<?php
App::uses('AppController', 'Controller');
/**
 * Teachers Controller
 *
 * @property Teacher $Teacher
 */
class TeachersController extends AppController {

	
	public function getTeacherGradelevel($type=null){

			$teacherkey = $this->Auth->user('teacherkey');
			$teacher  = $this->Teacher->findByTeacherkey($teacherkey);
		
		switch($type){
		case "grade":
			$details =  $this->Teacher->Gradelevel->find('list', array('conditions' => array('Gradelevel.id' => $teacher['Teacher']['gradelevel_id'])));
			
		break;
		case "section":
			$details =  $this->Teacher->Gradesection->find('list', array('conditions' => array('Gradesection.id' => $teacher['Teacher']['gradesection_id'])));
		break;
		}
		
		return $details;
	}
	

	public function teachername($sectionid){
		$teacher = $this->Teacher->find('first', array('conditions' => array('Teacher.gradesection_id' => $sectionid)));
		return $teacher['Teacher']['title'].' '.$teacher['Teacher']['firstname'].' '.$teacher['Teacher']['lastname'];
	}
	
	public function getallteachers($id){
		$teachers = $this->Teacher->find('all', array('conditions' => array('Teacher.gradelevel_id' => $id)));
		return $teachers;
	}
	
	
	public function listofteachers(){
		if(isset($this->request->data['Student']['gradesection_id']) || !empty($this->request->data['Schoolgrade']['gradesection_id'])){
			$id = $this->request->data['Student']['gradesection_id'];
			$this->set('teachers', $this->Teacher->find('all', array('conditions' => array('Teacher.gradesection_id' => $id), 'order' => array('Teacher.name' => 'ASC'))));			
		}
	}
	
	public function getallteachersinsections($id){
		$teachers = $this->Teacher->find('all', array('conditions' => array('Teacher.gradesection_id' => $id)));
		return $teachers;
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Teacher->recursive = 0;
		
		if($this->request->is('post') || $this->request->is('put')){
			
			if(!empty($this->data['Search']['firstname']) || !empty($this->data['Search']['lastname'])){
			
				$firstname = $this->data['Search']['firstname'];						
				$lastname = $this->data['Search']['lastname'];									
				
				if(!empty($firstname) && !empty($lastname)){
						$this->paginate = array('conditions' => array(
							'OR' => array(
								'Teacher.firstname LIKE' => "%$firstname%",
								'Teacher.lastname LIKE' => "%$lastname%"							
							)
						));
				}else{
					if(!empty($firstname)){
						$this->paginate = array('conditions' => array(
							'OR' => array(
								'Teacher.firstname LIKE' => "%$firstname%",							
							)
						));
					}
					
					if(!empty($lastname)){
						$this->paginate = array('conditions' => array(
							'OR' => array(
								'Teacher.lastname LIKE' => "%$lastname%"							
							)
						));
					}
				}
				
				
			}else{
				$this->Session->setFlash(__('Invalid keyword'), 'error_message');
				$this->paginate = array('order' => array('Teacher.lastname' => 'ASC'));
			
			}
		}else{
			$this->paginate = array('order' => array('Teacher.lastname' => 'ASC'));
			
		}
		
		$this->set('teachers', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Teacher->id = $id;
		if (!$this->Teacher->exists()) {
			throw new NotFoundException(__('Invalid teacher'));
		}
		$this->set('teacher', $this->Teacher->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Teacher->create();
			if ($this->Teacher->save($this->request->data)) {
				
				$data = array(
					'User' => array(
						'group_id' 	 => 2,
						'teacherkey' => $this->data['Teacher']['teacherkey'],
						'username'	 => $this->data['Teacher']['idnumber'],
						'password'	 => $this->data['Teacher']['idnumber']
					)
				);
				
				$this->Teacher->User->create();
				$this->Teacher->User->save($data);
				
				$this->Session->setFlash(__('The teacher has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
				
			} else {
				$this->Session->setFlash(__('The teacher could not be saved. Please, try again.'), 'error_message');
			}
		}
		
		$gradelevels = $this->Teacher->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		//$gradesections = $this->Teacher->Gradesection->find('list', array('order' => array('Gradesection.name' => 'ASC')));
		$gradesections ='';
		$this->set(compact('gradelevels', 'gradesections'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Teacher->id = $id;
		if (!$this->Teacher->exists()) {
			throw new NotFoundException(__('Invalid teacher'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Teacher->save($this->request->data)) {
				$this->Session->setFlash(__('The teacher has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The teacher could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$this->request->data = $this->Teacher->read(null, $id);
		}
		
		$gradelevels = $this->Teacher->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$gradesections = $this->Teacher->Gradesection->find('list', array('order' => array('Gradesection.name' => 'ASC')));
		$this->set(compact('gradelevels', 'gradesections'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Teacher->id = $id;
		if (!$this->Teacher->exists()) {
			throw new NotFoundException(__('Invalid teacher'));
		}
		if ($this->Teacher->delete()) {
			$this->Session->setFlash(__('Teacher deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Teacher was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
