<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'phpexcel/index');

/**
 * Attendances Controller
 *
 * @property Attendance $Attendance
 * @property PaginatorComponent $Paginator
 */
class AttendancesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	
	public function get_report_card($student=null, $sy=null, $teacher=null){
		$details = $this->Attendance->find('count', array(
				'conditions' => array(
					'Attendance.student_id' 	=> $student,
					'Attendance.gradelevel_id'  => $sy
					//'Attendance.schoolyear_id'  => $sy,
					//'Attendance.teacher_id' 	=> $teacher
		)));
		
		return $details;
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Attendance->Student->recursive = -2;
		
		$teacherkey = $this->Auth->user('teacherkey');
		$teacher  = $this->Attendance->Teacher->findByTeacherkey($teacherkey);
			
		$this->set('attendances', $this->Attendance->Student->find('all', array(
			'conditions' => array('Student.teacher_id' => $teacher['Teacher']['id']),
			'order'  => array(
				'Student.lastname' => 'ASC'
			)
		)));
		
		$this->set('sy', $this->Attendance->Schoolyear->find('list', array('order' => array('Schoolyear.sy_from' => 'DESC'))));
	}

	public function download_report(){
		
		$this->loadModel('Setting');
		$setting = $this->Setting->find('first');
		
		$region 	= $setting['Setting']['region'];
		$division 	= $setting['Setting']['division'];
		$district 	= $setting['Setting']['district'];
		$school 	= $setting['Setting']['school_name'];
		$principal 	= $setting['Setting']['principal'];
		$nmos = array();
		$newarr = array();
		$natts = array();
		$matts = array();
		
		if($this->RequestHandler->isAjax()){
			$this->layout = 'ajax';
			$this->autoRender = false;
			
			$sy 		= $this->data['Attendance']['sy'];
			$student 	= $this->data['Attendance']['studentid'];
			
			$sinfo 		= $this->Attendance->Student->findById($student);
			$syinfo 	= $this->Attendance->Schoolyear->findById($sy);
			$atts 		= $this->Attendance->find('all', array(
					'conditions' => array(
						'Attendance.student_id' 	=> $student,
						'Attendance.schoolyear_id' 	=> $sy
					),
					'order'	=> array('Attendance.id' => 'ASC')
			));
			foreach($atts as $mo):
				$natts[] = $mo['Attendance']['present'];
				$matts[] = $mo['Attendance']['absent'];
			endforeach;
			
			//$this->Attendance->Mo->recursive = -2;
			$mos	= $this->Attendance->Mo->find('all', array('conditions' => array('Mo.schoolyear_id' => $sy)));
			foreach($mos as $mo):
				$nmos[] = $mo['Mo']['sdays'];
			endforeach;

			
			$gender = ($sinfo['Student']['gender']=="M" ? "Male" : "Female");
			$age 	=  floor((time() - strtotime('Y-m-d', $sinfo['Student']['birthdate'])) / 31556926);
			
			$directory  = APP.'webroot/files/';
			$objPHPExcel = new PHPExcel();
			
			$fileType = 'Excel5';
			$fileName = $directory . 'form-138.xls';

			// Read the file
			$objReader = PHPExcel_IOFactory::createReader($fileType);
			$objPHPExcel = $objReader->load($fileName);

			// Change the file
			$objPHPExcel->setActiveSheetIndex(1)
						->setCellValue('O5', $region)
						->setCellValue('O7', $division)
						->setCellValue('O9', $district)
						->setCellValue('N11', $school)
						->setCellValue('N25', strtoupper($principal))
						->setCellValue('O13', strtoupper($sinfo['Student']['full_name']))  
						->setCellValue('O14', $age)  
						->setCellValue('S14', $gender)  
						->setCellValue('O15', $sinfo['Gradelevel']['name'])  
						->setCellValue('S15', $sinfo['Gradesection']['name'])  
						->setCellValue('Q16', $syinfo['Schoolyear']['sy_from'].' - '.$syinfo['Schoolyear']['sy_to']) 
						->setCellValue('S24', strtoupper($sinfo['Teacher']['firstname'].' '.$sinfo['Teacher']['lastname']))
						->setCellValue('B5', $nmos[0])
						->setCellValue('C5', $nmos[1])
						->setCellValue('D5', $nmos[2])
						->setCellValue('E5', $nmos[2])
						->setCellValue('F5', $nmos[4])
						->setCellValue('G5', $nmos[5])
						->setCellValue('H5', $nmos[6])
						->setCellValue('I5', $nmos[7])
						->setCellValue('J5', $nmos[8])
						->setCellValue('K5', $nmos[9])
						->setCellValue('B8', $natts[0])
						->setCellValue('C8', $natts[1])
						->setCellValue('D8', $natts[2])
						->setCellValue('E8', $natts[2])
						->setCellValue('F8', $natts[4])
						->setCellValue('G8', $natts[5])
						->setCellValue('H8', $natts[6])
						->setCellValue('I8', $natts[7])
						->setCellValue('J8', $natts[8])
						->setCellValue('K8', $natts[9])
						->setCellValue('B11', $matts[0])
						->setCellValue('C11', $matts[1])
						->setCellValue('D11', $matts[2])
						->setCellValue('E11', $matts[2])
						->setCellValue('F11', $matts[4])
						->setCellValue('G11', $matts[5])
						->setCellValue('H11', $matts[6])
						->setCellValue('I11', $matts[7])
						->setCellValue('J11', $matts[8])
						->setCellValue('K11', $matts[9]);
	
			
			
			// Write the file
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
			$objWriter->save($fileName);
		}

	
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Attendance->exists($id)) {
			throw new NotFoundException(__('Invalid attendance'));
		}
		$options = array('conditions' => array('Attendance.' . $this->Attendance->primaryKey => $id));
		$this->set('attendance', $this->Attendance->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($student=null, $sy=null) {
		$userid = $this->Auth->user('id');
		$group	= $this->Auth->user('group_id');
		
		if($group > 1){
			$teacherkey = $this->Auth->user('teacherkey');
			$teacher 	= $this->Attendance->Teacher->findByTeacherkey($teacherkey);
		}else{
			$st 		= $this->Attendance->Student->findById($student);
			$teacher 	= $this->Attendance->Teacher->findById($st['Student']['teacher_id']);
		}
		
		$tid = $teacher['Teacher']['id'];
		$saved = 0 ;
		
		if ($this->request->is('post')) {
			
			foreach($this->data['Attendance']['mo_id'] as $key => $index):
				$data[] = array(
					'Attendance' => array(
						'user_id'		=> $this->data['Attendance']['user_id'],
						'student_id'	=> $this->data['Attendance']['student_id'],
						'gradelevel_id'	=> $this->data['Attendance']['gradelevel_id'],
						'gradesection_id'	=> $this->data['Attendance']['gradesection_id'],
						'teacher_id'	=> $this->data['Attendance']['teacher_id'],
						'schoolyear_id'	=> $this->data['Attendance']['schoolyear_id'],
						'mo_id'			=> $this->data['Attendance']['mo_id'][$key],
						'present'		=> ( $this->data['Attendance']['present'][$key] - $this->data['Attendance']['absent'][$key]),
						'absent'		=> $this->data['Attendance']['absent'][$key],
						'added'			=> date('Y-m-d')
					)
				);
			endforeach;
			
			$this->Attendance->create();
			if($this->Attendance->saveAll($data)){
				$this->Session->setFlash(__('The attendance has been saved.'), 'success_message');
				return $this->redirect(array('controller' => 'students', 'action' => 'teacher_students', 0, 0, 0, $this->data['Attendance']['student_id']));
			} else {
				$this->Session->setFlash(__('The attendance could not be saved. Please, try again.'), 'error_message');
				return $this->redirect(array('controller' => 'students', 'action' => 'teacher_students', 0, 0, 0, $this->data['Attendance']['student_id']));
			}
		}
		
		$this->Attendance->Schoolyear->recursive = -2;
		$this->Attendance->User->recursive = -2;
		$this->Attendance->Teacher->recursive = -2;
		
		$users = $this->Attendance->User->find('first', array('conditions' => array('User.id' => $userid), 'fields' => array('User.id')));
		
		if(!empty($student)){
			$students = $this->Attendance->Student->find('list', array(
				'conditions' => array(
					'Student.teacher_id' => $tid,
					'Student.id'		=> $student
				)
			));
		}else{
			$students = $this->Attendance->Student->find('list', array('conditions' => array('Student.teacher_id' => $tid)));
		}
		
		$teachers = $this->Attendance->Teacher->find('first', array('conditions' => array('Teacher.id' => $tid), 'fields' => array('Teacher.id')));
		
		$this->set('glevel', $this->Attendance->Gradelevel->findById($teacher['Teacher']['gradelevel_id']));
		$gradelevels = $this->Attendance->Gradelevel->find('list', array('conditions' => array('Gradelevel.id' => $teacher['Teacher']['gradelevel_id'])));
		$gradesections = $this->Attendance->Gradesection->find('list', array('conditions' => array('Gradesection.id' => $teacher['Teacher']['gradesection_id'])));
		
		$schoolyears = $this->Attendance->Schoolyear->find('first', array(
			'conditions' => array(
				'OR' => array(
					'Schoolyear.sy_from' => date('Y'),
					'Schoolyear.sy_to' => date('Y')
				)
			),
			'fields' => array('Schoolyear.id, Schoolyear.sy_from, Schoolyear.sy_to')
			));
		$this->set('schoolyears', $schoolyears);
		$mos = $this->Attendance->Mo->find('all', array(
			'conditions' => array(
					'schoolyear_id' => $schoolyears['Schoolyear']['id']
			)));
		$this->set(compact('users', 'students', 'teachers', 'mos', 'gradelevels', 'gradesections'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($student=null, $gradelevel=null) {
		$saved = 0;
		
		if ($this->request->is('post') || $this->request->is('put')) {
			
			foreach($this->data['Attendance']['id'] as $key => $index):
				$data[] = array(
					'Attendance'		 => array(
						'id'			=> $this->data['Attendance']['id'][$key],
						'present'		=> ( $this->data['Attendance']['present'][$key] - $this->data['Attendance']['absent'][$key]),
						'absent'		=> $this->data['Attendance']['absent'][$key]
					)
				);
			endforeach;
			
			if($this->Attendance->saveMany($data, array('deep' => true))){
				$this->Session->setFlash(__('Changes has been saved.'), 'success_message');
				return $this->redirect(array('controller' => 'students', 'action' => 'teacher_students', 0, 0, 0, $student));
			}else{
				$this->Session->setFlash(__('The changes could not be saved. Please, try again.'), 'error_message');
				return $this->redirect(array('controller' => 'students', 'action' => 'teacher_students', 0, 0, 0, $student));
			}
		} else {
			$options = array('conditions' => array('Attendance.student_id' => $student, 'Attendance.gradelevel_id' => $gradelevel));
			$this->set('datas', $this->Attendance->find('all', $options));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Attendance->id = $id;
		if (!$this->Attendance->exists()) {
			throw new NotFoundException(__('Invalid attendance'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Attendance->delete()) {
			$this->Session->setFlash(__('The attendance has been deleted.'));
		} else {
			$this->Session->setFlash(__('The attendance could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
