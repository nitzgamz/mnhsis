<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'phpexcel/index');


/**
 * Periodicratings Controller
 *
 * @property Periodicrating $Periodicrating
 * @property PaginatorComponent $Paginator
 */
class PeriodicratingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

		
	public function get_report_card($student=null, $sy=null, $teacher=null){
		$details = $this->Periodicrating->find('count', array(
				'conditions' => array(
					'Periodicrating.student_id' => $student,
					'Periodicrating.gradelevel_id' => $sy
					//'Periodicrating.schoolyear_id' => $sy,
					//'Periodicrating.teacher_id' => $teacher
		)));
		
		return $details;
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Periodicrating->Student->recursive = -2;
		
		$teacherkey = $this->Auth->user('teacherkey');
		$teacher  = $this->Periodicrating->Teacher->findByTeacherkey($teacherkey);
			
		$this->set('attendances', $this->Periodicrating->Student->find('all', array(
			'conditions' => array('Student.teacher_id' => $teacher['Teacher']['id']),
			'order'  => array(
				'Student.lastname' => 'ASC'
			)
		)));
		
		$this->set('sy', $this->Periodicrating->Schoolyear->find('list', array('order' => array('Schoolyear.sy_from' => 'DESC'))));
	}
	
	public function download_report(){
		
			
		$this->loadModel('Setting');
		$setting = $this->Setting->find('first');
		
		$region 	= $setting['Setting']['region'];
		$division 	= $setting['Setting']['division'];
		$district 	= $setting['Setting']['district'];
		$school 	= $setting['Setting']['school_name'];
		$principal 	= $setting['Setting']['principal'];
		$nmos = array();
		$newarr = array();
		$natts = array();
		$matts = array();
		$fst_qtr = array();
		$snd_qtr = array();
		$trd_qtr = array();
		$fth_qtr = array();
		$rating = array();
		$action = array();
		$earned = array();
		
		if($this->RequestHandler->isAjax()){
			
			$this->layout = 'ajax';
			$this->autoRender = false;
		
			//$sy 		= $this->data['Periodicrating']['sy'];
			$student 	= $this->data['Periodicrating']['studentid'];
			
			
			$sinfo 		= $this->Periodicrating->Student->findById($student);
			$syinfo 	= $this->Periodicrating->Schoolyear->findById($sy);
			
			$atts_grade7 	= $this->Periodicrating->find('all', array(
					'conditions' => array(
						'Periodicrating.student_id' 	=> $student,
						'Periodicrating.gradelevel_id' 	=> 1
						//'Periodicrating.schoolyear_id' 	=> $sy
					)
			));
			
			$atts_grade8 		= $this->Periodicrating->find('all', array(
					'conditions' => array(
						'Periodicrating.student_id' 	=> $student,
						'Periodicrating.gradelevel_id' 	=> 3
						//'Periodicrating.schoolyear_id' 	=> $sy
					)
			));
			
			$atts_grade9 		= $this->Periodicrating->find('all', array(
					'conditions' => array(
						'Periodicrating.student_id' 	=> $student,
						'Periodicrating.gradelevel_id' 	=> 4
						//'Periodicrating.schoolyear_id' 	=> $sy
					)
			));
			
			$atts_grade10 		= $this->Periodicrating->find('all', array(
					'conditions' => array(
						'Periodicrating.student_id' 	=> $student,
						'Periodicrating.gradelevel_id' 	=> 5
						//'Periodicrating.schoolyear_id' 	=> $sy
					)
			));
			
			if(!empty($atts_grade7)){
				foreach($atts_grade7 as $mo):
					$fst_qtr7[] 		= 	$mo['Periodicrating']['1st_qtr'];
					$snd_qtr7[] 		= 	$mo['Periodicrating']['2nd_qtr'];
					$trd_qtr7[] 		= 	$mo['Periodicrating']['3rd_qtr'];
					$fth_qtr7[] 		= 	$mo['Periodicrating']['4th_qtr'];
					$rating7[] 			= 	$mo['Periodicrating']['rating'];
					$action7[] 			= 	$mo['Periodicrating']['action'];
					$earned7[] 			= 	$mo['Periodicrating']['earned'];
					$grade7[] 			= 	$mo['Gradelevel']['name'];
					$section7[] 		= 	$mo['Gradesection']['name'];
					$teacher7[] 		= 	$mo['Teacher']['firstname'].' '.substr($mo['Teacher']['middlename'], 0, 1).' '.$mo['Teacher']['lastname'];
					$sy7[] 				= 	$mo['Schoolyear']['sy_from'].' - '.$mo['Schoolyear']['sy_to'];
				endforeach;
				
				$attss_grade7 	= $this->Periodicrating->Student->Attendance->find('all', array(
						'conditions' => array(
							'Attendance.student_id' 	=> $student,
							'Attendance.gradelevel_id' 	=> 1
						),
						'order'	=> array('Attendance.id' => 'ASC')
				));
				
				foreach($attss_grade7 as $mo):
					$natts7[] 		= $mo['Attendance']['present'];
					//$matts[] = $mo['Attendance']['sdays'];
				endforeach;
				
			}else{
				$atts_grade7 ='';
			}
			
			if(!empty($atts_grade8)){
				foreach($atts_grade8 as $mo):
					$fst_qtr8[] 		= 	$mo['Periodicrating']['1st_qtr'];
					$snd_qtr8[] 		= 	$mo['Periodicrating']['2nd_qtr'];
					$trd_qtr8[] 		= 	$mo['Periodicrating']['3rd_qtr'];
					$fth_qtr8[] 		= 	$mo['Periodicrating']['4th_qtr'];
					$rating8[] 			= 	$mo['Periodicrating']['rating'];
					$action8[] 			= 	$mo['Periodicrating']['action'];
					$earned8[] 			= 	$mo['Periodicrating']['earned'];
					$grade8[] 			= 	$mo['Gradelevel']['name'];
					$section8[] 		= 	$mo['Gradesection']['name'];
					$teacher8[] 		= 	$mo['Teacher']['firstname'].' '.substr($mo['Teacher']['middlename'], 0, 1).' '.$mo['Teacher']['lastname'];
					$sy8[] 				= 	$mo['Schoolyear']['sy_from'].' - '.$mo['Schoolyear']['sy_to'];
				endforeach;
				
				
				$attss_grade8 	= $this->Periodicrating->Student->Attendance->find('all', array(
						'conditions' => array(
							'Attendance.student_id' 	=> $student,
							'Attendance.gradelevel_id' 	=> 3
						),
						'order'	=> array('Attendance.id' => 'ASC')
				));
				
				
				foreach($attss_grade8 as $mo):
					$natts8[] 		= $mo['Attendance']['present'];
				endforeach;
				
			}else{
				$atts_grade8 = '';
			}
			
			
			if(!empty($atts_grade9)){
				foreach($atts_grade9 as $mo):
					$fst_qtr9[] 		= 	$mo['Periodicrating']['1st_qtr'];
					$snd_qtr9[] 		= 	$mo['Periodicrating']['2nd_qtr'];
					$trd_qtr9[] 		= 	$mo['Periodicrating']['3rd_qtr'];
					$fth_qtr9[] 		= 	$mo['Periodicrating']['4th_qtr'];
					$rating9[] 			= 	$mo['Periodicrating']['rating'];
					$action9[] 			= 	$mo['Periodicrating']['action'];
					$earned9[] 			= 	$mo['Periodicrating']['earned'];
					$grade9[] 			= 	$mo['Gradelevel']['name'];
					$section9[] 		= 	$mo['Gradesection']['name'];
					$teacher9[] 		= 	$mo['Teacher']['firstname'].' '.substr($mo['Teacher']['middlename'], 0, 1).' '.$mo['Teacher']['lastname'];
					$sy9[] 				= 	$mo['Schoolyear']['sy_from'].' - '.$mo['Schoolyear']['sy_to'];
				endforeach;
				
				$attss_grade9 	= $this->Periodicrating->Student->Attendance->find('all', array(
						'conditions' => array(
							'Attendance.student_id' 	=> $student,
							'Attendance.gradelevel_id' 	=> 4
						),
						'order'	=> array('Attendance.id' => 'ASC')
				));
				
				foreach($attss_grade9 as $mo):
					$natts9[] 		= $mo['Attendance']['present'];
				endforeach;
				
			}else{
				$atts_grade9 = '';
			}
			
			if(!empty($atts_grade10)){
				foreach($atts_grade10 as $mo):
					$fst_qtr10[] 		= 	$mo['Periodicrating']['1st_qtr'];
					$snd_qtr10[] 		= 	$mo['Periodicrating']['2nd_qtr'];
					$trd_qtr10[] 		= 	$mo['Periodicrating']['3rd_qtr'];
					$fth_qtr10[] 		= 	$mo['Periodicrating']['4th_qtr'];
					$rating10[] 			= 	$mo['Periodicrating']['rating'];
					$action10[] 			= 	$mo['Periodicrating']['action'];
					$earned10[] 			= 	$mo['Periodicrating']['earned'];
					$grade10[] 			= 	$mo['Gradelevel']['name'];
					$section10[] 		= 	$mo['Gradesection']['name'];
					$teacher10[] 		= 	$mo['Teacher']['firstname'].' '.substr($mo['Teacher']['middlename'], 0, 1).' '.$mo['Teacher']['lastname'];
					$sy10[] 				= 	$mo['Schoolyear']['sy_from'].' - '.$mo['Schoolyear']['sy_to'];
				endforeach;
				
				$attss_grade10 	= $this->Periodicrating->Student->Attendance->find('all', array(
						'conditions' => array(
							'Attendance.student_id' 	=> $student,
							'Attendance.gradelevel_id' 	=> 5
						),
						'order'	=> array('Attendance.id' => 'ASC')
				));
				
					
				foreach($attss_grade10 as $mo):
					$natts10[] 		= $mo['Attendance']['present'];
				endforeach;
				
			}else{
				$atts_grade10 = '';
			}
			
			
			
			
			//$mos	= $this->Periodicrating->Student->Attendance->Mo->find('all', array('conditions' => array('Mo.schoolyear_id' => $sy)));
			$mos	= $this->Periodicrating->Student->Attendance->Mo->find('all');
			foreach($mos as $mo):
				$nmos[] = $mo['Mo']['sdays'];
			endforeach;
			
			
			$gender 	= ($sinfo['Student']['gender']=="M" ? "Male" : "Female");
			$age 		=  floor((time() - strtotime('Y-m-d', $sinfo['Student']['birthdate'])) / 31556926);
			
			$middlename = substr($sinfo['Student']['middlename'], 0, 1);
			
			$dob_y 		= date('Y', strtotime($sinfo['Student']['birthdate']));
			$dob_m 		= date('M', strtotime($sinfo['Student']['birthdate']));
			$dob_d 		= date('d', strtotime($sinfo['Student']['birthdate']));
			$pob		= $sinfo['Student']['birthplace'];
			$province	= $sinfo['Province']['name'];
			$city		= $sinfo['City']['name'];
			$brgy		= $sinfo['Barangay']['name'];
			$guardian	= !empty($sinfo['Student']['mother_fullname']) ? $sinfo['Student']['mother_fullname'] : $sinfo['Student']['guardian_fullname'];
			$address 	= $sinfo['Student']['guardian_address'];
			$occupation = $sinfo['Student']['guardian_occupation'];
			$lrn 		= $sinfo['Student']['lrn'];
			$firstname 	= $sinfo['Student']['firstname'];
			$lastname 	= $sinfo['Student']['lastname'];
			$grade 		= $sinfo['Gradelevel']['name'];
			$section 	= $sinfo['Gradesection']['name'];
			$shy		= $syinfo['Schoolyear']['sy_from'].' - '.$syinfo['Schoolyear']['sy_to'];
			$teacher	= $sinfo['Teacher']['firstname'].' '.substr($sinfo['Teacher']['middlename'], 0, 1).' '.$sinfo['Teacher']['lastname'];
			$a = 31;
			$b = 31;
			$c = 65;
			$d = 65;
			
			$directory  = APP.'webroot/files/';
			$objPHPExcel = new PHPExcel();
			
			$fileType = 'Excel5';
			$fileName = $directory . 'form-137.xls';

			// Read the file
			$objReader = PHPExcel_IOFactory::createReader($fileType);
			$objPHPExcel = $objReader->load($fileName);

			// Change the file
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A7', $lrn)
						->setCellValue('B10', $lastname)
						->setCellValue('I10', $firstname)
						->setCellValue('Q10', $middlename)
						->setCellValue('W10', $dob_y)
						->setCellValue('AA10', $dob_m)
						->setCellValue('AD10', $dob_d)
						->setCellValue('C12', $province)
						->setCellValue('Q12', $city)
						->setCellValue('Y12', $brgy)
						->setCellValue('D14', $guardian)
						->setCellValue('Q14', $address)
						->setCellValue('AA14', $occupation)
						->setCellValue('K16', $school);
						
						
						
			
			/*================
			| Grade 7
			=================*/
			
			if(!empty($atts_grade7)){
						/*==============
						| Grades
						==============*/
					  foreach($fst_qtr7 as $key => $qtr):
							$a++;
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$a, $qtr);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$a, $snd_qtr7[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$a, $trd_qtr7[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$a, $fth_qtr7[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$a, $rating7[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$a, $action7[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$a, $earned7[$key]);
						endforeach;
						
						/*============
						| Attendance
						=============*/
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C51', $nmos[0])
													->setCellValue('E51', $nmos[1])
													->setCellValue('F51', $nmos[2])
													->setCellValue('G51', $nmos[3])
													->setCellValue('H51', $nmos[4])
													->setCellValue('I51', $nmos[5])
													->setCellValue('J51', $nmos[6])
													->setCellValue('K51', $nmos[7])
													->setCellValue('L51', $nmos[8])
													->setCellValue('M51', $nmos[9])
													->setCellValue('C52', $natts7[0])
													->setCellValue('E52', $natts7[1])
													->setCellValue('F52', $natts7[2])
													->setCellValue('G52', $natts7[3])
													->setCellValue('H52', $natts7[4])
													->setCellValue('I52', $natts7[5])
													->setCellValue('J52', $natts7[6])
													->setCellValue('K52', $natts7[7])
													->setCellValue('L52', $natts7[8])
													->setCellValue('M52', $natts7[9])
													->setCellValue('B23', $school)
													->setCellValue('B24', $grade7[0])
													->setCellValue('H24', $section7[0])
													->setCellValue('K26', $sy7[0])
													->setCellValue('L55', $teacher7[0]);
			}
			
			
			/*================
			| Grade 8
			=================*/
			
			if(!empty($atts_grade8)){
					foreach($fst_qtr8 as $key => $qtr):
							$b++;
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$b, $qtr);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$b, $snd_qtr8[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$b, $trd_qtr8[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$b, $fth_qtr8[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$b, $rating8[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$b, $action8[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$b, $earned8[$key]);
						endforeach;
						
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S51', $nmos[0])
													->setCellValue('U51', $nmos[1])
													->setCellValue('V51', $nmos[2])
													->setCellValue('W51', $nmos[3])
													->setCellValue('X51', $nmos[4])
													->setCellValue('Y51', $nmos[5])
													->setCellValue('Z51', $nmos[6])
													->setCellValue('AA51', $nmos[7])
													->setCellValue('AB51', $nmos[8])
													->setCellValue('AC51', $nmos[9])
													->setCellValue('S52', $natts8[0])
													->setCellValue('U52', $natts8[1])
													->setCellValue('V52', $natts8[2])
													->setCellValue('W52', $natts8[3])
													->setCellValue('X52', $natts8[4])
													->setCellValue('Y52', $natts8[5])
													->setCellValue('Z52', $natts8[6])
													->setCellValue('AA52', $natts8[7])
													->setCellValue('AB52', $natts8[8])
													->setCellValue('AC52', $natts8[9])
													->setCellValue('R23', $school)
													->setCellValue('R24', $grade8[0])
													->setCellValue('X24', $section8[0])
													->setCellValue('AA26', $sy8[0])
													->setCellValue('AB55', $teacher8[0]);
			}
			
			/*================
			| Grade 9
			=================*/
			
			if(!empty($atts_grade9)){
					foreach($fst_qtr9 as $key => $qtr):
							$c++;
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$c, $qtr);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$c, $snd_qtr9[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$c, $trd_qtr9[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$c, $fth_qtr9[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$c, $rating9[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$c, $action9[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$c, $earned9[$key]);
						endforeach;
						
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C85', $nmos[0])
													->setCellValue('E85', $nmos[1])
													->setCellValue('F85', $nmos[2])
													->setCellValue('G85', $nmos[3])
													->setCellValue('H85', $nmos[4])
													->setCellValue('I85', $nmos[5])
													->setCellValue('J85', $nmos[6])
													->setCellValue('K85', $nmos[7])
													->setCellValue('L85', $nmos[8])
													->setCellValue('M85', $nmos[9])
													->setCellValue('C86', $natts9[0])
													->setCellValue('E86', $natts9[1])
													->setCellValue('F86', $natts9[2])
													->setCellValue('G86', $natts9[3])
													->setCellValue('H86', $natts9[4])
													->setCellValue('I86', $natts9[5])
													->setCellValue('J86', $natts9[6])
													->setCellValue('K86', $natts9[7])
													->setCellValue('L86', $natts9[8])
													->setCellValue('M86', $natts9[9])
													->setCellValue('B57', $school)
													->setCellValue('B58', $grade9[0])
													->setCellValue('H58', $section9[0])
													->setCellValue('K60', $sy9[0])
													->setCellValue('L89', $teacher9[0]);
			}
			
			
			/*================
			| Grade 10
			=================*/
			
			if(!empty($atts_grade10)){
					foreach($fst_qtr10 as $key => $qtr):
							$d++;
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$d, $qtr);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$d, $snd_qtr10[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$d, $trd_qtr10[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$d, $fth_qtr10[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$d, $rating10[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$d, $action10[$key]);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$d, $earned10[$key]);
						endforeach;
						
						
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S85', $nmos[0])
													->setCellValue('U85', $nmos[1])
													->setCellValue('V85', $nmos[2])
													->setCellValue('W85', $nmos[3])
													->setCellValue('X85', $nmos[4])
													->setCellValue('Y85', $nmos[5])
													->setCellValue('Z85', $nmos[6])
													->setCellValue('AA85', $nmos[7])
													->setCellValue('AB85', $nmos[8])
													->setCellValue('AC85', $nmos[9])
													->setCellValue('S86', $natts10[0])
													->setCellValue('U86', $natts10[1])
													->setCellValue('V86', $natts10[2])
													->setCellValue('W86', $natts10[3])
													->setCellValue('X86', $natts10[4])
													->setCellValue('Y86', $natts10[5])
													->setCellValue('Z86', $natts10[6])
													->setCellValue('AA86', $natts10[7])
													->setCellValue('AB86', $natts10[8])
													->setCellValue('AC86', $natts10[9])
													->setCellValue('R57', $school)
													->setCellValue('R58', $grade10[0])
													->setCellValue('X58', $section10[0])
													->setCellValue('AA60', $sy10[0])
													->setCellValue('AB89', $teacher10[0]);
			}
			
			
			
			// Write the file
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
			$objWriter->save($fileName);
		}

	
	}
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Periodicrating->exists($id)) {
			throw new NotFoundException(__('Invalid periodicrating'));
		}
		$options = array('conditions' => array('Periodicrating.' . $this->Periodicrating->primaryKey => $id));
		$this->set('periodicrating', $this->Periodicrating->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($student=null, $sy=null) {
		
		$userid = $this->Auth->user('id');
		$group = $this->Auth->user('group_id');
		
		$teacherkey = $this->Auth->user('teacherkey');
		$this->set('group', $group);
		$this->set('userid', $userid);
		
		
		if ($this->request->is('post')) {
			
			foreach($this->data['Periodicrating']['subject_id'] as $key => $index):
				$data[] = array(
					'Periodicrating'		 => array(
						'student_id'		=> $this->data['Periodicrating']['student_id'],
						'gradelevel_id'		=> $this->data['Periodicrating']['gradelevel_id'],
						'gradesection_id'	=> $this->data['Periodicrating']['gradesection_id'],
						'schoolyear_id'		=> $this->data['Periodicrating']['schoolyear_id'],
						'teacher_id'		=> $this->data['Periodicrating']['teacher_id'],
						'subject_id'		=> $this->data['Periodicrating']['subject_id'][$key],
						'1st_qtr'			=> $this->data['Periodicrating']['1st_qtr'][$key],
						'2nd_qtr'			=> $this->data['Periodicrating']['2nd_qtr'][$key],
						'3rd_qtr'			=> $this->data['Periodicrating']['3rd_qtr'][$key],
						'4th_qtr'			=> $this->data['Periodicrating']['4th_qtr'][$key],
						'rating'			=> $this->data['Periodicrating']['rating'][$key],
						'action'			=> $this->data['Periodicrating']['action'][$key],
						'earned'			=> $this->data['Periodicrating']['earned'][$key],
						'added'				=> date('Y-m-d')
					)
				);
			endforeach;
			
			if(!empty($data)){
				$this->Periodicrating->create();
				if($this->Periodicrating->saveAll($data)){
					$this->Session->setFlash(__('The Periodic Rating has been saved.'), 'success_message');
					//return $this->redirect(array('action' => 'add', $student, $sy));
					return $this->redirect(array('controller' => 'students', 'action' => 'teacher_students', 0, 0, 0, $this->data['Periodicrating']['student_id']));
	
				}else{
					$this->Session->setFlash(__('The Periodic Rating already exists from the selected student and S.Y.'), 'error_message');
					return $this->redirect(array('controller' => 'students', 'action' => 'teacher_students', 0, 0, 0, $this->data['Periodicrating']['student_id']));
	
				}
			}
			
		}
		
		if($group > 1){
			$teacher = $this->Periodicrating->Teacher->findByTeacherkey($teacherkey);
		}else{
			$st 		= $this->Periodicrating->Student->findById($student);
			$teacher 	= $this->Periodicrating->Teacher->findById($st['Student']['teacher_id']);
		}
		
		$this->set('glevel', $this->Periodicrating->Gradelevel->findById($teacher['Teacher']['gradelevel_id']));
		$gradelevels = $this->Periodicrating->Gradelevel->find('list', array('conditions' => array('Gradelevel.id' => $teacher['Teacher']['gradelevel_id'])));
		$gradesections = $this->Periodicrating->Gradesection->find('list', array('conditions' => array('Gradesection.id' => $teacher['Teacher']['gradesection_id'])));
		$teachers = $this->Periodicrating->Teacher->find('first', array('conditions' => array('Teacher.id' => $teacher['Teacher']['id']), 'fields' => array('Teacher.id')));
		
		if(!empty($student)){
			$students = $this->Periodicrating->Student->find('list', array(
					'conditions' => array(
						'Student.id' => $student,
						'Student.teacher_id' => $teacher['Teacher']['id']
			)));

		}else{
			$students = $this->Periodicrating->Student->find('list', array(
					'conditions' => array(
						'Student.teacher_id' => $teacher['Teacher']['id']
			)));
		}
		
		if(!empty($sy)){
			$schoolyears = $this->Periodicrating->Schoolyear->find('list', array(
				'conditions' => array(
					'Schoolyear.id' => $sy
				
			)));
		}else{
			$schoolyears = $this->Periodicrating->Schoolyear->find('list');
		}
		
		$subjects = $this->Periodicrating->Subject->find('all');
		$this->set(compact('students', 'gradelevels', 'gradesections', 'schoolyears', 'teachers', 'subjects'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($student=null, $gradelevel=null) {
		$saved = 0;
		
		if ($this->request->is('post') || $this->request->is('put')) {
			
			foreach($this->data['Periodicrating']['id'] as $key => $index):
				$data[] = array(
					'Periodicrating'		 => array(
						'id'				=> $this->data['Periodicrating']['id'][$key],
						'1st_qtr'			=> $this->data['Periodicrating']['1st_qtr'][$key],
						'2nd_qtr'			=> $this->data['Periodicrating']['2nd_qtr'][$key],
						'3rd_qtr'			=> $this->data['Periodicrating']['3rd_qtr'][$key],
						'4th_qtr'			=> $this->data['Periodicrating']['4th_qtr'][$key],
						'rating'			=> $this->data['Periodicrating']['rating'][$key],
						'action'			=> $this->data['Periodicrating']['action'][$key],
						'earned'			=> $this->data['Periodicrating']['earned'][$key]
					)
				);
			endforeach;
			
			if($this->Periodicrating->saveMany($data, array('deep' => true))){
				$this->Session->setFlash(__('Changes has been saved.'), 'success_message');
				return $this->redirect(array('controller' => 'students', 'action' => 'teacher_students', 0, 0, 0, $student));
			}else{
				$this->Session->setFlash(__('The changes could not be saved. Please, try again.'), 'error_message');
				return $this->redirect(array('controller' => 'students', 'action' => 'teacher_students', 0, 0, 0, $student));
			}
		} else {
			$options = array('conditions' => array('Periodicrating.student_id' => $student, 'Periodicrating.gradelevel_id' => $gradelevel));
			$this->set('datas', $this->Periodicrating->find('all', $options));
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Periodicrating->id = $id;
		if (!$this->Periodicrating->exists()) {
			throw new NotFoundException(__('Invalid periodicrating'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Periodicrating->delete()) {
			$this->Session->setFlash(__('The periodicrating has been deleted.'));
		} else {
			$this->Session->setFlash(__('The periodicrating could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
