<?php
App::uses('AppController', 'Controller');
/**
 * Barangays Controller
 *
 * @property Barangay $Barangay
 * @property PaginatorComponent $Paginator
 */
class BarangaysController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	
	public function listofbarangays(){				
		if(isset($this->request->data['Student']['city_id']) || !empty($this->request->data['Student']['city_id'])){
			$id = $this->request->data['Student']['city_id'];
			$this->set('sections', $this->Barangay->find('all', array('conditions' => array('Barangay.city_id' => $id), 'order' => array('Barangay.name' => 'ASC'))));			
		}
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Barangay->recursive = 0;
		$this->set('barangays', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Barangay->exists($id)) {
			throw new NotFoundException(__('Invalid barangay'));
		}
		$options = array('conditions' => array('Barangay.' . $this->Barangay->primaryKey => $id));
		$this->set('barangay', $this->Barangay->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Barangay->create();
			if ($this->Barangay->save($this->request->data)) {
				$this->Session->setFlash(__('The barangay has been saved.'), 'success_message');
				return $this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('The barangay could not be saved. Please, try again.'), 'error_message');
			}
		}
		$provinces = $this->Barangay->Province->find('list');
		$cities = $this->Barangay->City->find('list', array('conditions' => array('City.province_id' => 1)));
		$this->set(compact('provinces', 'cities'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Barangay->exists($id)) {
			throw new NotFoundException(__('Invalid barangay'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Barangay->save($this->request->data)) {
				$this->Session->setFlash(__('The barangay has been saved.'), 'success_message');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The barangay could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$options = array('conditions' => array('Barangay.' . $this->Barangay->primaryKey => $id));
			$this->request->data = $this->Barangay->find('first', $options);
		}
		$provinces = $this->Barangay->Province->find('list');
		$cities = $this->Barangay->City->find('list');
		$this->set(compact('provinces', 'cities'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Barangay->id = $id;
		if (!$this->Barangay->exists()) {
			throw new NotFoundException(__('Invalid barangay'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Barangay->delete()) {
			$this->Session->setFlash(__('The barangay has been deleted.'));
		} else {
			$this->Session->setFlash(__('The barangay could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
