<?php
App::uses('AppController', 'Controller');
/**
 * Gradesections Controller
 *
 * @property Gradesection $Gradesection
 */
class GradesectionsController extends AppController {
		
	public function getsectionlists(){				
		if(isset($this->request->data['Schoolgrade']['gradelevel_id']) || !empty($this->request->data['Schoolgrade']['gradelevel_id'])){
			$id = $this->request->data['Schoolgrade']['gradelevel_id'];
			$this->set('sections', $this->Gradesection->find('all', array('conditions' => array('Gradesection.gradelevel_id' => $id), 'order' => array('Gradesection.name' => 'ASC'))));			
		}
		
		if(isset($this->request->data['Student']['gradelevel_id']) || !empty($this->request->data['Student']['gradelevel_id'])){
			$id = $this->request->data['Student']['gradelevel_id'];
			$this->set('sections', $this->Gradesection->find('all', array('conditions' => array('Gradesection.gradelevel_id' => $id), 'order' => array('Gradesection.name' => 'ASC'))));			
		}
		
		if(isset($this->request->data['Filterstudent']['gradelevel_id']) || !empty($this->request->data['Filterstudent']['gradelevel_id'])){
			$id = $this->request->data['Filterstudent']['gradelevel_id'];
			$this->set('sections', $this->Gradesection->find('all', array('conditions' => array('Gradesection.gradelevel_id' => $id), 'order' => array('Gradesection.name' => 'ASC'))));			
		}
		
		if(isset($this->request->data['Teacher']['gradelevel_id']) || !empty($this->request->data['Teacher']['gradelevel_id'])){
			$id = $this->request->data['Teacher']['gradelevel_id'];
			$this->set('sections', $this->Gradesection->find('all', array('conditions' => array('Gradesection.gradelevel_id' => $id), 'order' => array('Gradesection.name' => 'ASC'))));			
		}
		

	}
	
	
	public function getsectionlists_ajax(){
		$options = '';
		if($this->RequestHandler->isAjax()){
			$this->layout = 'ajax';
			$this->autoRender = false;
			$id = $_POST['gradelevel'];
			$sections = $this->Gradesection->find('all', array('conditions' => array('Gradesection.gradelevel_id' => $id), 'order' => array('Gradesection.name' => 'ASC')));			
			if(!empty($sections)){
				foreach($sections as $s):
					$options .='<option value="'.$s['Gradesection']['id'].'">'.$s['Gradesection']['name'].'</option>';
				endforeach;
			}
			
			echo json_encode($options);
		}
	}
	
	public function getnamebyid($id){
		$name = $this->Gradesection->findById($id);
		return $name['Gradesection']['name'];
	
	}
  
	public function getallsections($id){
		$sections = $this->Gradesection->find('all', array('conditions' => array('Gradesection.gradelevel_id' => $id)));
		return $sections;
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Gradesection->recursive = 1;
		$this->set('gradesections', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Gradesection->id = $id;
		if (!$this->Gradesection->exists()) {
			throw new NotFoundException(__('Invalid gradesection'));
		}
		$this->set('gradesection', $this->Gradesection->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Gradesection->create();
			if ($this->Gradesection->save($this->request->data)) {
				$this->Session->setFlash(__('The gradesection has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The gradesection could not be saved. Please, try again.'), 'error_message');
			}
		}
		
		$gradelevels = $this->Gradesection->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));		
		$this->set(compact('gradelevels'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Gradesection->id = $id;
		if (!$this->Gradesection->exists()) {
			throw new NotFoundException(__('Invalid gradesection'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Gradesection->save($this->request->data)) {
				$this->Session->setFlash(__('The gradesection has been saved'), 'success_message');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The gradesection could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$this->request->data = $this->Gradesection->read(null, $id);
		}
		
		$gradelevels = $this->Gradesection->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$this->set(compact('gradelevels'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Gradesection->id = $id;
		if (!$this->Gradesection->exists()) {
			throw new NotFoundException(__('Invalid gradesection'));
		}
		if ($this->Gradesection->delete()) {
			$this->Session->setFlash(__('Gradesection deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Gradesection was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
