<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'phpexcel/index');
//App::import('Vendor', 'dompdf/dompdf_config.inc');


/**
 * Students Controller
 *
 * @property Student $Student
 */
class StudentsController extends AppController {
	
	public function beforeFilter(){
		parent::beforeFilter();
		//$this->Auth->allow('teacher_students');
	}
	
	public function getstudentlists(){				
		if(isset($this->request->data['Schoolgrade']['section_id']) || !empty($this->request->data['Schoolgrade']['section_id'])){
			$id = $this->request->data['Schoolgrade']['section_id'];
			$this->set('students', $this->Student->find('all', array('conditions' => array('Gradesection.section_id' => $id), 'order' => array('Gradesection.name' => 'ASC'))));			
		}else{
			//$this->set('regions', $this->User->Areamanager->find('all'));
			echo '<script>alert("Do data found from the selected item");</script>';
		}		
	}
	
	public function getallstudents($id){
		$students = $this->Student->find('all', array('conditions' => array('Student.gradelevel_id' => $id)));
		return $students;
	}
	
	public function getallstudentsinsection($id){
		$students = $this->Student->find('all', array('conditions' => array('Student.gradesection_id' => $id)));
		return $students;
	}
	
	public function getallstudentsinteacher($id){
		$students = $this->Student->find('all', array('conditions' => array('Student.teacher_id' => $id)));
		return $students;
	}
	
	public function getstudentidbeforeupload($firstname=null, $middlename=null, $lastname=null, $schoolyear=null){
		$student = $this->Student->find('first', array('conditions' => array(
				'Student.firstname' 	=> $firstname,
				'Student.middlename' 	=> $middlename,
				'Student.lastname' 		=> $lastname,
				//'Student.schoolyear_id' => $schoolyear
				)
			)
		);
		
		if(!empty($student)){
			return $student['Student']['id'];
		}else{	
			return 0;
		}
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		
			$group = $this->Auth->user('group_id');
			$this->set('group', $group);
			$teacherkey = $this->Auth->user('teacherkey');
			$teacher  = $this->Student->Teacher->findByTeacherkey($teacherkey);
			$this->set('teacher', !empty($teacher) ? $teacher['Teacher']['id'] : '');
			
			
				
				if($group > 1){
					$this->set('students', $this->Student->find('all', array(
						'conditions' => array(
							'Student.teacher_id' => $teacher['Teacher']['id'],
						),
						'order'	=> array('Student.lastname' => 'ASC')
					)));
				}else{
					$this->set('students', $this->Student->find('all', array(
						'order'	=> array('Student.lastname' => 'ASC')
					)));
				}
			
			
	
	}
	
	
	public function teacher_students($sy=null, $gradelevel=null, $section=null, $studentid=null){
			$group = $this->Auth->user('group_id');
			$this->set('group', $group);
			$this->set('studentid', $studentid);
			$teacherkey = $this->Auth->user('teacherkey');
			$teacher  = $this->Student->Teacher->findByTeacherkey($teacherkey);
			$this->set('teacher', !empty($teacher) ? $teacher['Teacher']['id'] : '');
			
			if(!empty($sy) && !empty($gradelevel) && !empty($section)){
			//if($this->request->is('post') || $this->request->is('put')){
				$this->set('initiate', false);
				$this->set('sy', $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.sy_from' => 'DESC'))));
				//$sy 		= $this->data['Filterstudent']['schoolyear_id'];
				//$gradelevel = $this->data['Filterstudent']['gradelevel_id'];
				//$section 	= $this->data['Filterstudent']['gradesection_id'];
				
				$this->set('syc', $this->Student->Schoolyear->findById($sy));
				$this->set('grade', $this->Student->Gradelevel->findById($gradelevel));
				$this->set('section', $this->Student->Gradesection->findById($section));
				
				if($group > 1){
					$this->set('students', $this->Student->find('all', array(
						'conditions' => array(
							'Student.teacher_id' => $teacher['Teacher']['id'],
							'Student.schoolyear_id' => $sy,
							'Student.gradelevel_id' => $gradelevel,
							'Student.gradesection_id' => $section
						)
					)));
				}else{
					$this->set('students', $this->Student->find('all', array(
						'conditions' => array(
							'Student.schoolyear_id' => $sy,
							'Student.gradelevel_id' => $gradelevel,
							'Student.gradesection_id' => $section
						)
					)));
				}
			}else{
				if(isset($studentid) && !empty($studentid)){
					$this->set('initiate', false);
					$this->set('sy', $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.sy_from' => 'DESC'))));
					if($group > 1){
						
						$this->set('students', $this->Student->find('all', array(
							'conditions' => array(
								'Student.id' => $studentid,
								'Student.teacher_id' => $teacher['Teacher']['id']
							)
						)));
					}else{
						$this->set('students', $this->Student->find('all', array(
							'conditions' => array(
								'Student.id' => $studentid
								
							)
						)));
					}
				}else{
					$this->set('initiate', true);
					$schoolyears 	= $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.sy_from' => 'DESC')));
					$gradesections 	= '';
					$gradelevels 	= $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
					$this->set(compact('schoolyears', 'gradelevels', 'gradesections'));
				}
			}
			
	
			
			
			
		
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null, $grade=null, $section=null, $sy=null) {
		$this->Student->id = $id;
		$group = $this->Auth->user('group_id');
		
		if (!$this->Student->exists()) {
			throw new NotFoundException(__('Invalid student'));
		}
		$this->set('grade', $grade);
		$this->set('section', $section);
		$this->set('group', $group);
		$this->set('sy', $sy);
		
		$this->set('student', $this->Student->read(null, $id));
		
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('Studentschoolyear');
		
		$userid = $this->Auth->user('id');
		$group = $this->Auth->user('group_id');
		$teacherkey = $this->Auth->user('teacherkey');
		$this->set('group', $group);
		$this->set('userid', $userid);
		
		if ($this->request->is('post')) {			
			if($this->checkifstudentexists($this->data['Student']['firstname'], $this->data['Student']['lastname'], $this->data['Student']['middlename'], $this->combinebirthdate($this->data['Student']['birthdate'], ''))){
				$this->Student->create();
				if ($this->Student->save($this->request->data)) {
					
					/*$studentid = $this->Student->getLastInsertId();
					
					$this->Studentschoolyear->create();
					$this->Studentschoolyear->save(array(	
						'Studentschoolyear' => array(
							'student_id'	=> $studentid,
							'sy'				=> $this->data['Student']['schoolyear'],
							'gradelevel_id'		=> $this->data['Student']['gradelevel_id'],
							'gradesection_id'	=> $this->data['Student']['gradesection_id']
						)
					));
					*/
					
					
					$this->Session->setFlash(__('The student information has been saved'), 'success_message');
					$this->redirect(array('action' => 'add'));
				} else {
					$this->Session->setFlash(__('The student could not be saved. Please, try again.'), 'error_message');
				}
			}else{
				$this->Session->setFlash(__('The student could not be saved. Data already exists.'), 'error_message');				
			}
		}
		$studentindicators = $this->Student->Studentindicator->find('list');
		
		if($group==1){
			$gradelevels = $this->Student->Gradelevel->find('list');
			$teachers = '';
			//$teachers = $this->Student->Teacher->find('list');
			$gradesections = '';
		}else{
			$teacher = $this->Student->Teacher->findByTeacherkey($teacherkey);
			$gradelevels = $this->Student->Gradelevel->find('list', array('conditions' => array('Gradelevel.id' => $teacher['Teacher']['gradelevel_id'])));
			$gradesections = $this->Student->Gradesection->find('list', array('conditions' => array('Gradesection.id' => $teacher['Teacher']['gradesection_id'])));
			$teachers = $this->Student->Teacher->find('list', array('conditions' => array('Teacher.teacherkey' => $teacherkey)));

		}
		
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.id' => 'DESC')));
		$provinces 	= $this->Student->Province->find('list', array('order' => array('Province.name' => 'ASC')));
		$cities 	= '';
		//$cities 	= $this->Student->City->find('list', array('conditions' => array('City.province_id' => 1), 'order' => array('City.name' => 'ASC')));
		$barangays 	= '';
		//$barangays 	= $this->Student->Barangay->find('list', array('conditions' => array('Barangay.city_id' => 1),'order' => array('Barangay.name' => 'ASC')));
		$this->set(compact('studentindicators', 'gradelevels', 'teachers', 'gradesections', 'schoolyears', 'provinces', 'cities', 'barangays'));
	}
	
	public function combinebirthdate($birthday){
		$year = $birthday['year'];
		$month = $birthday['month'];
		$day = $birthday['day'];
		$birthday  = $year.'-'.$month.'-'.$day;
		return $birthday;
	}
	
	public function batchupload(){
			
		$objPHPExcel = new PHPExcel();
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objReader = new PHPExcel_Reader_Excel5();
		$objReader->setReadDataOnly(true);		
		
		$userid = $this->Auth->user('id');
		$total = 0;
		$this->loadModel('Studentschoolyear');
		
		if ($this->request->is('post')) {
			if(empty($this->data['Student']['gradelevel_id']) || empty($this->data['Student']['gradesection_id'])){
				$this->Session->setFlash(__('Invalid Grade or Section Data'), 'error_message');
			}else{
				if(empty($this->data['Student']['filetoupload']['name'])  || empty($this->data['Student']['filetoupload']['tmp_name'])){
					$this->Session->setFlash(__('Invalid file, please try again.'), 'error_message');
				}else{
					if($this->checkforextension($this->data['Student']['filetoupload'])){			
						
						$objPHPExcel = PHPExcel_IOFactory::load($this->data['Student']['filetoupload']['tmp_name'], "r");
						$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);										
						$heighestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();	
						
						for($i=10; $i<=$heighestRow; $i++){ 	
										
										$lrn				= $objWorksheet->getCell('A'.$i.'')->getValue();
										$lastname			= $objWorksheet->getCell('B'.$i.'')->getValue();
										$firstname			= $objWorksheet->getCell('C'.$i.'')->getValue();
										$middlename			= $objWorksheet->getCell('D'.$i.'')->getValue();
										$gender				= $objWorksheet->getCell('E'.$i.'')->getValue();
										$birthdate			= date('Y-m-d', strtotime(PHPExcel_Style_NumberFormat::toFormattedString($objWorksheet->getCell('F'.$i.'')->getValue(), 'YYYY-MM-DD')));
										$age 				= $objWorksheet->getCell('J'.$i.'')->getValue(); //as of  first friday of june
										$birthpalce			= $objWorksheet->getCell('H'.$i.'')->getValue();
										$mother_tongue		= $objWorksheet->getCell('I'.$i.'')->getValue();
										$ethnic_group		= $objWorksheet->getCell('J'.$i.'')->getValue();
										$religion			= $objWorksheet->getCell('K'.$i.'')->getValue();
										$address_house		= $objWorksheet->getCell('L'.$i.'')->getValue();
										$address_brgy		= $objWorksheet->getCell('M'.$i.'')->getValue();
										$address_municipal	= $objWorksheet->getCell('N'.$i.'')->getValue();
										$adderss_province	= $objWorksheet->getCell('O'.$i.'')->getValue();
										$father_fullname	= $objWorksheet->getCell('P'.$i.'')->getValue();
										$mother_fullname	= $objWorksheet->getCell('Q'.$i.'')->getValue();
										$guardian_fullname	= $objWorksheet->getCell('R'.$i.'')->getValue();
										$relationship		= $objWorksheet->getCell('S'.$i.'')->getValue();
										$contact_number		= $objWorksheet->getCell('T'.$i.'')->getValue();
										$studentindicatorname	= $objWorksheet->getCell('U'.$i.'')->getValue();
										
										$schoolyear			= ''; // $this->data['Student']['schoolyear_id'];
										//$schoolyers 		= implode("," $this->data['Student']['schoolyear_id']);
										$indicator = $this->Uploadfile->getindicatorid($studentindicatorname);
										//$lrn_id = $this->checkiflrnexists($lrn);
										//check if data exists update the information

											if((preg_match('/^[0-9]*$/', $lrn) && strlen($lrn) >= 3) && !empty($firstname) && !empty($lastname)){											
												
												$studentid = $this->checkifexists($firstname, $lastname, $middlename, $birthdate, $schoolyear);
												//if($this->checkiflrnexists($lrn)){
													if(!empty($studentid)){
															$student = $this->Student->findById($studentid);
															//$sy = explode("," $student['Student']['schoolyears']);
															
														//update the data															
																 $this->Student->id = $studentid;															 
																 $this->Student->saveField('religion', $religion);
																 $this->Student->saveField('address_house', $address_house);
																 $this->Student->saveField('address_brgy', $address_brgy);
																 $this->Student->saveField('address_municipal', $address_municipal);
																 $this->Student->saveField('adderss_province', $adderss_province);
																 $this->Student->saveField('father_fullname', $father_fullname);
																 $this->Student->saveField('mother_fullname', $mother_fullname);
																 $this->Student->saveField('guardian_fullname', $guardian_fullname);
																 $this->Student->saveField('relationship', $relationship);
																 $this->Student->saveField('contact_number', $contact_number);
																 $this->Student->saveField('gradelevel_id', $this->data['Student']['gradelevel_id']);
																 $this->Student->saveField('gradesection_id', $this->data['Student']['gradesection_id']);
																 //$this->Student->saveField('teacher_id', $this->data['Student']['teacher_id']);
																// $this->Student->saveField('schoolyear_id', $this->data['Student']['schoolyear_id']);
																 
																 $ssy = $this->Student->Studentschoolyear->find('first', array('conditions' => array(
																		'Studentschoolyear.student_id'	=> $studentid,
																		'Studentschoolyear.sy'	=> $this->data['Student']['schoolyear'],
																		'Studentschoolyear.gradesection_id'	=> $this->data['Student']['gradesection_id'],
																		'Studentschoolyear.gradelevel_id'	=> $this->data['Student']['gradelevel_id']
																	)
																));
																
																if(empty($ssy)){
																	$this->Studentschoolyear->create();
																		$this->Studentschoolyear->save(array(	
																			'Studentschoolyear' => array(
																				'student_id'	=> $studentid,
																				'schoolyear_id'	=> $this->data['Student']['schoolyear_id'],														
																				'sy'			=> $this->data['Student']['schoolyear'],														
																				'teacher_id'	=> $this->data['Student']['teacher_id'],														
																				'gradesection_id'	=> $this->data['Student']['gradesection_id'],															
																				'gradelevel_id'	=> $this->data['Student']['gradelevel_id']				
																			)
																		));
																		
																		$this->Session->setFlash('Student information has been saved & updated.', 'success_message');
																		$this->redirect(array('controller' => 'studentschoolyears', 'action' => 'index'));
																}
																 //$this->Student->saveField('schoolyears', $contact_number);
													}else{
														$total++;
														$data[] = array(
															'Student' => array(												
																 'lrn'					=>	$lrn,
																 'firstname'			=>	$firstname,
																 'middlename'			=>	$middlename,
																 'lastname'				=>	$lastname,
																 'gender'				=>	$gender,
																 'birthdate'			=>	$birthdate,
																 'age'					=>	0,
																 'birthpalce'			=>	$birthpalce,
																 'mother_tongue'		=>	$mother_tongue,
																 'ethnic_group'			=>	$ethnic_group,
																 'religion'				=>	$religion,
																 'address_house'		=>	$address_house,
																 'address_brgy'			=>	$address_brgy,
																 'address_municipal'	=>	$address_municipal,
																 'adderss_province'		=>	$adderss_province,
																 'father_fullname'		=>	$father_fullname,
																 'mother_fullname'		=>	$mother_fullname,
																 'guardian_fullname'	=>	$guardian_fullname,
																 'relationship'			=>	$relationship,
																 'contact_number'		=>	$contact_number,
																 'studentindicator_id'	=>	$indicator,
																 'added'				=>	date('Y-m-d'),
																 'added_by'				=>	$this->Auth->user('id'),
																 'modified'				=>	date('Y-m-d'),
																 'modified_by'			=>	0,
																 //'gradelevel_id'		=>	$this->data['Student']['gradelevel_id'],
																 //'gradesection_id'		=>	$this->data['Student']['gradesection_id'],
																 //'teacher_id'			=>	$this->data['Student']['teacher_id'],
																 //'schoolyear_id'		=>	$this->data['Student']['schoolyear_id']
																 //'schoolyears'			=>	$schoolyers
															)
														);
													}
												//}
											}
										
																		
						}
						
						if(!empty($data)){
							$this->Student->create();
							if ($this->Student->saveAll($data)) {
								$post_ids = $this->Student->inserted_ids;
								foreach($post_ids as $sid):
									$newdata[] = array(
										'Studentschoolyear' => array(
											'student_id'	=> $sid,
											'schoolyear_id'	=> $this->data['Student']['schoolyear_id'],														
											'sy'			=> $this->data['Student']['schoolyear'],														
											'teacher_id'	=> $this->data['Student']['teacher_id'],														
											'gradesection_id'	=> $this->data['Student']['gradesection_id'],															
											'gradelevel_id'	=> $this->data['Student']['gradelevel_id']															
										)
									);
												endforeach;	
												
								
								$this->Studentschoolyear->create();
								$this->Studentschoolyear->saveAll($newdata);
								
								//$newids  = implode("," $post_ids);
								//$this->Uploadfile->saveallinsertedids($post_ids, $this->data['Student']['schoolyear_id']);							
								$this->Session->setFlash('( '. $total . ' ) Total student information that has been saved & updated.', 'success_message');
								$this->redirect(array('controller' => 'studentschoolyears', 'action' => 'index'));
							} else {
								$this->Session->setFlash(__('The student could not be saved. Please, try again.'), 'error_message');
							}
						}else{
								$this->Session->setFlash(__('No data to be processed, please try again.'), 'error_message');
						}
					}else{
						$this->Session->setFlash(__('You have uploaded an invalid file, please try again.'), 'error_message');
					}
				}
			}
		}
		$studentindicators = $this->Student->Studentindicator->find('list');
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$gradesections = ''; //$this->Student->Gradesection->find('list', array('order' => array('Gradesection.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list', array('order' => array('Teacher.name' => 'ASC')));
		//$schoolyears = $this->Student->Schoolyear->find('list');
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.id' => 'DESC')));
		$this->set(compact('studentindicators', 'gradelevels', 'gradesections', 'schoolyears', 'teachers'));
	}
	
	public function checkforextension($filename){
		if(!empty($filename['name'])){
				$extension = pathinfo($filename['name'], PATHINFO_EXTENSION);
				$ext=0;
				
				//check the extension
				switch($extension){
					case "xls": $ext++; break;				
					case "xlsx": $ext++; break;													
				}	
				
				if($ext > 0){
					return true;
				}else{
					return false;
				}
		}else{
			return false;
		}
							
	}
	
	
	public function checkiflrnexists($lrn){
		$student = $this->Student->findByLrn($lrn);
		
		if(!empty($student)){
			return false;
		}else{
			return true;
		}
	}
	
	public function checkifexists($firstname, $lastname, $middlename, $birthdate, $schoolyear){
		$student = $this->Student->find('first', array(
			'conditions' => array(
				'Student.firstname' 	=> $firstname, 
				'Student.lastname'  	=> $lastname, 
				'Student.middlename' 	=> $middlename,
				'Student.birthdate'		=> $birthdate,
				//'Student.schoolyear_id'	=> $schoolyear,
				)
			)
		);
		
		if(!empty($student)){
			return $student['Student']['id'];
		}else{
			return 0;
		}
	}
	
	public function checkifstudentexists($firstname, $lastname, $middlename, $birthdate){
		$student = $this->Student->find('count', array('conditions' => array(
					'Student.firstname' 	=> $firstname, 
					'Student.lastname'  	=> $lastname, 
					'Student.middlename' 	=> $middlename,
					'Student.birthdate'		=> $birthdate
				)
			)
		);
		
		if($student > 0){
			return false;
		}else{
			return true;
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function studentHasPrecord($sid, $syid){
		$details = $this->Student->Periodicrating->find('count', array(
			'conditions' => array(
				'Periodicrating.student_id' => $sid,
				'Periodicrating.schoolyear_id' => $syid
			)
		));
		
		if($details > 0){
			return true;
		}else{
			return false;
		}
	}
	
	public function studentHasArecord($sid, $syid){
		$details = $this->Student->Attendance->find('count', array(
			'conditions' => array(
				'Attendance.student_id' => $sid,
				'Attendance.schoolyear_id' => $syid
			)
		));
		
		if($details > 0){
			return true;
		}else{
			return false;
		}
	}
	
	
	public function edit($id = null, $view=false, $enroll=false) {
		$this->Student->id = $id;
		$userid = $this->Auth->user('id');
		$group = $this->Auth->user('group_id');
		
		$this->set('userid', $userid);
		$this->set('group', $group);
		$this->set('view', $view);
		$this->set('enroll', $enroll);
		
		if (!$this->Student->exists()) {
			throw new NotFoundException(__('Invalid student'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			//if(!$this->studentHasPrecord($this->data['Student']['id'], $this->data['Student']['schoolyear_id'])){
				//if(!$this->studentHasArecord($this->data['Student']['id'], $this->data['Student']['schoolyear_id'])){
					if ($this->Student->save($this->request->data)) {
						$this->Session->setFlash(__('The student information has been saved'), 'success_message');
						$this->redirect(array('action' => 'edit', $id));
					} else {
						$this->Session->setFlash(__('The student could not be saved. Please, try again.'), 'error_message');
					}
				//}else{
					//$this->Session->setFlash(__('The changes could not be saved. Invalid S.Y. Please, try again.'), 'error_message');
				//}
			//}else{
				//$this->Session->setFlash(__('The changes could not be saved. Invalid S.Y. Please, try again.'), 'error_message');
			//}
		} else {
			$this->request->data = $this->Student->read(null, $id);
		}
		
		$st = $this->Student->findById($id);
		
		$studentindicators = $this->Student->Studentindicator->find('list');
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list');
		$gradesections = $this->Student->Gradesection->find('list');
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.id' => 'DESC')));
		$provinces 	= $this->Student->Province->find('list', array('order' => array('Province.name' => 'ASC')));
		$cities 	= $this->Student->City->find('list', array('conditions' => array('City.province_id' => $st['Student']['province_id']), 'order' => array('City.name' => 'ASC')));
		$barangays 	= $this->Student->Barangay->find('list', array('conditions' => array('Barangay.city_id' => $st['Student']['city_id']), 'order' => array('Barangay.name' => 'ASC')));

		$this->set(compact('studentindicators', 'gradelevels', 'teachers', 'gradesections', 'schoolyears', 'provinces', 'cities', 'barangays'));
	}
	
	public function newenroll($id = null) {
		$this->Student->id = $id;
		$userid = $this->Auth->user('id');
		$group = $this->Auth->user('group_id');
		
		$this->set('userid', $userid);
		$this->set('group', $group);
		
		if (!$this->Student->exists()) {
			throw new NotFoundException(__('Invalid student'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if(!$this->studentHasPrecord($this->data['Student']['id'], $this->data['Student']['schoolyear_id'])){
				if(!$this->studentHasArecord($this->data['Student']['id'], $this->data['Student']['schoolyear_id'])){
					if ($this->Student->save($this->request->data)) {
						$this->Session->setFlash(__('The student information has been saved'), 'success_message');
						$this->redirect(array('action' => 'edit', $id));
					} else {
						$this->Session->setFlash(__('The student could not be saved. Please, try again.'), 'error_message');
					}
				}else{
					$this->Session->setFlash(__('The changes could not be saved. Invalid S.Y. Please, try again.'), 'error_message');
				}
			}else{
				$this->Session->setFlash(__('The changes could not be saved. Invalid S.Y. Please, try again.'), 'error_message');
			}
		}
		
		$this->request->data = $this->Student->read(null, $id);
		
		$st = $this->Student->findById($id);
		
		$studentindicators = $this->Student->Studentindicator->find('list');
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list');
		$gradesections = $this->Student->Gradesection->find('list');
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.id' => 'DESC')));
		$provinces 	= $this->Student->Province->find('list', array('order' => array('Province.name' => 'ASC')));
		$cities 	= $this->Student->City->find('list', array('conditions' => array('City.province_id' => $st['Student']['province_id']), 'order' => array('City.name' => 'ASC')));
		$barangays 	= $this->Student->Barangay->find('list', array('conditions' => array('Barangay.city_id' => $st['Student']['city_id']), 'order' => array('Barangay.name' => 'ASC')));

		$this->set(compact('studentindicators', 'gradelevels', 'teachers', 'gradesections', 'schoolyears', 'provinces', 'cities', 'barangays'));
	}
	

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function homepage(){
		$this->set('group', $this->Auth->user('group_id'));
	}
	
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Student->id = $id;
		if (!$this->Student->exists()) {
			throw new NotFoundException(__('Invalid student'));
		}
		if ($this->Student->delete()) {
			$this->Session->setFlash(__('Student deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Student was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function reportinitiate(){
		
		$this->set('download', false);
		if ($this->request->is('post')) {				
			//$this->redirect(array('action' => 'report', 'ext' => 'pdf', $this->data['Student']['teacher_id'], $this->data['Student']['gradelevel_id'], $this->data['Student']['gradesection_id'], $this->data['Student']['schoolyear_id']));
			$this->set('download', true);
		} 
		
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list');
		$gradesections = ''; //$this->Student->Gradesection->find('list');
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.name' => 'DESC')));
		$this->set(compact('gradelevels', 'teachers', 'gradesections', 'schoolyears'));
	}
	
	public function form1report(){
		
		
		$this->set('download', false);
		if ($this->request->is('post')) {	
			$this->set('download', true);
			$this->set('details', $this->countallstudents($this->data['Student']['gradelevel_id'], $this->data['Student']['gradesection_id'], $this->data['Student']['schoolyear_id']));
			$gradesections = $this->Student->Gradesection->find('list', array('conditions' => array('Gradesection.gradelevel_id' => $this->data['Student']['gradelevel_id'])));
			
			//$this->redirect(array('action' => 'registrareport', 'ext' => 'pdf', $this->data['Student']['gradelevel_id'], $this->data['Student']['gradesection_id'], $this->data['Student']['schoolyear_id']));
		}else{
			$gradesections = $this->Student->Gradesection->find('list', array('conditions' => array('Gradesection.gradelevel_id' => 1)));
			
		}
		
		$gradelevels = $this->Student->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$teachers = $this->Student->Teacher->find('list');
	
		
		$schoolyears = $this->Student->Schoolyear->find('list', array('order' => array('Schoolyear.name' => 'DESC')));
		$this->set(compact('gradelevels', 'teachers', 'gradesections', 'schoolyears'));
	}
	
	
	public function countallstudents($gradelevelid, $sectionid, $syid){
		$conditions = array(
			'conditions' => array(				
				'Student.gradelevel_id'		=> $gradelevelid,			
				'Student.gradesection_id'	=> $sectionid,			
				'Student.schoolyear_id'		=> $syid			
			)
		);
		
		$details = $this->Student->find('count', $conditions);
		
		return $details;
	}
	/*public function registrareport($gradelevelid, $sectionid, $syid){
		// increase memory limit in PHP 
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		$this->Student->recursive = 0;
				
		$this->set('gradelevel', $this->Student->Gradelevel->findById($gradelevelid));
		$this->set('section', $this->Student->Gradesection->findById($sectionid));
		$this->set('sy', $this->Student->Schoolyear->findById($syid));
		
		$this->paginate = array(
			'conditions' => array(				
				'Student.gradelevel_id'		=> $gradelevelid,			
				'Student.gradesection_id'	=> $sectionid,			
				'Student.schoolyear_id'		=> $syid			
			),
			'order'	=> array('Student.lastname' => 'ASC'),
			'limit'	=> 200
		);
		
		$this->set('students', $this->paginate());
	}*/
	
	
	
	public function registrareport($gradelevelid, $sectionid, $syid, $date){
		// increase memory limit in PHP 
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		$this->Student->Schoolyear->recursive = -2;
		$this->Student->Gradelevel->recursive = -2;
		$this->Student->Gradesection->recursive = -2;
		$this->Student->recursive = -2;
				
		$this->set('gradelevel', $this->Student->Gradelevel->findById($gradelevelid));
		$this->set('section', $this->Student->Gradesection->findById($sectionid));
		$this->set('sy', $this->Student->Schoolyear->findById($syid));
		
		$conditions = array(
			'conditions' => array(				
				'Student.gradelevel_id'		=> $gradelevelid,			
				'Student.gradesection_id'	=> $sectionid,			
				'Student.schoolyear_id'		=> $syid			
			),
			'order'	=> array('Student.lastname' => 'ASC')
		);
		
		$this->set('students', $this->Student->find('all', $conditions));
	}
	
	
	public function report($teacherid, $gradelevelid, $sectionid, $syid){
		// increase memory limit in PHP 
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		$this->Student->recursive = 0;
		
		$this->set('teacher', $this->Student->Teacher->findById($teacherid));
		$this->set('gradelevel', $this->Student->Gradelevel->findById($gradelevelid));
		$this->set('section', $this->Student->Gradesection->findById($sectionid));
		$this->set('sy', $this->Student->Schoolyear->findById($syid));
		
		$this->paginate = array(
			'conditions' => array(
				'Student.teacher_id'		=> $teacherid,			
				'Student.gradelevel_id'		=> $gradelevelid,			
				'Student.gradesection_id'	=> $sectionid,			
				'Student.schoolyear_id'		=> $syid			
			),
			'order'	=> array('Student.lastname' => 'ASC'),
			'limit'	=> 200
		);
		
		$this->set('students', $this->paginate());
	}
	
	public function gradelevelcount($id, $gender, $sy){
		$student = $this->Student->find('count', array('conditions' => array('Student.gender' => $gender, 'Student.gradelevel_id' => $id, 'Student.schoolyear_id' => $sy)));
		return $student;
	}
}
