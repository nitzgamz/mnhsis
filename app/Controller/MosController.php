<?php
App::uses('AppController', 'Controller');
/**
 * Mos Controller
 *
 * @property Mo $Mo
 * @property PaginatorComponent $Paginator
 */
class MosController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Mo->recursive = 0;
		$this->set('mos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Mo->exists($id)) {
			throw new NotFoundException(__('Invalid mo'));
		}
		$options = array('conditions' => array('Mo.' . $this->Mo->primaryKey => $id));
		$this->set('mo', $this->Mo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$data = array();
		$saved = 0 ;
		if ($this->request->is('post')) {
			
			if(!empty($this->data['Mo']['sdays'])){
				if(count($this->data['Mo']['sdays'])==10){
					foreach($this->data['Mo']['name'] as $key  => $index):
						$data = array(
							'Mo'	=> array(
								'name' 			=>	$this->data['Mo']['name'][$key],
								'short'		 	=>  $this->data['Mo']['short'][$key],
								'schoolyear_id' =>  $this->data['Mo']['schoolyear_id'],
								'sdays' 		=>  $this->data['Mo']['sdays'][$key]
							)
						);
						
						$this->Mo->create();
						if($this->Mo->save($data)){
							$saved++;
								
						}
						
					endforeach;
					
					
					if($saved==10){
							$this->Session->setFlash(__('The mo has been saved.'), 'success_message');
								return $this->redirect(array('action' => 'index'));
					}else{
							$this->Session->setFlash(__('Some of the mo has been saved.'), 'success_message');
								return $this->redirect(array('action' => 'index'));
					}
						
				}else{
					$this->Session->setFlash(__('The mo could not be saved. Please, try again.'), 'error_message');
				}
			}else{
				$this->Session->setFlash(__('The mo could not be saved. Please, try again.'), 'error_message');
			}
			
		}
		$schoolyears = $this->Mo->Schoolyear->find('list', array('order' => array('Schoolyear.sy_from' => 'DESC')));
		$this->set(compact('schoolyears'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Mo->exists($id)) {
			throw new NotFoundException(__('Invalid mo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Mo->save($this->request->data)) {
				$this->Session->setFlash(__('The mo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Mo.' . $this->Mo->primaryKey => $id));
			$this->request->data = $this->Mo->find('first', $options);
		}
		$schoolyears = $this->Mo->Schoolyear->find('list');
		$this->set(compact('schoolyears'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Mo->id = $id;
		if (!$this->Mo->exists()) {
			throw new NotFoundException(__('Invalid mo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Mo->delete()) {
			$this->Session->setFlash(__('The mo has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
