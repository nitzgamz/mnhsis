<?php
App::uses('AppController', 'Controller');
/**
 * Studentschoolyears Controller
 *
 * @property Studentschoolyear $Studentschoolyear
 */
class StudentschoolyearsController extends AppController {

	
	public function getstudentlists(){				
		if(isset($this->request->data['Schoolgrade']['gradesection_id']) || !empty($this->request->data['Schoolgrade']['gradesection_id'])){
			$id = $this->request->data['Schoolgrade']['gradesection_id'];
			$this->set('students', $this->Studentschoolyear->find('all', array('conditions' => array('Studentschoolyear.gradesection_id' => $id), 'order' => array('Student.lastname' => 'ASC'))));			
		}
	}
	
	
	public function saveallinsertedids($post_ids=array(), $sy=null){
			$newdata = array();
			foreach($post_ids as $sid):
								$newdata[] = array(
									'Studentschoolyear' => array(
										'student_id'	=> $sid,
										'schoolyear_id'	=> $sy
									)
								);
							endforeach;	
		$this->Studentschoolyear->create();
		if($this->Studentschoolyear->saveAll($newdata)){
			return true;
		}else{
			return false;
		}
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Studentschoolyear->recursive = 0;
		
		$this->set('search', false);
		$this->set('filter', false);
		
		if((isset($_GET['keyword']) && !empty($_GET['keyword'])) || (isset($_GET['lrnkeyword']) && !empty($_GET['lrnkeyword']))){
		
			$student_keyword = $this->request->query['keyword'];
			$student_lrn  	= $this->request->query['lrnkeyword'];
			
			
			$this->set('student_lrn', $student_lrn);
			$this->set('student_keyword', $student_keyword);
			
			
			if(isset($student_keyword) && !empty($student_keyword)){
					$this->set('search', true);
					
					if(isset($student_lrn) && !empty($student_lrn)){						
						$this->paginate = array(
								'conditions' => array(
									'OR' => array(
										'Student.firstname LIKE' => "%$student_keyword%",								
										'Student.lastname LIKE' => "%$student_keyword%",								
										'Student.lrn LIKE' => "%$student_lrn%"				
									)
								
								),
									'limit'		=> 200,
									'order'	=> array('Student.lastname' => 'ASC')
							);
					}else{
						$this->paginate = array('conditions' => array(
									'OR' => array(
										'Student.firstname LIKE' => "%$student_keyword%",								
										'Student.lastname LIKE' => "%$student_keyword%",																	
									),
									
								),
								'limit'		=> 200,
								'order'	=> array('Student.lastname' => 'ASC')
							);
					}
					
					
					
			}
			
			if(isset($student_lrn) && !empty($student_lrn)){
					$this->set('search', true);
						$this->paginate = array(
									'conditions' => array(								
										'OR' => array(										
											'Student.lrn LIKE' => "%$student_lrn%"
										),											
									),									
									'limit'		=> 200,
									'order'	=> array('Student.lastname' => 'ASC')
								);
					
					
						
			}
		}else{
		
		
			if((isset($_GET['gradelevel_id']) && !empty($_GET['gradelevel_id'])) || (isset($_GET['gradesection_id']) && !empty($_GET['gradesection_id'])) || (isset($_GET['schoolyear']) && !empty($_GET['schoolyear']))){
								
					$this->set('filter', true);
						
					//$grade = $this->request->query['gradelevel_id'];
					$grade = $this->request->query['gradelevel_id'];
					$section = $this->request->query['gradesection_id'];
					$sy = $this->request->query['schoolyear'];
										
					
					$this->set('grade', $this->Studentschoolyear->Gradelevel->findById($grade));
					$this->set('section', $this->Studentschoolyear->Gradesection->findById($section));
					$this->set('sy', $sy);
					
					
					
						if(!empty($sy)){
							if(!empty($grade)){
								if(!empty($section)){
									$this->paginate = array(
										'conditions' => array(
												'Studentschoolyear.sy'	=> $sy,
												'Studentschoolyear.gradelevel_id'	=> $grade,
												'Studentschoolyear.gradesection_id'	=> $section
										),
										'limit'		=> 200,
										'order'	=> array('Student.lastname' => 'ASC')
									);
								}else{
									$this->paginate = array(
										'conditions' => array(
												'Studentschoolyear.sy'	=> $sy,
												'Studentschoolyear.gradelevel_id'	=> $grade											
										),
										'limit'		=> 200,
										'order'	=> array('Student.lastname' => 'ASC')
									);
								}
							}else{
								$this->paginate = array(
										'conditions' => array(
												'Studentschoolyear.sy'	=> $sy												
										),
										'limit'		=> 200,
										'order'	=> array('Student.lastname' => 'ASC')
									);
							}							
						}
				
			}else{
				$this->paginate = array(
					'conditions' => array('Studentschoolyear.sy' => date('Y'))
				);
			}
		}
		
		
		$this->set('students', $this->paginate());
		
		
		$gradelevels = $this->Studentschoolyear->Gradelevel->find('list', array('order' => array('Gradelevel.name' => 'ASC')));
		$gradesections = $this->Studentschoolyear->Gradesection->find('list', array('order' => array('Gradesection.name' => 'ASC')));
		$this->set('schoolyears', $this->Studentschoolyear->find('all', array('group' => array('Studentschoolyear.sy'), 'order' => array('Studentschoolyear.sy' => 'DESC'))));
		$this->set(compact('gradesections', 'gradelevels'));
		
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Studentschoolyear->id = $id;
		if (!$this->Studentschoolyear->exists()) {
			throw new NotFoundException(__('Invalid studentschoolyear'));
		}
		$this->set('studentschoolyear', $this->Studentschoolyear->read(null, $id));
		
		
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Studentschoolyear->create();
			if ($this->Studentschoolyear->save($this->request->data)) {
				$this->Session->setFlash(__('The studentschoolyear has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The studentschoolyear could not be saved. Please, try again.'));
			}
		}
		$students = $this->Studentschoolyear->Student->find('list');
		$schoolyears = $this->Studentschoolyear->Schoolyear->find('list');
		$this->set(compact('students', 'schoolyears'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Studentschoolyear->id = $id;
		if (!$this->Studentschoolyear->exists()) {
			throw new NotFoundException(__('Invalid studentschoolyear'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Studentschoolyear->save($this->request->data)) {
				$this->Session->setFlash(__('The studentschoolyear has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The studentschoolyear could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Studentschoolyear->read(null, $id);
		}
		$students = $this->Studentschoolyear->Student->find('list');
		$schoolyears = $this->Studentschoolyear->Schoolyear->find('list');
		$this->set(compact('students', 'schoolyears'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Studentschoolyear->id = $id;
		if (!$this->Studentschoolyear->exists()) {
			throw new NotFoundException(__('Invalid studentschoolyear'));
		}
		if ($this->Studentschoolyear->delete()) {
			$this->Session->setFlash(__('Studentschoolyear deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Studentschoolyear was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
	public function schoolyears(){
		$this->Studentschoolyear->recursive = 0;
		$this->paginate = array('group' => array('Studentschoolyear.sy'), 'order' => array('Studentschoolyear.sy' => 'DESC'));
		$this->set('schoolyears', $this->paginate());
	}
	
	
	public function registrareport($gradelevelid, $sectionid, $syid, $date){
		// increase memory limit in PHP 
		$this->loadModel = 'Schoolyear';
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		//$this->Student->Schoolyear->recursive = 0;
				
		$this->set('gradelevel', $this->Studentschoolyear->Gradelevel->findById($gradelevelid));
		$this->set('section', $this->Studentschoolyear->Gradesection->findById($sectionid));
		$this->set('sy', $this->Schoolyear->findById($syid));
		
		$conditions = array(
			'conditions' => array(				
				'Student.gradelevel_id'		=> $gradelevelid,			
				'Student.gradesection_id'	=> $sectionid,			
				'Student.schoolyear_id'		=> $syid			
			),
			'order'	=> array('Student.lastname' => 'ASC')
		);
		
		$this->set('students', $this->Student->find('all', $conditions));
	}
	
	
	public function report($gradelevelid, $sectionid, $syid){
		// increase memory limit in PHP 
		$this->layout = "default-long";
		ini_set('memory_limit', '512M');
		$this->Studentschoolyear->recursive = 0;
		
		//$this->set('teacher', $this->Student->Teacher->findById($teacherid));
		$this->set('gradelevel', $this->Studentschoolyear->Gradelevel->findById($gradelevelid));
		$this->set('section', $this->Studentschoolyear->Gradesection->findById($sectionid));
		$this->set('sy', $syid);
		
		$this->paginate = array(
			'conditions' => array(				
				'Studentschoolyear.gradelevel_id'		=> $gradelevelid,			
				'Studentschoolyear.gradesection_id'	=> $sectionid,			
				'Studentschoolyear.sy'		=> $syid			
			),
			'order'	=> array('Student.lastname' => 'ASC'),
			'limit'	=> 200
		);
		
		$this->set('students', $this->paginate());
	}
	
	public function gradelevelcount($id, $gender, $sy){
		$student = $this->Studentschoolyear->find('count', array('conditions' => array('Student.gender' => $gender, 'Studentschoolyear.gradelevel_id' => $id, 'Studentschoolyear.sy' => $sy)));
		return $student;
	}
	
}
