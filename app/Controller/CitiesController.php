<?php
App::uses('AppController', 'Controller');
/**
 * Cities Controller
 *
 * @property City $City
 * @property PaginatorComponent $Paginator
 */
class CitiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	
	public function listofcities(){				
		if(isset($this->request->data['Student']['province_id']) || !empty($this->request->data['Student']['province_id'])){
			$id = $this->request->data['Student']['province_id'];
			$this->set('sections', $this->City->find('all', array('conditions' => array('City.province_id' => $id), 'order' => array('City.name' => 'ASC'))));			
		}
		
		if(isset($this->request->data['Barangay']['province_id']) || !empty($this->request->data['Barangay']['province_id'])){
			$id = $this->request->data['Barangay']['province_id'];
			$this->set('sections', $this->City->find('all', array('conditions' => array('City.province_id' => $id), 'order' => array('City.name' => 'ASC'))));			
		}
		
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->City->recursive = 0;
		$this->set('cities', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->City->exists($id)) {
			throw new NotFoundException(__('Invalid city'));
		}
		$options = array('conditions' => array('City.' . $this->City->primaryKey => $id));
		$this->set('city', $this->City->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->City->create();
			if ($this->City->save($this->request->data)) {
				$this->Session->setFlash(__('The city has been saved.'), 'success_message');
				return $this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('The city could not be saved. Please, try again.'), 'error_message');
			}
		}
		$provinces = $this->City->Province->find('list');
		$this->set(compact('provinces'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->City->exists($id)) {
			throw new NotFoundException(__('Invalid city'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->City->save($this->request->data)) {
				$this->Session->setFlash(__('The city has been saved.'), 'success_message');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The city could not be saved. Please, try again.'), 'error_message');
			}
		} else {
			$options = array('conditions' => array('City.' . $this->City->primaryKey => $id));
			$this->request->data = $this->City->find('first', $options);
		}
		$provinces = $this->City->Province->find('list');
		$this->set(compact('provinces'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->City->id = $id;
		if (!$this->City->exists()) {
			throw new NotFoundException(__('Invalid city'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->City->delete()) {
			$this->Session->setFlash(__('The city has been deleted.'));
		} else {
			$this->Session->setFlash(__('The city could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
