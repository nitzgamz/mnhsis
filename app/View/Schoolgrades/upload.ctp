<?php echo $this->element('submenu/student'); ?>
<div class="schoolgrades form">
<h3 class="default"><?php echo __('Upload Grades'); ?></h3>
<?php echo $this->Form->create('Schoolgrade', array('enctype' => 'multipart/form-data'));?>
	<fieldset>

	<?php
		//echo '<label for="">Select Quarter</label>';
		//echo $this->Form->input('quarter', array('options' => array('1' => 'First Quarter', '2' => 'Second Quarter', '3' => 'Third Quarter', '4' => 'Fourth Quarter'), 'label' => ''));		
		echo '<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left">';
		echo '<label for="">Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => '', 'empty' => '---------- Select Grade level---------', 'id' => 'gradelevel_id'));
		echo '<label for="">Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => '', 'empty' => '---------- Select Section ---------', 'id' => 'gradesection_id'));
		//echo '<label for="">Teacher</label>';
		//echo $this->Form->input('teacher_id', array('label' => ''));	
		echo '<label for="">School Year</label>';
		//echo $this->Form->input('schoolyear_id', array('label' => ''));	
		echo '<select name="data[Schoolgrade][schoolyear]" class="normalinput2">';
			$schoolyears = array();
			for($i=date('Y')-10; $i<=date('Y')+1; $i++){			
				$schoolyears[] = $i;
			}
				foreach($schoolyears as $sy):
					echo '<option value="'.$sy.'">'.$sy.' - '.($sy + 1).'</option>';
				endforeach;
				echo '</select>';		
		
		echo '</div>';
		echo '<div class="col-md-3 col-lg-3 col-xs-3">';		
		
		echo '<label for="">Select File to Upload</label>';
		echo $this->Form->file('filetoupload', array('label' => ''));				
		echo $this->Form->input('user_id', array('default' => $userid, 'type' => 'hidden'));
		echo $this->Form->input('added', array('default' => date('Y-m-d'), 'type' => 'hidden'));
		echo $this->Form->submit(__('Upload File & Save'), array('class' => 'btn btn-primary')); 
		echo '</div>';
	?>
	</fieldset>
	<br />

</div>
<br /><br />

<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradelevel_id', 'update_id' => '#gradesection_id', 'controller_id' => 'gradesections', 'action_id' => 'getsectionlists', 'method' => 'post', 'form' => 'Schoolgrade')); ?>
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradesection_id', 'update_id' => '#studentlist', 'controller_id' => 'studentschoolyears', 'action_id' => 'getstudentlists', 'method' => 'post', 'form' => 'Schoolgrade'));