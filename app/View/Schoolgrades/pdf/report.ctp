<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
	<title>School Form 1</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style>
		html, body{margin: 20px 30px;}		
		table.table tr.table-information td{padding-left: 3px;}
		.clear{clear: both;}
		/*table {page-break-before: always; page-break-after: always;}*/
		/*@page{
			margin-top: 5px;
			margin-left: 20px;
			margin-bottom: 5px;
			margin-right: 20px;
		}*/
		table{border-collapse:collapse;}
		table,tbody,tr,th,td{margin:0;padding:0;}
		/*th,td{white-space:nowrap;}*/
		 #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; background-color: lightblue; }
    #footer .page:after { content: counter(page, upper-roman); }
	</style>
<body>	
<div id="header">
    <h1>ibmphp.blogspot.com</h1>
  </div>
  <div id="footer">
    
  </div>
<script type="text/php">
        /*if ( isset($pdf) ) {
            $font = Font_Metrics::get_font("helvetica", "bold");
            $pdf->page_text(30, 18, "Page {PAGE_NUM} out of {PAGE_COUNT}", $font, 8, array(255,0,0));

        }*/
    </script>
<?php
if(!empty($students)){
	foreach($students as $student):
?>
<table style="cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td style="width: 20%;"><img src="<?php echo APP; ?>/webroot/img/1.png" style="width: 90px;"/></td>
	<td style="width: 80%;">
		<h2 style="text-align: center; font: bold 13px arial, heveltica, sans-serif; padding: 0; margin: 0;">
			<div style="font-size: 12px;">Republika ng Pilipinas</div>
			Kagawaran ng Edukasyon
			<div style="font-size: 12px;">Rehiyon Caraga</div>
			<div style="font-size: 12px;">Sangay ng Bislig City</div>
			<div style="margin-top: 5px;">
			PALAGIANG TALAAN SA MABABANG PAARALAN<br />
			(ELEMENTARY SCHOOL PERMANENT RECORD)
			</div>
		</h2>	
	</td>
	<td style="width: 20%; vertical-align: top;"><img src="<?php echo APP; ?>/webroot/img/2.png" style="width: 130px; margin-right: 20px;"/></td>
</tr>	
</table>

<div style="width: 180px; position: absolute; right: 0; top: 85px;">
<table style="font: bold 10px arial, heveltica, sans-serif;" cellpadding="0" cellspacing="0" width="100%">
	<tr style="text-align: center;">		
		<td style="border: 1px solid #666;">1</td>
		<td style="border: 1px solid #666;">3</td>
		<td style="border: 1px solid #666;">2</td>
		<td style="border: 1px solid #666;">6</td>
		<td style="border: 1px solid #666;">3</td>
		<td style="border: 1px solid #666;">1</td>
		<td style="border: 1px solid #666;">0</td>
		<td style="border: 1px solid #666;">8</td>
		<td style="border: 1px solid #666;">0</td>
		<td style="border: 1px solid #666;">0</td>
		<td style="border: 1px solid #666;">0</td>
		<td style="border: 1px solid #666;">2</td>
	</tr>	
	
</table>
</div>

<div class="clear"></div>
<br />
<div>
	<table style="font: bold 10px arial, heveltica, sans-serif;" cellpadding="0" cellspacing="0" width="100%">
		<thead>
			<tr>
				<td>I. Pangalan</td>
				<td style="border-bottom: 1px solid #666;"><?php echo $student['Student']['firstname']; ?></td>
				<td style="border-bottom: 1px solid #666;"><?php echo $student['Student']['lastname']; ?></td>
				<td style="border-bottom: 1px solid #666;"><?php echo $student['Student']['middlename']; ?></td>
				<td style="text-align: right;">Sangay &nbsp;</td>
				<td style="border-bottom: 1px solid #666;">BISLIG CITY</td>
				<td style="text-align: right;">Paaralan &nbsp;</td>
				<td style="border-bottom: 1px solid #666; width: 100px;">SAN FERNANDO ES</td>
			</tr>
			<tr>
				<td>(Name)</td>			
				<td>(Lastname)</td>			
				<td>(Firstname)</td>			
				<td>(MI)</td>			
				<td>&nbsp;</td>		
				<td>Division</td>		
				<td>&nbsp;</td>		
				<td>(School)</td>		
			</tr>
			<tr><td colspan="8">&nbsp;</td></tr>
			<tr>
				<td>Kasarian</td>
				<td style="border-bottom: 1px solid #666;"><?php echo ($student['Student']['gender']=="M" ? "LALAKE" : "BABAE"); ?></td>
				<td style="text-align: right;">Petsa ng Kapanganakan &nbsp; </td>
				<td style="border-bottom: 1px solid #666;"><?php echo date('m/d/Y', strtotime($student['Student']['birthdate'])); ?></td>
				<td style="text-align: right;">Pook &nbsp; </td>
				<td style="border-bottom: 1px solid #666;"><?php echo $student['Student']['birthplace']; ?></td>
				<td style="text-align: right;">Petsa ng pagpasok &nbsp; </td>
				<td style="border-bottom: 1px solid #666;">&nbsp;</td>
			</tr>
			<tr>
				<td>(Sex)</td>	
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td style="width: 100px;">(Date of Birth)</td>			
				<td>&nbsp;</td>			
				<td>(Place)Bayan/<br /> Lalawigan/Lungsod</td>			
				<td>&nbsp;</td>		
				<td>Date of Entrance</td>		
			</tr>
			<tr>
				<td>Magulang/Tagapag-alaga</td>
				<td style="border-bottom: 1px solid #666;" colspan="2"><?php echo $student['Student']['father_fullname'].'/ '.$student['Student']['mother_fullname'].' / '.$student['Student']['guardian_fullname']; ?></td>								
				<td style="border-bottom: 1px solid #666;" colspan="3">&nbsp;</td>							
				<td style="border-bottom: 1px solid #666;" colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>	
				<td>Pangalan <br />( Name )</td>
				<td>&nbsp;</td>
				<td colspan="2">Tirahan <br /> (Address)</td>			
				<td>&nbsp;</td>			
				<td colspan="2">Hanapbuhay <br /> (Occupation)</td>									
			</tr>
			<tr><td colspan="8">&nbsp;</td></tr>
			<tr>
				<td colspan="8" style="font-weight: bold; font-size: 16px; text-align: center;">PAGUNLAD SA MABABANG PAARALAN </td>										
			</tr>
			<tr>
				<td colspan="8" style="text-align: center; font-size: 14px;">(ELEMENTARY SCHOOL PROGRESS)</td>										
			</tr>
		</thead>
	</table>
</div>
<div>
	<table style="font: bold 12px arial, heveltica, sans-serif; margin-top: 10px;" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td style="padding-right: 10px;">
				<?php echo $this->element('grade-table/grade1-table', array('width' => '100', 'studentid' => $student['Student']['id'])); ?>
			</td>
			<td style="padding-left: 10px;">
				<?php echo $this->element('grade-table/grade2-table', array('width' => '100')); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-right: 10px;">
				<?php echo $this->element('grade-table/grade3-table', array('width' => '100')); ?>
			</td>
			<td style="padding-left: 10px;">
				<?php echo $this->element('grade-table/grade4-table', array('width' => '100')); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-right: 10px;">
				<?php echo $this->element('grade-table/grade5-table', array('width' => '100')); ?>
			</td>
			<td style="padding-left: 10px;">
				<?php echo $this->element('grade-table/grade6-table', array('width' => '100')); ?>
			</td>
		</tr>
	</table>
</div>
<div>
	<?php echo $this->element('grade-table/ratings-table', array('width' => '100')); ?>
</div>
<?php endforeach; ?>
<?php } ?>
</body>
</html>