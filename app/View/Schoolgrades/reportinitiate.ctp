<?php //echo $this->element('submenu/student'); ?>
<div class="students form">
<div class="col-md-5 col-lg-5 col-xs-5 " style="background: #ccc; border: 1px solid #ccc; border-radius: 10px 10px; margin-top: 30px;">	
<h3 class="default">School Form 137</h3>
<?php echo $this->Form->create('Schoolgrade', array('url' => array('controller' => 'schoolgrades', 'action' => 'report', 'ext' => 'pdf')));?>
	
	<?php
		
		echo '<label for="">Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => '', 'empty' => '---------- Select Grade level---------', 'id' => 'gradelevel_id'));
		echo '<label for="">Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => '', 'empty' => '---------- Select Section ---------', 'id' => 'gradesection_id'));		
		if(!$download){ 
			echo '<h3>Student will be listed here</h3>';
		}
		echo '<div id="studentlist">';
			
		echo '</div>';
	?>
	
	<br />
	<div class="col-md-4 col-lg-4 col-xs-4 no-padding-left">
		<?php echo $this->Form->submit(__('Generate PDF Report'), array('class' => 'btn btn-success'));?>
	</div>	
	<?php 
		if($download){ 
			
			echo '<div class="col-md-4 col-lg-4 col-xs-4" style="margin-top: 5px;">';
				echo $this->Html->link('Download / View File', array('action' => 'report', 'ext' => 'pdf', '', $this->data['Schoolgrade']['gradelevel_id'], $this->data['Schoolgrade']['gradesection_id']), array('class' => 'btn btn-danger', 'target' => '_blank'));
			echo '</div>';
		}
	?>
	<div class="clear"></div>
	<br /><br />
</div>
</div>
<div class="clear"></div>
<br />
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradelevel_id', 'update_id' => '#gradesection_id', 'controller_id' => 'gradesections', 'action_id' => 'getsectionlists', 'method' => 'post', 'form' => 'Schoolgrade')); ?>
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradesection_id', 'update_id' => '#studentlist', 'controller_id' => 'studentschoolyears', 'action_id' => 'getstudentlists', 'method' => 'post', 'form' => 'Schoolgrade')); ?>
