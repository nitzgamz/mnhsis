<?php echo $this->element('submenu/student'); ?>
<div class="schoolgrades view">
	<h3 class="default"><?php  echo __('Student Grade Profile');?></h3>
	<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left">	
		<dd>
			<h3 style="background: #f4f2f2; border: 1px solid #ccc; padding: 5px 5px;"><?php echo $schoolgrade['Student']['lastname'].', '.$schoolgrade['Student']['firstname'].' '.$schoolgrade['Student']['middlename']; ?></h3>
		</dd>
	<table class="table table-striped table-hover table-condensed">
		<tr>
			<td><?php echo __('Grade Level'); ?></td>
			<td><?php echo h($schoolgrade['Gradelevel']['name']); ?></td>
		</tr>
		<tr>
			<td><?php echo __('Grade Section'); ?></td>
			<td><?php echo h($schoolgrade['Gradesection']['name']); ?></td>
		</tr>
		<tr>
			<td><?php echo __('Teacher'); ?></td>
			<td><?php echo  $schoolgrade['Teacher']['title'].' '.$schoolgrade['Teacher']['firstname'].' '.$schoolgrade['Teacher']['lastname']; ?></td>
		</tr>		
	
	</table>
	</div>
	<div class="col-md-8 col-lg-8 col-xs-8">
	<h3 class="default">S.Y. <?php echo h($schoolgrade['Schoolyear']['name']); ?></h3>
	<div class="col-md-5 col-lg-5 col-xs-5 no-padding-left">		
		<?php 
			$ginit = $schoolgrade['Schoolgrade']['grades']; 
			$fg = explode(",", $ginit);
		?>
		<table class="table table-striped table-hover table-condensed">
			<tr>
				<th colspan="5"><?php echo $fg[0]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[1]; ?></td>
				<td><?php echo $fg[2]; ?></td>
				<td><?php echo $fg[3]; ?></td>
				<td><?php echo $fg[4]; ?></td>
				<td><?php echo $fg[5]; ?></td>
			</tr>
			<tr>
				<th colspan="5"><?php echo $fg[6]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[7]; ?></td>
				<td><?php echo $fg[8]; ?></td>
				<td><?php echo $fg[9]; ?></td>
				<td><?php echo $fg[10]; ?></td>
				<td><?php echo $fg[11]; ?></td>
			</tr>
			<tr>
				<th colspan="5"><?php echo $fg[12]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[13]; ?></td>
				<td><?php echo $fg[14]; ?></td>
				<td><?php echo $fg[15]; ?></td>
				<td><?php echo $fg[16]; ?></td>
				<td><?php echo $fg[17]; ?></td>
			</tr>
			<tr>
				<th colspan="5"><?php echo $fg[18]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[19]; ?></td>
				<td><?php echo $fg[20]; ?></td>
				<td><?php echo $fg[21]; ?></td>
				<td><?php echo $fg[22]; ?></td>
				<td><?php echo $fg[23]; ?></td>
			</tr>
			<tr>
				<th colspan="5"><?php echo $fg[24]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[25]; ?></td>
				<td><?php echo $fg[26]; ?></td>
				<td><?php echo $fg[27]; ?></td>
				<td><?php echo $fg[28]; ?></td>
				<td><?php echo $fg[29]; ?></td>
			</tr>		
		</table>
	</div>
	<div class="col-md-5 col-lg-5 col-xs-5 no-padding-right">		
		<table class="table table-striped table-hover table-condensed">		
			
			<tr>
				<th colspan="5"><?php echo $fg[30]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[31]; ?></td>
				<td><?php echo $fg[32]; ?></td>
				<td><?php echo $fg[33]; ?></td>
				<td><?php echo $fg[34]; ?></td>
				<td><?php echo $fg[35]; ?></td>
			</tr>
			<tr>
				<th colspan="5"><?php echo $fg[36]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[37]; ?></td>
				<td><?php echo $fg[38]; ?></td>
				<td><?php echo $fg[39]; ?></td>
				<td><?php echo $fg[40]; ?></td>
				<td><?php echo $fg[41]; ?></td>
			</tr>
			<tr>
				<th colspan="5"><?php echo $fg[42]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[43]; ?></td>
				<td><?php echo $fg[44]; ?></td>
				<td><?php echo $fg[45]; ?></td>
				<td><?php echo $fg[46]; ?></td>
				<td><?php echo $fg[47]; ?></td>
			</tr>
			<tr>
				<th colspan="5"><?php echo $fg[48]; ?></th>
			</tr>
			<tr>
				<td>1st <br /> Qtr</td>
				<td>2nd <br /> Qtr</td>
				<td>3rd <br /> Qtr</td>
				<td>4th <br /> Qtr</td>
				<td>Total <br /> Average</td>
			</tr>
			<tr>
				<td><?php echo $fg[49]; ?></td>
				<td><?php echo $fg[50]; ?></td>
				<td><?php echo $fg[51]; ?></td>
				<td><?php echo $fg[52]; ?></td>
				<td><?php echo $fg[53]; ?></td>
			</tr>
			<tr>
				<td colspan="4"><?php echo __('Total Average'); ?></td>		
				<td><?php echo h($schoolgrade['Schoolgrade']['tgrade']); ?></td>	
			</tr>
			<tr>
				<td colspan="4"><?php echo __('General Average'); ?></td>
				<td><?php echo h($schoolgrade['Schoolgrade']['ggrade']); ?></td>
			</tr>
		</table>
	</div>
	</div>
	<div class="clear"></div>
</div>

