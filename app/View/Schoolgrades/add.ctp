<div class="schoolgrades form">
<?php echo $this->Form->create('Schoolgrade');?>
	<fieldset>
		<legend><?php echo __('Add Schoolgrade'); ?></legend>
	<?php
		echo $this->Form->input('student_id');
		echo $this->Form->input('schoolsubject_id');
		echo $this->Form->input('first_quarter');
		echo $this->Form->input('second_quarter');
		echo $this->Form->input('third_quarter');
		echo $this->Form->input('fourth_quarter');
		echo $this->Form->input('quarter_tgrade');
		echo $this->Form->input('tgrade');
		echo $this->Form->input('ggrade');
		echo $this->Form->input('sy');
		echo $this->Form->input('user_id');
		echo $this->Form->input('added');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Schoolgrades'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolsubjects'), array('controller' => 'schoolsubjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolsubject'), array('controller' => 'schoolsubjects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
