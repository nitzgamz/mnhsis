<div class="ratings form">
<?php echo $this->Form->create('Rating');?>
	<fieldset>
		<legend><?php echo __('Add Rating'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Ratings'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Studentratings'), array('controller' => 'studentratings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Studentrating'), array('controller' => 'studentratings', 'action' => 'add')); ?> </li>
	</ul>
</div>
