<div class="ratings view">
<h2><?php  echo __('Rating');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($rating['Rating']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($rating['Rating']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($rating['Rating']['description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Rating'), array('action' => 'edit', $rating['Rating']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Rating'), array('action' => 'delete', $rating['Rating']['id']), null, __('Are you sure you want to delete # %s?', $rating['Rating']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ratings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rating'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Studentratings'), array('controller' => 'studentratings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Studentrating'), array('controller' => 'studentratings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Studentratings');?></h3>
	<?php if (!empty($rating['Studentrating'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Student Id'); ?></th>
		<th><?php echo __('Rating Id'); ?></th>
		<th><?php echo __('Gradelevel Id'); ?></th>
		<th><?php echo __('Gradesection Id'); ?></th>
		<th><?php echo __('Quarter'); ?></th>
		<th><?php echo __('Rating'); ?></th>
		<th><?php echo __('Added'); ?></th>
		<th><?php echo __('Sy'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($rating['Studentrating'] as $studentrating): ?>
		<tr>
			<td><?php echo $studentrating['id'];?></td>
			<td><?php echo $studentrating['student_id'];?></td>
			<td><?php echo $studentrating['rating_id'];?></td>
			<td><?php echo $studentrating['gradelevel_id'];?></td>
			<td><?php echo $studentrating['gradesection_id'];?></td>
			<td><?php echo $studentrating['quarter'];?></td>
			<td><?php echo $studentrating['rating'];?></td>
			<td><?php echo $studentrating['added'];?></td>
			<td><?php echo $studentrating['sy'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'studentratings', 'action' => 'view', $studentrating['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'studentratings', 'action' => 'edit', $studentrating['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'studentratings', 'action' => 'delete', $studentrating['id']), null, __('Are you sure you want to delete # %s?', $studentrating['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Studentrating'), array('controller' => 'studentratings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
