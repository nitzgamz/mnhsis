<div class="mos form">
<?php $mos_name = $this->App->mos_name(true); ?>
<?php $mos_short = $this->App->mos_short(true); ?>

<?php echo $this->Form->create('Mo'); ?>
	<?php
		echo $this->Form->input('schoolyear_id', array(
				'label' => 'Select S.Y.', 
				'class' => 'form-control input-lg', 
				'div' 	=> '<div class="col-md-12"></div>',
				'after'	=> '<div class="clear separator20"></div>'
			)
		);
		foreach($mos_name as $key => $d):
	?>
		<div class="col-md-2 nopadding-left">
			<div class="panel panel-danger col-md-12 nopadding">
				<div class="panel-heading"><?php echo $d; ?></div>
				<div class="panel-body">
				<?php echo $this->Form->hidden('name.', array('default' => $d)); ?>
				<?php echo $this->Form->hidden('short.', array('default' => $mos_short[$key])); ?>
				<?php echo $this->Form->input('sdays.', array('label' => 'School Days', 'default' => 20)); ?>
				</div>
			</div>
		</div>
	<?php
		endforeach;
	?>
	<div class="clear separator20"></div>
<?php echo $this->App->formbtn('Save Settings'); ?>
</div>