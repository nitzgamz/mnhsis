<div class="mos index col-md-4 nopadding">
	<?php $headers = array("S.Y.", "Mos", "Short Name", "No. of SD"); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>
	
	<?php foreach ($mos as $mo): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($mo['Schoolyear']['name'], array('controller' => 'schoolyears', 'action' => 'view', $mo['Schoolyear']['id'])); ?>
		</td>
		<td><?php echo h($mo['Mo']['name']); ?>&nbsp;</td>
		<td><?php echo h($mo['Mo']['short']); ?>&nbsp;</td>
		
		<td><?php echo h($mo['Mo']['sdays']); ?>&nbsp;</td>
		<td class="actions">
			<?php $this->App->linkbtn('Edit', 'mos', 'edit', $mo['Mo']['id']); ?>
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $mo['Mo']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $mo['Mo']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $mo['Mo']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $mo['Mo']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
<div class="clear"></div>
<div class="actions">	
	<?php $this->App->linkbtn('Set New School Days for S.Y.', 'mos', 'add'); ?>
</div>
