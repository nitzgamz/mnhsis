<?php echo $this->element('submenu/settings'); ?>
<div class="mos form col-md-3 nopadding">
<?php echo $this->Form->create('Mo'); ?>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('label' => 'Month', 'class' => 'form-control input-sm', 'disabled' => true));
		echo $this->Form->input('short', array('label' => 'Short Name', 'class' => 'form-control input-sm',  'disabled' => true));
		echo $this->Form->input('schoolyear_id', array('label' => 'S.Y.', 'class' => 'form-control input-sm', 'disabled' => true));
		echo $this->Form->input('sdays', array('label' => 'Nos. of School Days', 'class' => 'form-control input-sm', ));
	?>

<?php echo $this->App->formbtn(__('Save Changes')); ?>
</div>
<?php $this->App->clear(); ?>