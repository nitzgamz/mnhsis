<div class="mos view">
<h2><?php echo __('Mo'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mo['Mo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($mo['Mo']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short'); ?></dt>
		<dd>
			<?php echo h($mo['Mo']['short']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Schoolyear'); ?></dt>
		<dd>
			<?php echo $this->Html->link($mo['Schoolyear']['name'], array('controller' => 'schoolyears', 'action' => 'view', $mo['Schoolyear']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sdays'); ?></dt>
		<dd>
			<?php echo h($mo['Mo']['sdays']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Mo'), array('action' => 'edit', $mo['Mo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Mo'), array('action' => 'delete', $mo['Mo']['id']), array(), __('Are you sure you want to delete # %s?', $mo['Mo']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Mos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mo'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolyears'), array('controller' => 'schoolyears', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolyear'), array('controller' => 'schoolyears', 'action' => 'add')); ?> </li>
	</ul>
</div>
