<?php echo $this->element('submenu/places'); ?>
<div class="provinces form col-md-4 nopadding">
<?php echo $this->Form->create('City'); ?>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('province_id');
		echo $this->Form->input('name');
	?>

<?php echo $this->App->formbtn(__('Save Changes')); ?>
</div>
