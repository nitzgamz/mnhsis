<?php echo $this->element('submenu/places'); ?>
<div class="provinces index col-md-6 nopadding">
	<?php $headers = array("Name", "Province"); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>
	<?php foreach ($cities as $city): ?>
	<tr>
		<td><?php echo h($city['City']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $city['Province']['name']; ?>
		</td>
		
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $city['City']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $city['City']['id'])); ?>
			<?php echo $this->App->linkbtn(__('Edit'), 'cities', 'edit', $city['City']['id']); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $city['City']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $city['City']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
