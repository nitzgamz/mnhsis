<?php echo $this->element('submenu/places'); ?>
<div class="provinces form col-md-4 nopadding">
<?php echo $this->Form->create('Barangay'); ?>
	
	<?php
		echo $this->Form->input('province_id', array('id' => 'province_id'));
		echo $this->Form->input('city_id', array('id' => 'city_id'));
		echo $this->Form->input('name');
	?>

<?php echo $this->App->formbtn(__('Save')); ?>
</div>

<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#province_id', 'update_id' => '#city_id', 'controller_id' => 'cities', 'action_id' => 'listofcities', 'method' => 'post', 'form' => 'Barangay')); ?>