<div class="barangays view">
<h2><?php echo __('Barangay'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($barangay['Barangay']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Province'); ?></dt>
		<dd>
			<?php echo $this->Html->link($barangay['Province']['name'], array('controller' => 'provinces', 'action' => 'view', $barangay['Province']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo $this->Html->link($barangay['City']['name'], array('controller' => 'cities', 'action' => 'view', $barangay['City']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($barangay['Barangay']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Barangay'), array('action' => 'edit', $barangay['Barangay']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Barangay'), array('action' => 'delete', $barangay['Barangay']['id']), array(), __('Are you sure you want to delete # %s?', $barangay['Barangay']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Barangays'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Barangay'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Provinces'), array('controller' => 'provinces', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Province'), array('controller' => 'provinces', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
	</ul>
</div>
