<?php echo $this->element('submenu/places'); ?>
<div class="provinces index col-md-6 nopadding">
	<?php $headers = array("Name", "Province", "Mucipality"); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>
	<?php foreach ($barangays as $barangay): ?>
	<tr>
		<td><?php echo h($barangay['Barangay']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $barangay['Province']['name']; ?>
		</td>
		<td>
			<?php echo $barangay['City']['name']; ?>
		</td>
		
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $barangay['Barangay']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $barangay['Barangay']['id'])); ?>
			<?php echo $this->App->linkbtn(__('Edit'), 'barangays', 'edit', $barangay['Barangay']['id']); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $barangay['Barangay']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $barangay['Barangay']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>

