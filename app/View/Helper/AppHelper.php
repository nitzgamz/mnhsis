<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
	
	public $helpers = array('Form', 'Html');
	public $newarr  = array();
	
	public function linkbtn($text, $controller, $view, $param1=null, $param2=null){
		$link =  $this->Html->link($text, array('controller' => $controller, 'action' => $view, $param1, $param2), array('class' => 'btn btn-xs btn-primary', 'escape' => false));
		echo $link;
	}
	
	public function modal_link($text, $target, $class=null, $param1=null, $param2=null){
		$link =  $this->Html->link($text, '#', array('class' => 'btn btn-xs '.$class.' btn-primary', 'escape' => false, 'param1' => $param1, 'param2' => $param2, 'data-toggle' => 'modal', 'data-target' => '.'.$target));
		echo $link;
	}
	
	public function modal_link_a($text, $title, $controller, $action, $param1=null, $param2=null){
		$link =  $this->Html->link($text, array('controller' => $controller, 'action' => $action, $param1, $param2), array('class' => 'btn btn-xs btn-primary modalink', 'escape' => false, 'param1' => $param1, 'param2' => $param2, 'title' => $title));
		echo $link;
	}
	
	public function roptions(){
		$arr = array(
			"Passed" => "Passed",
			"Failed" => "Failed"
		);
		
		return $arr;
	}
	
	public function modal_link_ad($text, $target){
		$link =  $this->Html->link($text, '#', array('class' => 'btn btn-sm btn-primary', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '.'.$target));
		echo $link;
	}
	
	
	public function title($title){
		echo '<h4>'.$title.'</h4>';
	}
	
	public function clear($sep=null){
		$sep = !empty($sep) ? $sep : '20';
		echo '<div class="clear separator'.$sep.'"></div>';
	}
	
	public function formbtn($text, $type='lg', $class='primary'){
		$btn = array(
			'label' => $text,
			'class' => 'btn btn-lg btn-'.$class,
			'after'	=> '<div class="clear separator20"></div>',
			'before'=> '<div class="clear separator20"></div>',
			'escape'	=> true
		);
		
		return $this->Form->end($btn, true);
		
	}
	
	
	

	
	public function mos_name($key=false){
		$mos = array(
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December",
			"January",
			"Fenruary",
			"March",
		);
		
		foreach($mos as $m => $i):
			$newarr[$i] = $i;
		endforeach;
		
		if($key){
			return $mos;
		}else{
			return $newarr;
		}
		
	}
	
	
	public function mos_short($key=false){
		$mos = array(
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Oct",
			"Nov",
			"Dec",
			"Jan",
			"Feb",
			"Mar",
		);
		
		foreach($mos as $m => $i):
			$newarr[$i] = $i;
		endforeach;
		
		if($key){
			return $mos;
		}else{
			return $newarr;
		}
	
	}
	
	public function computeAge($birthday=null){
		 $age = floor((time() - strtotime($birthday)) / 31556926);
		 return $age;
	}	
	
	
	public function StudentReportCard($student, $sy, $teacher){
		$details = $this->requestAction(array('controller' => 'attendances', 'action' => 'get_report_card', $student, $sy, $teacher));
		return $details;
	}
	
	public function StudentPermanentCard($student, $sy, $teacher){
		$details = $this->requestAction(array('controller' => 'periodicratings', 'action' => 'get_report_card', $student, $sy, $teacher));
		return $details;
	}
	
	public function getSchoolyear(){
		$details = $this->requestAction(array('controller' => 'schoolyears', 'action' => 'getlist'));
		return $details;
	}
	
	public function getGradelevel(){
		$details = $this->requestAction(array('controller' => 'gradelevels', 'action' => 'getlist'));
		return $details;
	}
	
	public function getTeacherGradelevel($type=null){
		$details = $this->requestAction(array('controller' => 'teachers', 'action' => 'getTeacherGradelevel', $type));
		return $details;
	}

	
}
