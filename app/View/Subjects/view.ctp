<div class="subjects view">
<h2><?php echo __('Subject'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($subject['Subject']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($subject['Subject']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short'); ?></dt>
		<dd>
			<?php echo h($subject['Subject']['short']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($subject['Subject']['description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Subject'), array('action' => 'edit', $subject['Subject']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Subject'), array('action' => 'delete', $subject['Subject']['id']), array(), __('Are you sure you want to delete # %s?', $subject['Subject']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Subjects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subject'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Periodicratings'), array('controller' => 'periodicratings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Periodicrating'), array('controller' => 'periodicratings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Periodicratings'); ?></h3>
	<?php if (!empty($subject['Periodicrating'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Student Id'); ?></th>
		<th><?php echo __('Gradelevel Id'); ?></th>
		<th><?php echo __('Gradesection Id'); ?></th>
		<th><?php echo __('Schoolyear Id'); ?></th>
		<th><?php echo __('Teacher Id'); ?></th>
		<th><?php echo __('Subject Id'); ?></th>
		<th><?php echo __('1st Qtr'); ?></th>
		<th><?php echo __('2nd Qtr'); ?></th>
		<th><?php echo __('3rd Qtr'); ?></th>
		<th><?php echo __('4th Qtr'); ?></th>
		<th><?php echo __('Rating'); ?></th>
		<th><?php echo __('Action'); ?></th>
		<th><?php echo __('Earned'); ?></th>
		<th><?php echo __('Added'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($subject['Periodicrating'] as $periodicrating): ?>
		<tr>
			<td><?php echo $periodicrating['id']; ?></td>
			<td><?php echo $periodicrating['student_id']; ?></td>
			<td><?php echo $periodicrating['gradelevel_id']; ?></td>
			<td><?php echo $periodicrating['gradesection_id']; ?></td>
			<td><?php echo $periodicrating['schoolyear_id']; ?></td>
			<td><?php echo $periodicrating['teacher_id']; ?></td>
			<td><?php echo $periodicrating['subject_id']; ?></td>
			<td><?php echo $periodicrating['1st_qtr']; ?></td>
			<td><?php echo $periodicrating['2nd_qtr']; ?></td>
			<td><?php echo $periodicrating['3rd_qtr']; ?></td>
			<td><?php echo $periodicrating['4th_qtr']; ?></td>
			<td><?php echo $periodicrating['rating']; ?></td>
			<td><?php echo $periodicrating['action']; ?></td>
			<td><?php echo $periodicrating['earned']; ?></td>
			<td><?php echo $periodicrating['added']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'periodicratings', 'action' => 'view', $periodicrating['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'periodicratings', 'action' => 'edit', $periodicrating['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'periodicratings', 'action' => 'delete', $periodicrating['id']), array(), __('Are you sure you want to delete # %s?', $periodicrating['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Periodicrating'), array('controller' => 'periodicratings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
