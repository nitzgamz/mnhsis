<?php echo $this->element('submenu/subjects'); ?>
<?php $this->App->title('Add Subject'); ?>
<div class="subjects form col-md-3 nopadding">
<?php echo $this->Form->create('Subject'); ?>

	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('short', array('default' => 'N/A'));
		echo $this->Form->input('description', array('default' => 'N/A'));
	?>

<?php echo $this->App->formbtn('Save'); ?>
</div>

