<?php echo $this->element('submenu/subjects'); ?>
<div class="subjects index col-md-6 nopadding">
	<?php $headers = array("Name"); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>

	<?php foreach ($subjects as $subject): ?>
	<tr>

		<td><?php echo h($subject['Subject']['name']); ?>&nbsp;</td>
	
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $subject['Subject']['id'])); ?>
			<?php echo $this->App->linkbtn('Edit', 'subjects', 'edit', $subject['Subject']['id']); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $subject['Subject']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $subject['Subject']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $subject['Subject']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
<?php $this->App->clear(); ?>
