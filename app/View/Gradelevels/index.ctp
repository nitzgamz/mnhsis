<div class="gradelevels index col-md-6 nopadding">
	<?php echo $this->element('submenu/subjects'); ?>
	<?php $headers = array("Grade Level", "Nos. of Sections", "Nos. of Teacher", "Nos. of Student"); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>

	<?php
	$i = 0;
	foreach ($gradelevels as $gradelevel): ?>
	<tr>		
		<td><?php echo h($gradelevel['Gradelevel']['name']); ?>&nbsp;</td>
		<!--<td>
			<?php 
				/*$students = $this->requestAction(array('controller' => 'students', 'action' => 'getallstudents', $gradelevel['Gradelevel']['id']));
				if(!empty($students)){
					echo count($students);
				}else{
					echo '0';
				}*/
			 ?>
		</td>-->
		<td>
			<?php 
				$sections = $this->requestAction(array('controller' => 'gradesections', 'action' => 'getallsections', $gradelevel['Gradelevel']['id']));
				if(!empty($sections)){
					echo count($sections);
				}else{
					echo '0';
				}
			 ?>
		</td>
		<td><?php echo count($gradelevel['Teacher']); ?>&nbsp;</td>
		<td><?php echo count($gradelevel['Student']); ?>&nbsp;</td>
		<!--<td>
			<?php 
				/*$teachers = $this->requestAction(array('controller' => 'teachers', 'action' => 'getallteachers', $gradelevel['Gradelevel']['id']));
				if(!empty($teachers)){
					echo '<ul>';
					foreach($teachers as $teacher):
						echo '<li>'.$teacher['Teacher']['title'] .' '. $teacher['Teacher']['firstname'] .' '.$teacher['Teacher']['lastname'].'</li>';
					endforeach;
					echo '</ul>';
				}else{
					echo '0';
				}*/
			 ?>
		</td>-->
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $gradelevel['Gradelevel']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $gradelevel['Gradelevel']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false)); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $gradelevel['Gradelevel']['id']), null, __('Are you sure you want to delete # %s?', $gradelevel['Gradelevel']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<?php //echo $this->element('bottom.navigation'); ?>
</div>

