<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
	<title>School Form 1</title>
	<meta name="dompdf.view" content="Fit"/>
	<meta name="Author" content="S&amp;S Enterprises"/>
	<meta name="description" content="Wish List Quote Request" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style>
		html, body{margin: 30px 30px;}		
		/*table {page-break-before: always; page-break-after: always;}*/
		@page{
			margin-top: 5px;
			margin-left: 20px;
			margin-bottom: 5px;
			margin-right: 20px;
		}
		table{border-collapse:collapse;}
		table,tbody,tr,th,td{margin:0;padding:0;}
		/*th,td{white-space:nowrap;}*/
		 #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; background-color: lightblue; }
    #footer .page:after { content: counter(page, upper-roman); }
	</style>
<body>	
<div id="header">
    <h1>ibmphp.blogspot.com</h1>
  </div>
  <div id="footer">
    
  </div>
<script type="text/php">
        /*if ( isset($pdf) ) {
            $font = Font_Metrics::get_font("helvetica", "bold");
            $pdf->page_text(30, 18, "Page {PAGE_NUM} out of {PAGE_COUNT}", $font, 8, array(255,0,0));

        }*/
    </script>
	
<h2 style="text-align: center; font: bold 18px arial, heveltica, sans-serif; padding-bottom: 0; margin-bottom: 0;">
	School Form 6 ( SF 6 ) Summarized Report on Promotion & Level of Profeciency
	<div style="font-size: 10px; font-style: italic;">( This replaced Form 20 )</div>
</h2>	

<table style="font: bold 12px arial, heveltica, sans-serif;" cellpadding="2" cellspacing="2" width="100%">
	
	<tr>
		<td rowspan="4" style="width: 150px; vertical-align: top;">
			<img src="<?php echo APP; ?>/webroot/img/1.png" style="width: 80%;"/>
		</td>
		<td style="text-align: right;">Region</td><td style="border: 1px solid #666; text-align: left;">CARAGA</td>
		<td style="text-align: right;">Division</td><td style="border: 1px solid #666; text-align: left;">BISLIG CITY</td>
		<td style="text-align: right;">District</td><td style="border: 1px solid #666; text-align: left;">BISLIG I</td>
		<td rowspan="4" style="width: 240px; vertical-align: top; text-align: right;">
			<img src="<?php echo APP; ?>/webroot/img/2.png" style="width: 80%;"/>
		</td>
	</tr>
	<tr>
		<td style="text-align: right;">School ID</td><td style="border: 1px solid #666;">132609</td>
		<td style="text-align: right;">School Year</td><td style="border: 1px solid #666;"><?php echo $sy.' - '.($sy+1); ?></td>
		<td style="text-align: right;">Curriculum</td><td style="border: 1px solid #666; text-transform: uppercase;">RBEC</td>			
	</tr>
	<tr>
		<td style="text-align: right;">School Name</td><td colspan="5" style="border: 1px solid #666;">SAN FERNANDO ELEMENTARY SCHOOL</td>
	</tr>
	
	
</table>


<table style="font: bold 10px arial, heveltica, sans-serif; border-top: 2px solid #000; border: 1px solid #000;" cellpadding="3" cellspacing="0" width="100%">
	<tr>
		<td style="border-bottom: 1px solid #666;">SUMMARY TABLE</td>
		<td style="border-bottom: 1px solid #666;" colspan="3">GRADE 1</td>
		<td style="border-bottom: 1px solid #666;" colspan="3">GRADE 2</td>
		<td style="border-bottom: 1px solid #666;" colspan="3">GRADE 3</td>
		<td style="border-bottom: 1px solid #666;" colspan="3">GRADE 4</td>
		<td style="border-bottom: 1px solid #666;" colspan="3">GRADE 5</td>
		<td style="border-bottom: 1px solid #666;" colspan="3">GRADE 6</td>
	</tr>
	<?php
		$grade1M = $this->Externalfunction->gradelevelcount('1', 'M', $sy);
		$grade2M = $this->Externalfunction->gradelevelcount('3', 'M', $sy);
		$grade3M = $this->Externalfunction->gradelevelcount('4', 'M', $sy);
		$grade4M = $this->Externalfunction->gradelevelcount('5', 'M', $sy);
		$grade5M = $this->Externalfunction->gradelevelcount('6', 'M', $sy);
		$grade6M = $this->Externalfunction->gradelevelcount('2', 'M', $sy);
		$grade7M = 0;
		$grade8M = 0;
		$grade9M = 0;
		$grade10M = 0;
		$grade11M = 0;
		$grade12M = 0;
		
		/*
		$grade7M = $this->Externalfunction->gradelevelcount('7', 'M', $sy);
		$grade8M = $this->Externalfunction->gradelevelcount('8', 'M', $sy);
		$grade9M = $this->Externalfunction->gradelevelcount('9', 'M', $sy);
		$grade10M = $this->Externalfunction->gradelevelcount('10', 'M', $sy);
		$grade11M = $this->Externalfunction->gradelevelcount('11', 'M', $sy);
		$grade12M = $this->Externalfunction->gradelevelcount('12', 'M', $sy);
		*/
		
		$grade1F = $this->Externalfunction->gradelevelcount('1', 'F', $sy);
		$grade2F = $this->Externalfunction->gradelevelcount('3', 'F', $sy);
		$grade3F = $this->Externalfunction->gradelevelcount('4', 'F', $sy);
		$grade4F = $this->Externalfunction->gradelevelcount('5', 'F', $sy);
		$grade5F = $this->Externalfunction->gradelevelcount('6', 'F', $sy);
		$grade6F = $this->Externalfunction->gradelevelcount('2', 'F', $sy);
		$grade7F = 0;
		$grade8F = 0;
		$grade9F = 0;
		$grade10F = 0;
		$grade11F = 0;
		$grade12F = 0;
		
		/*
		$grade7F = $this->Externalfunction->gradelevelcount('7', 'F', $sy);
		$grade8F = $this->Externalfunction->gradelevelcount('8', 'F', $sy);
		$grade9F = $this->Externalfunction->gradelevelcount('9', 'F', $sy);
		$grade10F = $this->Externalfunction->gradelevelcount('10', 'F', $sy);
		$grade11F = $this->Externalfunction->gradelevelcount('11', 'F', $sy);
		$grade12F = $this->Externalfunction->gradelevelcount('12', 'F', $sy);
		*/
		
	?>
	<tr>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">M</td>
		<td style="border-bottom: 1px solid #666;">F</td>
		<td style="border-bottom: 1px solid #666;">TOTAL</td>
		<td style="border-bottom: 1px solid #666;">M</td>
		<td style="border-bottom: 1px solid #666;">F</td>
		<td style="border-bottom: 1px solid #666;">TOTAL</td>
		<td style="border-bottom: 1px solid #666;">M</td>
		<td style="border-bottom: 1px solid #666;">F</td>
		<td style="border-bottom: 1px solid #666;">TOTAL</td>
		<td style="border-bottom: 1px solid #666;">M</td>
		<td style="border-bottom: 1px solid #666;">F</td>
		<td style="border-bottom: 1px solid #666;">TOTAL</td>
		<td style="border-bottom: 1px solid #666;">M</td>
		<td style="border-bottom: 1px solid #666;">F</td>
		<td style="border-bottom: 1px solid #666;">TOTAL</td>
		<td style="border-bottom: 1px solid #666;">M</td>
		<td style="border-bottom: 1px solid #666;">F</td>
		<td style="border-bottom: 1px solid #666;">TOTAL</td>
	</tr>
	<tr>
		<td style="border-bottom: 1px solid #666;">PROMOTED</td>
		<td style="border-bottom: 1px solid #666;"><?php echo $grade1M + $grade7M; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $grade1F + $grade7F; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo (($grade1M + $grade7M) + ($grade1F + $grade7F)); ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $grade2M + $grade8M; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $grade2F + $grade8F; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo (($grade2M + $grade8M) + ($grade2F + $grade8F)); ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $grade3M + $grade9M; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $grade3F + $grade9F; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo (($grade3M + $grade9M) + ($grade3F + $grade9F)); ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $grade4M + $grade10M; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $grade4F + $grade10F; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo (($grade4M + $grade10M) + ($grade4F + $grade10F)); ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $grade5M + $grade11M; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $grade5F + $grade11F; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo (($grade5M + $grade11M) + ($grade5F + $grade11F)); ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $grade6M + $grade12M; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $grade6F + $grade12F; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo (($grade6M + $grade12M) + ($grade6F + $grade12F)); ?></td>
	</tr>
	<tr>
		
		<td style="border-bottom: 1px solid #666;">IRREGULAR</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
		<td style="border-bottom: 1px solid #666;">&nbsp;</td>
	</tr>
	<tr>
		<td>RETAINED</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">LEVEL OF <br />PROFECIENCY</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">M</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">F</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">TOTAL</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">M</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">F</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">TOTAL</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">M</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">F</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">TOTAL</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">M</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">F</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">TOTAL</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">M</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">F</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">TOTAL</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">M</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">F</td>
		<td style="border-bottom: 2px solid #000; border-top: 2px solid #000;">TOTAL</td>
	</tr>
	<tr>
		
		<?php
			$m1_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'M', 1, 7, $sy);
			$f1_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'F', 1, 7, $sy);
			
			$m2_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'M', 3, 8, $sy);
			$f2_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'F', 3, 8, $sy);
			
			$m3_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'M', 4, 9, $sy);
			$f3_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'F', 4, 9, $sy);
			
			$m4_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'M', 5, 10, $sy);
			$f4_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'F', 5, 10, $sy);
			
			$m5_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'M', 6, 11, $sy);
			$f5_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'F', 6, 11, $sy);
			
			$m6_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'M', 2, 12, $sy);
			$f6_74 = $this->Externalfunction->studentprofeciencyall('74', '<=', 'F', 2, 12, $sy);
		?>
		<td style="border-bottom: 1px solid #666;">Nos. of BEGINNNING<br />(B: 74% and below)</td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f1_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_74 + $f1_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f2_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_74 + $f2_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f3_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_74 + $f3_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f4_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_74 + $f4_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f5_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_74 + $f5_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f6_74; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_74 + $f6_74; ?></td>
	</tr>
	<tr>
		<?php			
			$m1_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'M', 1, 7, $sy);			
			$f1_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'F', 1, 7, $sy);

			$m2_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'M', 3, 8, $sy);			
			$f2_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'F', 3, 8, $sy);	

			$m3_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'M', 4, 9, $sy);			
			$f3_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'F', 4, 9, $sy);	
			
			$m4_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'M', 5, 10, $sy);			
			$f4_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'F', 5, 10, $sy);	
			
			$m5_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'M', 6, 11, $sy);			
			$f5_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'F', 6, 11, $sy);	
			
			$m6_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'M', 2, 12, $sy);			
			$f6_75 = $this->Externalfunction->studentprofeciencyall2('75', '79', 'F', 2, 12, $sy);	
		?>
		<td style="border-bottom: 1px solid #666;">Nos. of DEVELOPING <br />(D: 75%-79%)</td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f1_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_75 + $f1_75; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f2_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_75 + $f2_75; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f3_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_75 + $f3_75; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f4_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_75 + $f4_75; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f5_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_75 + $f5_75; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f6_75; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_75 + $f6_75; ?></td>
	</tr>
	<tr>
		<?php			
			$m1_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'M', 1, 7, $sy);			
			$f1_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'F', 1, 7, $sy);

			$m2_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'M', 3, 8, $sy);			
			$f2_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'F', 3, 8, $sy);	

			$m3_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'M', 4, 9, $sy);			
			$f3_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'F', 4, 9, $sy);	
			
			$m4_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'M', 5, 10, $sy);			
			$f4_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'F', 5, 10, $sy);	
			
			$m5_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'M', 6, 11, $sy);			
			$f5_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'F', 6, 11, $sy);	
			
			$m6_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'M', 2, 12, $sy);			
			$f6_80 = $this->Externalfunction->studentprofeciencyall2('80', '84', 'F', 2, 12, $sy);	
		?>
		
		<td style="border-bottom: 1px solid #666;">Nos. of APPROACHING PROFICIENCY <br /> (AP: 80%-84%)</td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f1_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_80 + $f1_80; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f2_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_80 + $f2_80; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f3_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_80 + $f3_80; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f4_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_80 + $f4_80; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f5_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_80 + $f5_80; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f6_80; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_80 + $f6_80; ?></td>
		
	</tr>
	<tr>
		<?php			
			$m1_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'M', 1, 7, $sy);			
			$f1_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'F', 1, 7, $sy);

			$m2_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'M', 3, 8, $sy);			
			$f2_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'F', 3, 8, $sy);	

			$m3_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'M', 4, 9, $sy);			
			$f3_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'F', 4, 9, $sy);	
			
			$m4_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'M', 5, 10, $sy);			
			$f4_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'F', 5, 10, $sy);	
			
			$m5_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'M', 6, 11, $sy);			
			$f5_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'F', 6, 11, $sy);	
			
			$m6_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'M', 2, 12, $sy);			
			$f6_85 = $this->Externalfunction->studentprofeciencyall2('85', '89', 'F', 2, 12, $sy);	
		?>
		
		<td style="border-bottom: 1px solid #666;">Nos. of PROFICIENT <br />(P: 85% -89%)</td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f1_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_85 + $f1_85; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f2_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_85 + $f2_85; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f3_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_85 + $f3_85; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f4_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_85 + $f4_85; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f5_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_85 + $f5_85; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f6_85; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_85 + $f6_85; ?></td>
		
	</tr>
	<tr>
		
		<?php
			$m1_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'M', 1, 7, $sy);
			$f1_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'F', 1, 7, $sy);
			
			$m2_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'M', 3, 8, $sy);
			$f2_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'F', 3, 8, $sy);
			
			$m3_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'M', 4, 9, $sy);
			$f3_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'F', 4, 9, $sy);
			
			$m4_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'M', 5, 10, $sy);
			$f4_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'F', 5, 10, $sy);
			
			$m5_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'M', 6, 11, $sy);
			$f5_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'F', 6, 11, $sy);
			
			$m6_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'M', 2, 12, $sy);
			$f6_90 = $this->Externalfunction->studentprofeciencyall('90', '<=', 'F', 2, 12, $sy);
		?>
		
		<td style="border-bottom: 1px solid #666;">Nos. of ADVANCED <br /> (A: 90%  and above)</td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f1_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m1_90 + $f1_90; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f2_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m2_90 + $f1_90; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f3_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m3_90 + $f2_90; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f4_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m4_90 + $f4_90; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f5_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m5_90 + $f5_90; ?></td>
		
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $f6_90; ?></td>
		<td style="border-bottom: 1px solid #666;"><?php echo $m6_90 + $f6_90; ?></td>
		
	</tr>
	<tr>
		<td style="border-top: 3px solid #666;">TOTAL</td>
		<td style="border-top: 3px solid #666;">
			<?php echo $m1_74 + $m1_75 + $m1_80 + $m1_85 + $m1_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo $f1_74 + $f1_75 + $f1_80 + $f1_85 + $f1_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo ($m1_74 + $f1_74) + ($m1_75 + $f1_75) + ($m1_80 + $f1_80) + ($m1_85 + $f1_85)  + ($m1_90 + $f1_90); ?>
		</td>
		
		<td style="border-top: 3px solid #666;">
			<?php echo $m2_74 + $m2_75 + $m2_80 + $m2_85 + $m2_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo $f2_74 + $f2_75 + $f2_80 + $f2_85 + $f2_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo ($m2_74 + $f2_74) + ($m2_75 + $f2_75) + ($m2_80 + $f2_80) + ($m2_85 + $f2_85)  + ($m2_90 + $f2_90); ?>
		</td>
		
		<td style="border-top: 3px solid #666;">
			<?php echo $m3_74 + $m3_75 + $m3_80 + $m3_85 + $m3_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo$f3_74 + $f3_75 + $f3_80 + $f3_85 + $f3_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo ($m3_74 + $f3_74) + ($m3_75 + $f3_75) + ($m3_80 + $f3_80) + ($m3_85 + $f3_85)  + ($m3_90 + $f3_90); ?>
		</td>
		
		<td style="border-top: 3px solid #666;">
			<?php echo  $m4_74 + $m4_75 + $m4_80 + $m4_85 + $m4_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo  $f4_74 + $f4_75 + $f4_80 + $f4_85 + $f4_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo  ($m4_74 + $f4_74) + ($m4_75 + $f4_75) + ($m4_80 + $f4_80) + ($m4_85 + $f4_85)  + ($m4_90 + $f4_90); ?>
		</td>
		
		<td style="border-top: 3px solid #666;">
			<?php echo  $m5_74 + $m5_75 + $m5_80 + $m5_85 + $m5_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo $f5_74 + $f5_75 + $f5_80 + $f5_85 + $f5_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo ($m5_74 + $f5_74) + ($m5_75 + $f5_75) + ($m5_80 + $f5_80) + ($m5_85 + $f5_85)  + ($m5_90 + $f5_90); ?>
		</td>
		
		<td style="border-top: 3px solid #666;">
			<?php echo $m6_74 + $m6_75 + $m6_80 + $m6_85 + $m6_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo $f6_74 + $f6_75 + $f6_80 + $f6_85 + $f6_90; ?>
		</td>
		<td style="border-top: 3px solid #666;">
			<?php echo ($m6_74 + $f6_74) + ($m6_75 + $f6_75) + ($m6_80 + $f6_80) + ($m6_85 + $f6_85)  + ($m6_90 + $f6_90); ?>
		</td>
				
	</tr>
</table>

<table style="font: bold 12px arial, heveltica, sans-serif; margin-top: 20px;" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td>Prepared & Submitted by:</td>
		<td style="border-bottom: 1px solid #000;">&nbsp;</td>
		
		
		
		<td>Reviewed & Validated by:</td>
		<td style="border-bottom: 1px solid #000;">&nbsp;</td>
		

		<td>Noted by:</td>
		<td style="border-bottom: 1px solid #000;">&nbsp;</td>
		
	</tr>
	<tr>
		<td>&nbsp;</td>		
		<td style="text-align: center;">SCHOOL HEAD <br /> ( Name & Signature )</td>
		
		<td>&nbsp;</td>		
		<td style="text-align: center;">DIVISION REPRESENTATIVE <br /> ( Name & Signature )</td>
				
		<td>&nbsp;</td>
		<td style="text-align: center;">SCHOOLS DIVISION SUPERINTENDENT <br /> ( Name & Signature )</td>
	</tr>
</table>
<table style="font: bold 11px arial, heveltica, sans-serif; " cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td>
			GUIDELINES:<br /><br />
			<ol>
				<li> After receiving and validating the Report for Promotion submitted by the class adviser, the School Head shall compute the Total for Grade Level in order to reflect the result in each data field.</li>
				<li>This report together with the copy of Report for Promotion submitted by the class adviser shall be forwarded to the Division Office by the end of the school year.</li>
				<li> The Report on Promotion per  Grade Level is reflected in the End of School Year Report of GESP/GSSP</li>
				<li>Protocols of validation & submission will remain under the discretion of the Schools Division Superintendent</li>
				
			</ol>
		</td>
	</tr>
</table>