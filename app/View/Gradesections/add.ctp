<?php echo $this->element('submenu/subjects'); ?>
<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left">
<div class="gradesections form">
<?php echo $this->Form->create('Gradesection');?>

	
	<?php
		echo '<label for="">Select Grade</label>';
		echo $this->Form->input('gradelevel_id', array('label' => ''));
		echo '<label for="">Name</label>';
		echo $this->Form->input('name', array('label' => ''));
	?>

<?php echo $this->Form->submit(__('Save Information'), array('class' => 'btn btn-primary'));?>
<br />
</div>
</div>
<div class="clear"></div>