<div class="gradesections index">
	<?php echo $this->element('submenu/subjects'); ?>
	<div class="col-md-6 nopadding">
		<?php $headers = array("Section", "Grade Level", "Nos. of Students"); ?>
		<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>

		<?php
		$i = 0;
		foreach ($gradesections as $gradesection): ?>
		<tr>		
			<td><?php echo h($gradesection['Gradesection']['name']); ?>&nbsp;</td>
			<td><?php echo h($gradesection['Gradelevel']['name']); ?>&nbsp;</td>
			<td><?php echo count($gradesection['Student']); ?>&nbsp;</td>
			<!--<td>
				<?php 
					/*$teachers = $this->requestAction(array('controller' => 'teachers', 'action' => 'getallteachersinsections', $gradesection['Gradesection']['id']));
					if(!empty($teachers)){
						echo '<ul>';
						foreach($teachers as $teacher):
							echo '<li>'.$teacher['Teacher']['title'] .' '. $teacher['Teacher']['firstname'] .' '.$teacher['Teacher']['lastname'].'</li>';
						endforeach;
						echo '</ul>';
					}else{
						echo '0';
					}*/
				 ?>
			</td>-->
			<!--<td>
				<?php 
					/*$students = $this->requestAction(array('controller' => 'students', 'action' => 'getallstudentsinsection', $gradesection['Gradesection']['id']));
					if(!empty($students)){
						echo count($students);
					}else{
						echo '0';
					}*/
				 ?>
			</td>-->
			<td class="actions">
				<?php //echo $this->Html->link(__('View'), array('action' => 'view', $gradesection['Gradesection']['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $gradesection['Gradesection']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false)); ?>
				<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $gradesection['Gradesection']['id']), null, __('Are you sure you want to delete # %s?', $gradesection['Gradesection']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
		</tbody>
		</table>
		<?php //echo $this->element('bottom.navigation'); ?>
	</div>
	<div class="clear"></div>
</div>

