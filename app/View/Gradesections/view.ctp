<div class="gradesections view">
<h2><?php  echo __('Gradesection');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($gradesection['Gradesection']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($gradesection['Gradesection']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Gradesection'), array('action' => 'edit', $gradesection['Gradesection']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Gradesection'), array('action' => 'delete', $gradesection['Gradesection']['id']), null, __('Are you sure you want to delete # %s?', $gradesection['Gradesection']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradesections'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradesection'), array('action' => 'add')); ?> </li>
	</ul>
</div>
