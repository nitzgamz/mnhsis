<?php
	if (!empty($sections)) {		
		echo '<option value="">'.__('--------- Select Section --------').'</option>';
		foreach ($sections as $section):
			echo '<option value="'.$section['Gradesection']['id'].'">'.$section['Gradesection']['name'].'</option>';
		endforeach;	
	}else{
		echo '<option value="">No section were created from this grade level.</option>';
	}
?>