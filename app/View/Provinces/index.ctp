<?php echo $this->element('submenu/places'); ?>
<div class="provinces index col-md-4 nopadding">
	<?php $headers = array("Name"); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>

	<?php foreach ($provinces as $province): ?>
	<tr>
		<td><?php echo h($province['Province']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $province['Province']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $province['Province']['id'])); ?>
			<?php echo $this->App->linkbtn(__('Edit'), 'provinces', 'edit', $province['Province']['id']); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $province['Province']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $province['Province']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
