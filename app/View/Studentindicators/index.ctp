<div class="studentindicators index">
	<?php echo $this->element('submenu/student'); ?>
	<h3 class="default"><?php echo __('List and code of Indicators under REMARK column');?></h3>
	<table class="table table-striped table-hover table-condensed">
	<tr>		
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('code');?></th>			
			<th class=""><?php echo __('Required Information');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($studentindicators as $studentindicator): ?>
	<tr>		
		<td><?php echo h($studentindicator['Studentindicator']['name']); ?>&nbsp;</td>		
		<td><?php echo h($studentindicator['Studentindicator']['code']); ?>&nbsp;</td>
		<td><?php echo h($studentindicator['Studentindicator']['description']); ?>&nbsp;</td>		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $studentindicator['Studentindicator']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $studentindicator['Studentindicator']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $studentindicator['Studentindicator']['id']), null, __('Are you sure you want to delete # %s?', $studentindicator['Studentindicator']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>
