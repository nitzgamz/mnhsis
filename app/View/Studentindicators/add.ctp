<?php echo $this->element('submenu/student'); ?>
<div class="studentindicators form">
<h3 class="default"><?php echo __('Add Indicator / Code'); ?></h3>
<?php echo $this->Form->create('Studentindicator');?>
	<fieldset>
	<?php
		echo '<label for="">Name</label>';
		echo $this->Form->input('name', array('label' => ''));		
		echo '<label for="">Code</label>';
		echo $this->Form->input('code', array('label' => ''));
		echo '<label for="">Required Information</label>';
		echo $this->Form->input('description', array('label' => ''));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Create'), array('class' => 'btn btn-primary'));?>
</div>

