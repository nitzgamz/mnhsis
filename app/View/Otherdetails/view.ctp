<div class="otherdetails view">
<h2><?php  echo __('Otherdetail');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Student'); ?></dt>
		<dd>
			<?php echo $this->Html->link($otherdetail['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $otherdetail['Student']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gradelevel'); ?></dt>
		<dd>
			<?php echo $this->Html->link($otherdetail['Gradelevel']['name'], array('controller' => 'gradelevels', 'action' => 'view', $otherdetail['Gradelevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gradesection'); ?></dt>
		<dd>
			<?php echo $this->Html->link($otherdetail['Gradesection']['name'], array('controller' => 'gradesections', 'action' => 'view', $otherdetail['Gradesection']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sy'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['sy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inc Num 1'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['inc_num_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inc Num 2'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['inc_num_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inc Num 3'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['inc_num_3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inc Num 4'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['inc_num_4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inc Num 5'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['inc_num_5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inc Num 6'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['inc_num_6']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inc Num 7'); ?></dt>
		<dd>
			<?php echo h($otherdetail['Otherdetail']['inc_num_7']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Otherdetail'), array('action' => 'edit', $otherdetail['Otherdetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Otherdetail'), array('action' => 'delete', $otherdetail['Otherdetail']['id']), null, __('Are you sure you want to delete # %s?', $otherdetail['Otherdetail']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Otherdetails'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Otherdetail'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradelevels'), array('controller' => 'gradelevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradelevel'), array('controller' => 'gradelevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradesections'), array('controller' => 'gradesections', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradesection'), array('controller' => 'gradesections', 'action' => 'add')); ?> </li>
	</ul>
</div>
