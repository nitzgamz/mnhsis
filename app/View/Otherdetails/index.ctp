<div class="otherdetails index">
	<h2><?php echo __('Otherdetails');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('student_id');?></th>
			<th><?php echo $this->Paginator->sort('gradelevel_id');?></th>
			<th><?php echo $this->Paginator->sort('gradesection_id');?></th>
			<th><?php echo $this->Paginator->sort('sy');?></th>
			<th><?php echo $this->Paginator->sort('inc_num_1');?></th>
			<th><?php echo $this->Paginator->sort('inc_num_2');?></th>
			<th><?php echo $this->Paginator->sort('inc_num_3');?></th>
			<th><?php echo $this->Paginator->sort('inc_num_4');?></th>
			<th><?php echo $this->Paginator->sort('inc_num_5');?></th>
			<th><?php echo $this->Paginator->sort('inc_num_6');?></th>
			<th><?php echo $this->Paginator->sort('inc_num_7');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($otherdetails as $otherdetail): ?>
	<tr>
		<td><?php echo h($otherdetail['Otherdetail']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($otherdetail['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $otherdetail['Student']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($otherdetail['Gradelevel']['name'], array('controller' => 'gradelevels', 'action' => 'view', $otherdetail['Gradelevel']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($otherdetail['Gradesection']['name'], array('controller' => 'gradesections', 'action' => 'view', $otherdetail['Gradesection']['id'])); ?>
		</td>
		<td><?php echo h($otherdetail['Otherdetail']['sy']); ?>&nbsp;</td>
		<td><?php echo h($otherdetail['Otherdetail']['inc_num_1']); ?>&nbsp;</td>
		<td><?php echo h($otherdetail['Otherdetail']['inc_num_2']); ?>&nbsp;</td>
		<td><?php echo h($otherdetail['Otherdetail']['inc_num_3']); ?>&nbsp;</td>
		<td><?php echo h($otherdetail['Otherdetail']['inc_num_4']); ?>&nbsp;</td>
		<td><?php echo h($otherdetail['Otherdetail']['inc_num_5']); ?>&nbsp;</td>
		<td><?php echo h($otherdetail['Otherdetail']['inc_num_6']); ?>&nbsp;</td>
		<td><?php echo h($otherdetail['Otherdetail']['inc_num_7']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $otherdetail['Otherdetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $otherdetail['Otherdetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $otherdetail['Otherdetail']['id']), null, __('Are you sure you want to delete # %s?', $otherdetail['Otherdetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Otherdetail'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradelevels'), array('controller' => 'gradelevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradelevel'), array('controller' => 'gradelevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradesections'), array('controller' => 'gradesections', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradesection'), array('controller' => 'gradesections', 'action' => 'add')); ?> </li>
	</ul>
</div>
