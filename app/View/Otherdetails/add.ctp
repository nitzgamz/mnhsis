<?php echo $this->element('submenu/student'); ?>
<div class="col-md-5 col-lg-5 col-xs-5 " style="background: #ccc; border: 1px solid #ccc; border-radius: 10px 10px; margin-top: 30px;">	
<h3 class="default">Attendance for :<br /> <br />
	<div style="text-align: center; font-size: 20px; padding: 10px 0 20px 0;">
		<?php echo $student['Student']['firstname'].' '.$student['Student']['lastname']; ?><br />
		<div style="font-size: 14px;"><?php echo $student['Gradelevel']['name']; ?> ( <?php echo $student['Gradesection']['name']; ?> )</div>
		<div style="font-size: 14px;">S.Y. <?php echo $student['Studentschoolyear']['sy'].' - '.($student['Studentschoolyear']['sy']+1); ?> </div>
	</div>
</h3>
<div class="otherdetails form">
<?php echo $this->Form->create('Otherdetail');?>
	<fieldset>
		
	<?php
		echo $this->Form->input('student_id', array('type' => 'hidden', 'default' => $student['Student']['id']));		
		echo $this->Form->input('gradelevel_id', array('type' => 'hidden', 'default' => $student['Studentschoolyear']['gradelevel_id']));
		echo $this->Form->input('gradesection_id', array('type' => 'hidden', 'default' => $student['Studentschoolyear']['gradesection_id']));
		echo $this->Form->input('sy', array('type' => 'hidden', 'default' => $student['Studentschoolyear']['sy']));
		echo $this->Form->input('inc_num_1', array('type' => 'hidden', 'default' => '0'));
		echo '<label for="">
				Bilang ng Araw ng may pasok<br />
				(No. of School Days)
				</label>';
				
		echo $this->Form->input('inc_num_2', array('label' => ''));
		
		echo '<label for="">
				Bilang ng Araw na lumiban sa klase <br />
				(No.of School Days Absent)

				</label>';
				
		echo $this->Form->input('inc_num_3', array('label' => ''));
		
		
		echo '<label for="">
				Pangunahing Dahilan <br />
				(Chief Cause)

				</label>';
		echo $this->Form->input('inc_num_4', array('label' => ''));
		
		echo '<label for="">
				Bilang nahuli ng pagpasok <br />
				(No.of Times Tardy)

				</label>';
		echo $this->Form->input('inc_num_5', array('label' => ''));
		
			echo '<label for="">
				Pangunahing Dahilan <br />
				(Chief Cause)

				</label>';
		echo $this->Form->input('inc_num_6', array('label' => ''));
		
		echo '<label for="">
				Bialng ng Araw na pagpasok <br />
				( No. of School Days Present )

				</label>';
		echo $this->Form->input('inc_num_7', array('label' => ''));
		
	?>
	</fieldset>
	<br />
	<div class="col-md-4 no-padding-left">
		<?php echo $this->Form->submit(__('Save Information'), array('class' => 'btn btn-primary'));?>
	</div>
	<div class="col-md-3 no-padding-left" style="margin-top: 5px; margin-left: -20px;">
		<?php echo $this->Html->link('Candel', array('controller' => 'students', 'action' => 'view', $student['Student']['id'], $student['Studentschoolyear']['gradelevel_id'], $student['Studentschoolyear']['gradesection_id'], $student['Studentschoolyear']['sy']), array('class' => 'btn btn-primary')); ?>
	</div>
	<div class="clear"></div>
<br />
</div>
</div>
<div class="clear"></div>
