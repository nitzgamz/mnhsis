<?php echo $this->element('submenu/teachers'); ?>
<div class="col-md-4 nopadding-left">
	<div class="panel panel-success col-md-12 nopadding">
		<div class="panel-heading">Teacher Profile</div>
		<div class="panel-body">
			<table class="table table-condensed table-bordered">
				<tr><td>Name</td><td><?php echo $teacher['Teacher']['name']; ?></td></tr>
				<tr><td>Sex</td><td><?php echo $teacher['Teacher']['sex']; ?></td></tr>
				<tr><td>Date of Birth</td><td><?php echo date('d M Y', strtotime($teacher['Teacher']['dob'])); ?></td></tr>
				<tr><td>Place of Birth</td><td><?php echo $teacher['Teacher']['pob']; ?></td></tr>
				<tr><td>Address</td><td><?php echo $teacher['Teacher']['address']; ?></td></tr>
				<tr><td>Email</td><td><?php echo $teacher['Teacher']['email']; ?></td></tr>
				<tr><td>Contact</td><td><?php echo $teacher['Teacher']['contact']; ?></td></tr>
				<tr><td>Employment Status</td><td><?php echo $teacher['Teacher']['employment_status']; ?></td></tr>
				<tr><td>Other Information</td><td><?php echo $teacher['Teacher']['others']; ?></td></tr>
			</table>
		</div>
	</div>
</div>

<div class="col-md-4 nopadding-left">
	<div class="panel panel-success col-md-12 nopadding">
		<div class="panel-heading">Other Details</div>
		<div class="panel-body">
			<table class="table table-condensed table-bordered">
				<tr><td>Grade Level</td><td><?php echo $teacher['Gradelevel']['name']; ?></td></tr>
				<tr><td>Secion</td><td><?php echo $teacher['Gradesection']['name']; ?></td></tr>
				<tr><td>Total Students</td><td><?php echo count($teacher['Student']); ?></td></tr>
			</table>
		</div>
	</div>
</div>

<div class="col-md-3 nopadding-left">
	<div class="panel panel-success col-md-12 nopadding">
		<div class="panel-heading">Subjects</div>
		<div class="panel-body">
			
				<?php if (!empty($teacher['Teachersubject'])):?>	
				<ul>
				<?php
					foreach ($teacher['Teachersubject'] as $teachersubject): 											
							echo '<li>'.$this->Externalfunction->getnamebyid($teachersubject['schoolsubject_id'], 'schoolsubjects', 'getnamebyid').'</li>';				
					endforeach; ?>		
				<?php endif; ?>
				</ul>
				<?php echo $this->Html->link( 'Assign Subject', array('controller' => 'teachersubjects', 'action' => 'add', $teacher['Teacher']['id']), array('class' => 'btn btn-xs btn-primary'));?>					
				
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>



