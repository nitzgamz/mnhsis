<?php
	if (!empty($teachers)) {		
		echo '<option value="">'.__('--------- Select Teacher --------').'</option>';
		foreach ($teachers as $t):
			echo '<option value="'.$t['Teacher']['id'].'">'.$t['Teacher']['name'].'</option>';
		endforeach;	
	}else{
		echo '<option value="">No teacher found.</option>';
	}
?>