<?php echo $this->element('submenu/teachers'); ?>
<div class="teachers form">
<?php echo $this->Form->create('Teacher');?>
	
	<div class="col-md-3 nopadding">
		<div class="panel panel-success col-md-12 nopadding">
			<div class="panel-heading">Teacher Information</div>
			<div class="panel-body">
				<?php echo $this->Form->input('id'); ?>
				<?php echo $this->Form->input('title', array('options' => array('Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss'), 'label' => 'Title', 'div' => 'col-md-12 nopadding')); ?>
				<?php echo $this->Form->input('firstname', array('label' => 'First Name', 'div' => 'col-md-12 nopadding')); ?>
				<?php echo $this->Form->input('middlename', array('label' => 'Middle Name', 'div' => 'col-md-12 nopadding')); ?>
				<?php echo $this->Form->input('lastname', array('label' => 'Last Name', 'div' => 'col-md-12 nopadding')); ?>
				<?php echo $this->Form->input('sex', array('type' => 'select', 'options' => array('M' => 'M', 'F' => 'F'), 'label' => 'Sex', 'class' => 'form-control input-sm birthdate')); ?>
				<?php echo $this->Form->input('idnumber', array('label' => 'ID Number', 'class' => 'form-control input-sm')); ?>
			</div>
		</div>
	</div>
	<div class="col-md-3 nopadding-right">
		<div class="panel panel-success col-md-12 nopadding">
			<div class="panel-heading">Other Information</div>
			<div class="panel-body">
				<?php echo $this->Form->input('dob', array('separator' => false, 'minYear' => date('Y')-10, 'maxYear' => date('Y')-50, 'label' => 'Birthdate', 'class' => 'form-control input-sm birthdate')); ?>				
				<?php echo $this->Form->input('pob', array('label' => 'Place of Birth', 'rows' => 1, 'div' => 'col-md-12 nopadding')); ?>		
				<?php echo $this->Form->input('contact', array('label' => 'Contact No.', 'class' => 'form-control input-sm', 'div' => 'col-md-6 nopadding-left')); ?>
				<?php echo $this->Form->input('email', array('type' => 'email', 'label' => 'Email Address', 'class' => 'form-control input-sm',  'div' => 'col-md-6 nopadding')); ?>
				<?php echo $this->Form->input('address', array('label' => 'Address', 'class' => 'form-control input-sm',  'rows' => 2)); ?>
				<?php echo $this->Form->input('others', array('label' => 'Additional Information', 'rows' => 2, 'div' => 'col-md-12 nopadding')); ?>
			</div>
		</div>
	</div>
	<div class="col-md-3 nopadding-right">
		<div class="panel panel-success col-md-12 nopadding">
			<div class="panel-heading">School Information</div>
			<div class="panel-body">
				<?php echo $this->Form->input('gradelevel_id', array('label' => 'Grade Level', 'empty' => '-- Choose --', 'div' => 'col-md-12 nopadding', 'id' => 'gradelevel_id')); ?>
				<?php echo $this->Form->input('gradesection_id', array('label' => 'Grade Section', 'empty' => '-- Choose --', 'div' => 'col-md-12 nopadding', 'id' => 'gradesection_id')); ?>
				<?php echo $this->Form->input('employment_status', array('type' => 'select', 'options' => array('Regular' => 'Regular', 'Probitionary' => 'Probitionary', 'Part-time' => 'Part-time'), 'label' => 'Employment Status', 'div' => 'col-md-12 nopadding')); ?>
			
			</div>
		</div>
	</div>
	<div class="clear"></div>


	<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary'));?>
</div>

<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradelevel_id', 'update_id' => '#gradesection_id', 'controller_id' => 'gradesections', 'action_id' => 'getsectionlists', 'method' => 'post', 'form' => 'Teacher')); ?>

