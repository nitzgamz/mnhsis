<div class="teachers index col-md-8 nopadding">
	<?php echo $this->element('submenu/teachers'); ?>
	<?php $headers = array("ID No.", "Title", "Firstname", "Middlename", "Lastname", "Grade Level", "Section"); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>

	<?php
	$i = 0;
	foreach ($teachers as $teacher): ?>
	<tr>		
		<td><?php echo h($teacher['Teacher']['idnumber']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Teacher']['title']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Teacher']['firstname']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Teacher']['middlename']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Teacher']['lastname']); ?>&nbsp;</td>		
		<td><?php echo h($teacher['Gradelevel']['name']); ?>&nbsp;</td>
		<td><?php echo h($teacher['Gradesection']['name']); ?>&nbsp;</td>		
		<!--td>
			<?php 
				//$subjects = $this->requestAction(array('controller' => 'teachersubjects', 'action' => 'getallteacher', $teacher['Teacher']['id']));
				//if(!empty($subjects)){
					//echo count($subjects);
				//}else{
					//echo '0';
				//}
			 ?>
		</td-->		
		<!--<td>
			<?php 
				//$students = $this->requestAction(array('controller' => 'students', 'action' => 'getallstudentsinteacher', $teacher['Teacher']['id']));
				//if(!empty($students)){
					//echo count($students);
				//}else{
					//echo '0';
				//}
			 ?>
		</td>	-->	
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $teacher['Teacher']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false)); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $teacher['Teacher']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false)); ?>
			<?php //echo $this->Html->link(__('Tag / Assign Subject'), array('controller' => 'teachersubjects', 'action' => 'add', $teacher['Teacher']['id'])); ?>			
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $teacher['Teacher']['id']), null, __('Are you sure you want to delete # %s?', $teacher['Teacher']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	<tbody>
	</table>
</div>
