<div class="attendances view">
<h2><?php echo __('Attendance'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($attendance['Attendance']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($attendance['User']['full_name'], array('controller' => 'users', 'action' => 'view', $attendance['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Student'); ?></dt>
		<dd>
			<?php echo $this->Html->link($attendance['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $attendance['Student']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Teacher'); ?></dt>
		<dd>
			<?php echo $this->Html->link($attendance['Teacher']['name'], array('controller' => 'teachers', 'action' => 'view', $attendance['Teacher']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Schoolyear'); ?></dt>
		<dd>
			<?php echo $this->Html->link($attendance['Schoolyear']['name'], array('controller' => 'schoolyears', 'action' => 'view', $attendance['Schoolyear']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mos'); ?></dt>
		<dd>
			<?php echo $this->Html->link($attendance['Mos']['name'], array('controller' => 'mos', 'action' => 'view', $attendance['Mos']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Present'); ?></dt>
		<dd>
			<?php echo h($attendance['Attendance']['present']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Present'); ?></dt>
		<dd>
			<?php echo h($attendance['Attendance']['total_present']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Absent'); ?></dt>
		<dd>
			<?php echo h($attendance['Attendance']['absent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Absent'); ?></dt>
		<dd>
			<?php echo h($attendance['Attendance']['total_absent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Added'); ?></dt>
		<dd>
			<?php echo h($attendance['Attendance']['added']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Attendance'), array('action' => 'edit', $attendance['Attendance']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Attendance'), array('action' => 'delete', $attendance['Attendance']['id']), array(), __('Are you sure you want to delete # %s?', $attendance['Attendance']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Attendances'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Attendance'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolyears'), array('controller' => 'schoolyears', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolyear'), array('controller' => 'schoolyears', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Mos'), array('controller' => 'mos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Mos'), array('controller' => 'mos', 'action' => 'add')); ?> </li>
	</ul>
</div>
