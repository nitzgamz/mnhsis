<div class="attendances form col-md-12 wbg">
<?php echo $this->Form->create('Attendance'); ?>
	<?php
		foreach($datas as $mo):
	?>
			<div class="col-md-3 nopadding">
				<div class="panel panel-success col-md-12 nopadding noborder">
					<div class="panel-heading bolder">
						<?php echo $mo['Mo']['name']; ?>
						<?php echo $this->Form->hidden('id.', array('default' => $mo['Attendance']['id'])); ?>
					</div>
					<div class="panel-body">
						<table class="table table-condensed smalltable">
							<tr><td width="70%">Nos. of School Days</td><td><?php echo $mo['Mo']['sdays']; ?></td></tr>
							<tr><td>Nos. of Days Present</td><td><?php echo $this->Form->input('present.', array('default' => $mo['Attendance']['present'], 'class' => 'form-control input-sm yellow')); ?></td></tr>
							<tr><td>Nos. of Days Absent</td><td><?php echo $this->Form->input('absent.', array('default' => $mo['Attendance']['absent'], 'class' => 'form-control input-sm red')); ?></td></tr>
						</table>
					
					</div>
				</div>
			</div>
	<?php
		endforeach;
	?>
	
<?php echo $this->App->formbtn('Save Changes', 'btn-lg btn-block btn-primary'); ?>
</div>
<?php $this->App->clear(); ?>

