<div class="attendances form col-md-12 wbg">
<?php $sy = array($schoolyears['Schoolyear']['id'] => $schoolyears['Schoolyear']['sy_from'].' - '.$schoolyears['Schoolyear']['sy_to']); ?>
<?php echo $this->Form->create('Attendance'); ?>
	
	<?php
		echo $this->Form->hidden('user_id', array('default' => $users['User']['id']));
		echo $this->Form->hidden('teacher_id', array('default' => $teachers['Teacher']['id'])) ;
		echo $this->Form->input('student_id', array('label' => 'Select Student', 'class' => 'form-control input-lg', 'div' => 'col-md-6 nopadding-left'));
		echo $this->Form->input('schoolyear_id', array('label' => 'Select S.Y.', 'options' => $sy, 'empty' => false, 'class' => 'form-control input-lg', 'div' => 'col-md-6 nopadding-right'));
		echo $this->App->clear();
		
		
		echo $this->Form->input('gradelevel_id', array('label' => 'Grade Level', 'class' => 'form-control input-lg', 'div' => 'col-md-6 nopadding-left'));
		
		echo $this->Form->input('gradesection_id', array('label' => 'Section', 'class' => 'form-control input-lg', 'div' => 'col-md-6 nopadding-right'));
		echo $this->App->clear();
		
		foreach($mos as $mo):
	?>
			<div class="col-md-3 nopadding">
				<div class="panel panel-success col-md-12 nopadding noborder">
					<div class="panel-heading bolder">
						<?php echo $mo['Mo']['name']; ?>
						<?php echo $this->Form->hidden('mo_id.', array('default' => $mo['Mo']['id'])); ?>
					</div>
					<div class="panel-body">
						<table class="table table-condensed smalltable">
							<tr><td width="70%">Nos. of School Days</td><td><?php echo $mo['Mo']['sdays']; ?></td></tr>
							<tr><td>Nos. of Days Present</td><td><?php echo $this->Form->input('present.', array('default' =>$mo['Mo']['sdays'], 'class' => 'form-control input-sm yellow')); ?></td></tr>
							<tr><td>Nos. of Days Absent</td><td><?php echo $this->Form->input('absent.', array('default' => 0, 'class' => 'form-control input-sm red')); ?></td></tr>
						</table>
					
					</div>
				</div>
			</div>
	<?php
		endforeach;
	?>
	
<?php echo $this->App->formbtn('Save Information', 'btn-lg btn-block btn-primary'); ?>
</div>
<?php $this->App->clear(); ?>

