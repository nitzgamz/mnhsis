<h4>Attendance Report &raquo; S.Y. <?php echo date('Y'); ?> - <?php echo date('Y') + 1; ?> </h4>
<?php echo $this->App->linkbtn('<span class="glyphicon glyphicon-plus-sign"></span> New Attendance', 'attendances', 'add'); ?>
<?php $this->App->clear(); ?>
<div class="attendances index col-md-6 nopadding">
	<?php $headers = array('Name'); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>
	<?php foreach ($attendances as $attendance): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($attendance['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $attendance['Student']['id'])); ?>
		</td>
		<td class="actions">
			<?php $this->App->modal_link('Download Report', 'select_sy', 'download_report', $attendance['Student']['id']); ?>
			<?php //$this->App->linkbtn('Edit', 'attendances', 'edit', $attendance['Student']['id']); ?>
			<?php //$this->App->linkbtn('View', 'attendances', 'view_report', $attendance['Student']['id']); ?>
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $attendance['Attendance']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $attendance['Attendance']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $attendance['Attendance']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $attendance['Attendance']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>


<!---Select S.Y. Form-->
	<div class="modal fade select_sy" tabindex="-1" id="addnewform" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
			  <div class="modal-dialog modal-xs" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="gridSystemModalLabel">Select S.Y.</h4>
					</div>
					<div class="modal-body col-md-12">
						<?php echo $this->Form->create('Attendance', array('method' => 'post', 'class' => 'formgen')); ?>
						<?php echo $this->Form->hidden('student', array('id' => 'student_id')); ?>
						<?php echo $this->Form->input('sy', array('label' => false, 'type' => 'select', 'options' => $sy, 'class' => 'form-control input-lg')); ?>
						
							<?php echo $this->App->formbtn('Generate Report', 'lg', 'success btn-block btngen'); ?>
							<a  href="<?php echo $this->webroot; ?>app/webroot/files/form-138.xls" class="btn btn-primary btn-xs download" download disabled><span class="glyphicon glyphicon-download"></span> Download</a>
						
						<div class="clear separator20"></div>
					</div>
					<div class="clear"></div>
				</div>
			  </div>
			</div>

	<!---Load Content-->
<?php
	$this->Js->Buffer('
		$(".download_report").bind("click", function(){
			var param1 = $(this).attr("param1");
			$("#student_id").val(param1);
		});
		
		$(".btngen").bind("click", function(e){
			e.preventDefault();
			var btn = $(this);
			var formdata = $(".formgen").serialize();
			
			$.ajax({
							type		: 	"POST",
							url			:  "'.$this->Html->url('/', true).'attendances/download_report",
							data		: formdata,
							dataType	: "html", 
							beforeSend	: function(){	
									btn.prop("disabled", true);
									btn.val("Generating Report....");
									$(".download").attr("disabled", "");
							},
							success		: function(){
									$(".download").removeAttr("disabled");
							},	
							error : function(){
								alert("Opps something is not right, please try again.");
							},
							complete	: function(){
									btn.prop("disabled", false);
									btn.val("Generate Report");
							}
						});
						
		});
	');
?>