<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>	
	<title>Assessment Certificate</title>	
	<link rel="stylesheet" type="text/css" href="../webroot/css/bootstrap-pdf.css" />	
</head>
<body>
	<!-- Wrap all page content here -->
    <div id="wrap">
		<!-- Begin page content -->		
		<div class="container main-content certificate" style="">													
			<?php echo $this->fetch('content'); 
				//$dompdf->load_html(utf8_decode($content_for_layout), Configure::read('App.encoding'));				
			?>			
			<!--div class="water-mark"><img src="../webroot/img/ml_water_mark.png" /></div-->
			
			
		</div>
		
	</div>	
</body>
</html>
