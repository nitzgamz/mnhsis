<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">	
    <head> 
        <title><?php echo $title_for_layout; ?></title>         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<?php echo $this->Html->css('bootstrap'); ?>
		<?php echo $this->Html->css('bootstrap.theme'); ?>
		<?php echo $this->Html->css('style.generic.default'); ?>		
		<?php echo $this->Html->css('jquery.dataTables'); ?>
	
		<?php echo $this->Html->script('jquery.min'); ?>		
		<?php echo $this->Html->script('bootstrap'); ?>				
		<?php echo $this->Html->script('jquery.dataTables'); ?>				
		<?php echo $this->Html->script('jquery.dataTables.init'); ?>				
	<body>
		<div class="title-header-page navbar-fixed-top">
				<?php echo $this->element('main.menu'); ?>							
		</div>
		<div class="clear separator20"></div>
		<div id="container">		
			<div clas="main-wrap-inner-header">	
				<div class="header-c">
					<?php //echo $this->element('logo.menu'); ?>					
					<div class="clear"></div>
				</div>
			</div>							
			<div class="main-wrap">		
				<div class="main-wrap-inner">														
					<div class="main-wrap-inner-body">												
						<?php //echo $this->element('logged.user'); ?>						
						<div class="message-response"><?php echo $this->Session->flash(); ?></div>
						<div class="before-content-layout"><?php echo $content_for_layout; ?></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>	
			<div class="footer"></div>
		</div>
		
		
		<!---Loading-->
		<div class="modal fade loadingmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
		  <div class="modal-dialog modal-xs" role="document">
				<span class="alert alert-success"> Initializing... please wait...</span>		
		  </div>
		</div>
		<!---Loading-->
		
		<!---Load Content-->
		<div class="modal fade modallink" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
				  <div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
						</div>
						<div class="modal-body" id="modalview">
							
						</div>
					</div>
				  </div>
				</div>

		<!---Load Content-->
		
		
		<!---Load Content-->
		<div class="modal fade filterstudent" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
				  <div class="modal-dialog modal-sm" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="gridSystemModalLabel">Filter Student</h4>
						</div>
						<div class="modal-body">
							
								<?php
									$fschoolyears = $this->App->getSchoolyear();
									$fgradelevels = $this->App->getGradelevel();
									
									echo $this->Form->create('Filterstudent', array('url' => array('controller' => 'students', 'action' => 'teacher_students'))); 
									echo $this->Form->input('schoolyear_id', array('id' => 'fschoolyear_id', 'label' => 'S.Y.', 'type' => 'select', 'options' => $fschoolyears, 'class' => 'form-control input-sm', 'escape' => false));
									echo $this->Form->input('gradelevel_id', array('type' => 'select', 'options' => $fgradelevels, 'label' => 'Grade Level', 'empty' => '-- Choose --', 'id' => 'fgradelevel_id', 'class' => 'form-control input-sm', 'escape' => false));
									echo $this->Form->input('gradesection_id', array('label' => 'Section',  'id' => 'fgradesection_id', 'empty' => '-- Choose --', 'class' => 'form-control input-sm', 'escape' => false));
									echo $this->App->clear();
									echo $this->Form->end(array('type' => 'button', 'label' => 'Filter Results', 'class' => 'btnfilter btn btn-primary btn-block btn-lg'));
									echo $this->Html->link('Browse All', array('controller' => 'students', 'action' => 'index'), array('class' => 'btn btn-lg btn-info btn-block', 'escape' => false));
								?>
								
								<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#fgradelevel_id', 'update_id' => '#fgradesection_id', 'controller_id' => 'gradesections', 'action_id' => 'getsectionlists', 'method' => 'post', 'form' => 'Filterstudent')); ?>

								
						</div>
					</div>
				  </div>
				</div>

		<!---Load Content-->
		
		
		<?php $this->Js->Buffer('
			
			$(".btnfilter").on("click", function(e){
				e.preventDefault();
				var sy = $("#fschoolyear_id").find("option:selected").val();
				var level = $("#fgradelevel_id").find("option:selected").val();
				var sec = $("#fgradesection_id").find("option:selected").val();
				
				if(sy!=="" && level!=="" && sec!==""){
					window.location.href="'.$this->html->url('/', true).'students/teacher_students/" + sy + "/" + level + "/"  + sec;
				}
			});
			
			
			/*=============================
			| load content when modal link 
			| is clicked
			==============================*/
			
			$(".modalink").click(function(e) {
					e.preventDefault();
					var $this 	= $(this);
					var loadurl = $this.attr("href");
					var targ 	= $("#modalview"); //$this.attr("data-target");
					var title 	= $this.attr("title");
				
					
						$(".loadingmodal").modal("show");
						//$(targ).html("Loading Data...");	
						
						$.get(loadurl, function(data, status){
							if(status=="success"){
								$(".loadingmodal").modal("hide");
								$(".modallink").modal("show");
								$(".modal-title").text(title);
								$(targ).html(data);
							}else{
								$(".loadingmodal").modal("hide");
							}
						});
						
					
					return false;
			});
			
			$(".filterstudent").on("shown.bs.modal", function(){
				$("#fgradelevel_id").val("");
			
			});
			
		'); ?>
		<?php echo $this->Js->writeBuffer(); ?>
	</body>
</html>