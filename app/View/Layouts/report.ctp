<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Assessment Certificate</title>
	<link rel="stylesheet" type="text/css" href="../webroot/css/bootstrap-pdf.css" />
</head>
<body>
	<!-- Wrap all page content here -->
    <div id="wrap">

		

		<!-- Begin page content -->
	
		<div class="container main-content">							
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>			
		</div>
		
	</div>

	
	<?php
		echo $this->Js->writeBuffer(); 
		echo $this->Html->script('bootstrap');					
		
		/*echo $this->Html->script('jquery.validate');					
		echo $this->Html->script('additional-methods');					
		echo $this->Html->script('jquery.metadata');*/
		
	?>
	<script type="text/javascript">
		$(function () { 
			$("[data-toggle='tooltip']").tooltip(); 
		});		
	</script>	
</body>
</html>
