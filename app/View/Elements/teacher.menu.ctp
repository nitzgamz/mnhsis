<?php $user = $this->requestAction('users/loggeduser'); ?>

	<div class="btn-group pull-left noradius">
		<?php echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Enroll New Student', array('controller' => 'students', 'action' => 'add'), array('class' => 'noradius btn btn-success btn-sm', 'escape' => false)); ?>
		<?php //echo $this->Html->link('<i class="glyphicon glyphicon-user"></i> My Students', array('controller' => 'students', 'action' => 'teacher_students'), array('class' => 'noradius btn btn-primary btn-sm', 'escape' => false)); ?>
		<?php $this->App->modal_link_ad('<i class="glyphicon glyphicon-user"></i> My Students', 'filterstudent');  ?>
		
		<?php //echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i> Report Card', array('controller' => 'attendances', 'action' => 'index'), array('class' => 'noradius btn btn-primary btn-sm', 'escape' => false)); ?>
		<?php //echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i> Permanent Record', array('controller' => 'periodicratings', 'action' => 'index'), array('class' => 'noradius btn btn-primary btn-sm', 'escape' => false)); ?>
		<?php echo $this->Html->link('<i class="glyphicon glyphicon-cog"></i> My Profile', array('controller' => 'users', 'action' => 'myprofile'), array('class' => 'noradius btn btn-default btn-sm', 'escape' => false)); ?>		
		<?php echo $this->Html->link('<i class="glyphicon glyphicon-lock"></i> Change Password', array('controller' => 'users', 'action' => 'changepassword'), array('class' => 'noradius btn btn-default btn-sm', 'escape' => false)); ?>		
		<?php //echo $this->Html->link('<i class="glyphicon glyphicon-cog"></i> Settings', array('controller' => 'settings', 'action' => 'index'), array('class' => 'noradius btn btn-default btn-sm', 'escape' => false)); ?>		
		<?php //echo $this->Html->link('<i class="glyphicon glyphicon-cog"></i> Subjects', array('controller' => 'subjects', 'action' => 'index'), array('class' => 'noradius btn btn-default btn-sm', 'escape' => false)); ?>		
	</div>
			
		<?php echo $this->Html->link('<i class="glyphicon glyphicon-off"></i> Logout', array('controller' => 'users', 'action' => 'logout'), array('class' => 'noradius btn btn-danger btn-sm pull-right', 'escape' => false)); ?>				


<div class="clear"></div>
