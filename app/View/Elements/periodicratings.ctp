<tr>
				<td class="bolder">
					<?php echo $this->Form->hidden('subject_id.', array('default' => $sb['Subject']['id'], 'label' => false, 'class' => 'form-control input-xs')); ?>
					<?php echo $sb['Subject']['name']; ?>
				</td>
				<td width="30%">
					<div class="col-md-12 nopadding">
						<div class="col-md-3 center nopadding"><?php echo $this->Form->input('1st_qtr.', array('default' => '0', 'label' => false, 'class' => 'red form-control input-xs')); ?></div>
						<div class="col-md-3 center nopadding"><?php echo $this->Form->input('2nd_qtr.', array('default' => '0', 'label' => false, 'class' => 'red form-control input-xs')); ?></div>
						<div class="col-md-3 center nopadding"><?php echo $this->Form->input('3rd_qtr.', array('default' => '0', 'label' => false, 'class' => 'red form-control input-xs')); ?></div>
						<div class="col-md-3 center nopadding"><?php echo $this->Form->input('4th_qtr.', array('default' => '0', 'label' => false, 'class' => 'red form-control input-xs')); ?></div>
					</div>
				</td>
				<td width="10%"><?php echo $this->Form->input('rating.', array('default' => '0', 'label' => false, 'class' => 'yellow form-control input-xs')); ?></td>
				<td width="18%"><?php echo $this->Form->input('action.', array('type' => 'select', 'options' => $this->App->roptions(), 'empty' => false, 'label' => false, 'class' => 'yellow form-control input-xs')); ?></td>
				<td width="10%"><?php echo $this->Form->input('earned.', array('default' => '0', 'label' => false, 'class' => 'yellow form-control input-xs')); ?></td>
			</tr>