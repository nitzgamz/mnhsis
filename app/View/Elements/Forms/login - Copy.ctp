<!-- Large modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>

<!-- Small modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>

<div class="login-navigation">	
	<div class="client-login">
					<!-- Modal -->
					<div class="modal fade" id="client_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Administrator Login</h4>
						  </div>
						  <!--<div class="modal-header-separator"></div>-->
						  <div class="modal-body">							
								<div class="inside">
									
									</div>											
						  </div>						
						</div>
					  </div>
					</div>					
					<a href="" data-toggle="modal" data-target="#client_login">Administrator Login</a>
					<div class="modal fade" id="client_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">Modal title</h4>
							  </div>
							  <div class="modal-body">
								...
							  </div>							
							</div>
						  </div>
					</div>
				</div>
</div>

<div class="login-content">	
										<div class="login-inner-content">
											<div style="text-align: left; color: #000; margin: -15px 0 10px 0;"><?php echo __('Today &raquo; ').' '.date('l F d, Y'); ?></div>
											<?php echo $this->Form->create('User', array('action' => 'login')); ?>
											<div class="div-separator-2"><?php echo __('Username'); ?></div>
											<div class="div-separator-2">
												<?php echo $this->Form->input('username', array('class' => 'normalinputfield', 'label' => '')); ?>
											</div>
											<div class="div-separator-2"><?php echo __('Password'); ?></div>
											<div class="div-separator-2">
												<?php echo $this->Form->input('password', array('class' => 'normalinputfield', 'label' => '')); ?>
											</div>
											<div class="div-separator">
												<?php echo $this->Form->submit('Login Account', array('class' => 'subtn', 'title' => 'Login Account')); ?>
												<?php //echo $this->Form->button('CANCEL', array('type' => 'reset', 'class' => 'subtn', 'title' => 'Cancel Login')); ?>
											</div>		
										</div>	
									</div>