<tr>
				<td class="bolder">
					<?php echo $this->Form->hidden('id.', array('default' => $sb['Periodicrating']['id'], 'label' => false, 'class' => 'form-control input-xs')); ?>
					<?php echo $sb['Subject']['name']; ?>
				</td>
				<td width="30%">
					<div class="col-md-12 nopadding">
						<div class="col-md-3 center nopadding"><?php echo $this->Form->input('1st_qtr.', array('default' => $sb['Periodicrating']['1st_qtr'], 'label' => false, 'class' => 'red form-control input-xs')); ?></div>
						<div class="col-md-3 center nopadding"><?php echo $this->Form->input('2nd_qtr.', array('default' => $sb['Periodicrating']['2nd_qtr'], 'label' => false, 'class' => 'red form-control input-xs')); ?></div>
						<div class="col-md-3 center nopadding"><?php echo $this->Form->input('3rd_qtr.', array('default' => $sb['Periodicrating']['3rd_qtr'], 'label' => false, 'class' => 'red form-control input-xs')); ?></div>
						<div class="col-md-3 center nopadding"><?php echo $this->Form->input('4th_qtr.', array('default' => $sb['Periodicrating']['4th_qtr'], 'label' => false, 'class' => 'red form-control input-xs')); ?></div>
					</div>
				</td>
				<td width="10%"><?php echo $this->Form->input('rating.', array('default' => $sb['Periodicrating']['rating'], 'label' => false, 'class' => 'yellow form-control input-xs')); ?></td>
				<td width="18%"><?php echo $this->Form->input('action.', array('default' => $sb['Periodicrating']['action'], 'type' => 'select', 'options' => $this->App->roptions(), 'empty' => false, 'label' => false, 'class' => 'yellow form-control input-xs')); ?></td>
				<td width="10%"><?php echo $this->Form->input('earned.', array('default' => $sb['Periodicrating']['earned'], 'label' => false, 'class' => 'yellow form-control input-xs')); ?></td>
			</tr>