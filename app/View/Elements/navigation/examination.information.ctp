<?php if(!empty($examination)){ ?>	
<div class="examinations-view">
	<!--<div class="day"><?php echo date('l', strtotime($examination['Examination']['examination_date'])); ?></div>-->
	<div><?php echo strtoupper(date('l - F d, Y', strtotime($examination['Examination']['examination_date']))); ?></div>
	<div class="status">Banned Until : <?php echo strtoupper(date('l - F d, Y', strtotime($examination['Examination']['banned_until']))); ?></div>
	<div class="status"><?php echo $this->Time->timeAgoInWords($examination['Examination']['examination_date'], array('format' => 'F jS, Y', 'end' => '+1 year')); ?></div>
	<div class="status" style="<?php echo (($examination['Examinationstatus']['id']==7) ? "color: #000;" : "color: ".$examination['Examinationstatus']['flagcolor'].';'); ?>"><?php echo __(' Status : ').$examination['Examinationstatus']['name'].__('  '); ?></div>
</div>
<h2><?php echo __('Examination Details'); ?></h2>
<?php 
	echo $this->element('table/table-header', array('headers' => array(
	'Region',
	'Area',
	'Total',
	'Created By',
	'Created',
	'Modified By',
	'Modified',
	'Actions'), 'navigation' => false));
	echo '<tr class="table-information">';
		echo '<td>'.$examination['Region']['name'].'</td>';
		echo '<td>'.$examination['Area']['name'].'</td>';
		echo '<td>'.__('Total').'</td>';
		echo $this->element('table/table-related', array('fields' => array('added_by', 'added', 'modified_by', 'modified'), 'controller' => $examination['Examination']));
		echo '<td class="actions">';
			echo $this->Html->link('Details', array('controller' => 'examinations', 'action' => 'view', $examination['Examination']['id']), array('class' => 'view', 'title' => 'View Examination Information'));
			//echo $this->Html->link('Update', array('controller' => 'examinations', 'action' => 'edit', $examination['Examination']['id']), array('class' => 'edit', 'title' => 'Update Examination Information'));
		echo '</td>';
	echo '</tr>';
	echo $this->element('table/table-footer', array('navigation' => false));
?>
<?php } ?>