<?php
	if(!empty($applicant)){
		echo '<div style="margin: 5px 0;">';
		echo '<div class="form-title"><h2>';
			if(!empty($formtitle)){ echo $formtitle; }else{ echo __('Personal Data'); }
		echo '</h2></div>';
		echo '<table width="100%" cellpadding="0" cellspacing="0">';
			echo '<tr class="table-header">';
				foreach(array(
					"Surname", 
					"Given Name", 
					"Middle Name",
					"Gender", 					
					"Birthdate", 
					"Age", 
					"Added By", 
					"Modified By", 					
				) as $header):				
					echo '<th>'.$header.'</th>';
				endforeach;
			echo '</tr>';
			echo '<tr class="table-information2">';								
				echo '<td style="font: bold 15px arial;">'.$applicant['Applicant']['lastname'].'</td>';
				echo '<td style="font: bold 15px arial;">'.$applicant['Applicant']['firstname'].'</td>';
				echo '<td style="font: bold 15px arial;">'.$applicant['Applicant']['middlename'].'</td>';			
				echo '<td style="font: bold 15px arial;">'.$applicant['Applicant']['gender'].'</td>';	
				echo '<td style="font: bold 15px arial;">'.$applicant['Applicant']['birthdate'].'</td>';			
				echo '<td style="font: bold 15px arial;">'.$this->Externalfunction->getThecurrentage($applicant['Applicant']['birthdate']).'</td>';			
				echo '<td style="font: bold 15px arial;">';
					echo (($applicant['Applicant']['added_by']==0) ? '-' : $this->Externalfunction->gettheauthor($applicant['Applicant']['added_by']));
				echo '</td>';			
				echo '<td style="font: bold 15px arial;">';					
					echo (($applicant['Applicant']['modified_by']==0) ? '-' : $this->Externalfunction->gettheauthor($applicant['Applicant']['modified_by']));
				echo '</td>';		
				/*echo '<td class="actions">';
					echo $this->Html->link(__('View'), array('controller' => 'applicants', 'action' => 'view', $applicant['Applicant']['id']), array('class' => 'view', 'title' => 'View Applicant Information')); 
					echo $this->Html->link(__('Edit'), array('controller' => 'applicants', 'action' => 'edit', $applicant['Applicant']['id']), array('class' => 'edit', 'title' => 'Update Applicant Information'));
				echo '</td>';*/
			echo '</tr>';
			
		echo '</table>';
		echo '</div>';
	}
?>