<div class="paging">	
	<div class="inside-paging">
	<?php		
		$prev_img_active = $this->Html->image("menus/previousl-button04.png", array('escape' => false, 'class' => 'previous'));
		$prev_img_inactive = $this->Html->image("menus/previousl-button03.png", array('escape' => false, 'class' => 'previous'));
		$next_img_active = $this->Html->image("menus/next-button04.png", array('escape' => false, 'class' => 'next'));
		$next_img_inactive = $this->Html->image("menus/next-button03.png", array('escape' => false, 'class' => 'next'));
		//echo $this->Paginator->prev(!$this->Paginator->hasPrev() ? $prev_img_inactive : $prev_img_active, array('escape' => false, 'class' => 'previous'));									
		echo $this->Paginator->first('First Page', array('class' => 'first'), null, array('class' => 'first'));	
		echo $this->Paginator->prev('Previous Page',  array('class' => 'previous'), null, array('class' => 'previous'));						
		echo $this->Paginator->numbers(array('class' => 'numbers', 'separator' => ' '));								
		echo $this->Paginator->next('Next Page', array('class' => 'next'), null, array('class' => 'next'));	
		echo $this->Paginator->last('Last Page', array('class' => 'last'), null, array('class' => 'last'));
		//echo $this->Paginator->next(!$this->Paginator->hasNext() ? $next_img_inactive : $next_img_active, array('class' => 'next', 'escape' => false));
	?>
	</div>	
	<?php if(!empty($model) && !empty($controller) && !empty($action)){ ?>
	<div class="search-form">		
		<?php			
				echo $this->Form->create($model, array(
					'url' => array(
						'controller' => $controller, 
						'action' => $action,
					)
				));
				echo $this->Form->input('search', array('class' => 'normalinput', 'label' => '', 'placeholder' => 'Search '.$model));
				echo $this->Form->submit('Find', array('class' => 'searchbtn', 'title' => 'Search'));
		?>
	</div>
	<?php } ?>
	<div class="clear"></div>
</div>
<div class="page-status">
	<div class="page-status-inner">
		<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages} &raquo; showing {:current} records out of {:count} total &raquo; starting on record {:start} &raquo; ending on {:end}')
		));
		?>
	</div>
</div>