<ul class="pagination pagination-sm pull-right">
	<?php		
		echo '<li>'.$this->Paginator->prev(__(' << Previous'), array(), null, array('class' => 'prev-disabled')).'</li>';
		echo '<li>'.$this->Paginator->first('First Page', array(), null, array('class' => 'first')).'</li>';
		echo '<li>'.$this->Paginator->numbers(array('separator' => '', )).'</li>';
		echo '<li>'.$this->Paginator->last('Last Page').'</li>';
		echo '<li>'.$this->Paginator->next(__(' Next >> '), array(), null, array('class' => 'next-disabled')).'</li>';
	?>
</ul>
