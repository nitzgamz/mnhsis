
<table class="table table-condensed table-striped" id="dtable">
		<thead>
			<tr>
<?php
		foreach($headers as $header):
			echo '<th ';
				if(in_array($header, $disable_sort)){
					echo ' class="no-sort" ';
				}
			echo '>'.$header.'</th>';
		endforeach;
		echo '<th class="actions no-sort">Actions</th>';
?>
	</tr>
	</thead>
	<tbody>
