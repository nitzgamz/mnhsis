<div class="btn-group">
	<?php $this->App->linkbtn('System Access', 'groups', 'index'); ?>
	<?php $this->App->linkbtn('Change Password', 'users', 'changepassword'); ?>
	<?php $this->App->linkbtn('My Profile', 'users', 'myprofile'); ?>
	<?php $this->App->linkbtn('S.Y.', 'schoolyears', 'index'); ?>
	<?php $this->App->linkbtn('S..M.', 'mos', 'index'); ?>
	<?php $this->App->linkbtn('Province', 'provinces', 'index'); ?>
	<?php $this->App->linkbtn('Municipal', 'cities', 'index'); ?>
	<?php $this->App->linkbtn('Brgy', 'barangays', 'index'); ?>
</div>
<?php $this->App->clear(); ?>