<?php $user = $this->requestAction('users/loggeduser'); ?>
<div id="navigation2" style="padding-left: 0">	
	<div class="btn-group">		
		<?php echo $this->Html->link('Add New', array('controller' => 'teachers', 'action' => 'add'), array('class' => 'btn btn-sm btn-primary')); ?>
		<?php echo $this->Html->link('Browse', array('controller' => 'teachers', 'action' => 'index'), array('class' => 'btn btn-sm btn-primary')); ?>			
		<?php echo $this->Html->link('Tag / Assign Subject', array('controller' => 'teachersubjects', 'action' => 'add'), array('class' => 'btn btn-sm btn-primary')); ?>		
  
		<?php echo $this->Html->link('Browse Subjects', array('controller' => 'schoolsubjects', 'action' => 'index'), array('class' => 'btn btn-sm btn-primary')); ?>				  
		<?php echo $this->Html->link('Add New Subject', array('controller' => 'schoolsubjects', 'action' => 'add'), array('class' => 'btn btn-sm btn-primary')); ?>				  			  			  
			
	</div>	
</div>
