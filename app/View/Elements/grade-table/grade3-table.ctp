<table class="table" style="font: bold 11px arial, heveltica, sans-serif; margin-top: 5px;" cellspacing="0" width="<?php echo $width; ?>%">
					<tr>
						<td>Grade I - School</td>
						<td style="border-bottom: 1px solid #000;" colspan="6">SAN FERNANDO ES</td>
					</tr>
					<tr>
						<td>School Year</td>
						<td style="border-bottom: 1px solid #000;" colspan="6">2010 - 2011</td>
					</tr>
					
					
					<tr>
						<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
						<td colspan="5" style="text-align: center; vertical-align: middle; border: 1px solid #000;">Periodic Rating</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr>
						<td style="border: 1px solid #000; border-top: 0; text-align: center; vertical-align: top;">Learning Areas</td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">FR</td>
						<td style="border: 1px solid #000; border-top: 0;">Remarks</td>
					</tr>
					
					<tr class="table-information">
						<td style="border: 1px solid #000;">ENGLISH</td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr class="table-information">
						<td style="border: 1px solid #000;">MATHEMATICS</td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr class="table-information">
						<td style="border: 1px solid #000;">SCIENCE & HEALTH</td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr class="table-information">
						<td style="border: 1px solid #000;">FILIPINO</td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr class="table-information">
						<td style="border: 1px solid #000;">MAKABAYAN</td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr class="table-information">
						<td style="border: 1px solid #000;"><ul style="padding: 0; margin: 0;"><li style="padding-left: 15px;">Sibika at Kultura</li></ul></td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr class="table-information">
						<td style="border: 1px solid #000;"><ul style="padding: 0; margin: 0;"><li style="padding-left: 15px;">Values Education</li></ul></td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					<tr class="table-information">
						<td style="border: 1px solid #000;"><ul style="padding: 0; margin: 0;"><li style="padding-left: 15px;">Values Education</li></ul></td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					<tr class="table-information">
						<td style="border: 1px solid #000;"><ul style="padding: 0; margin: 0;"><li style="padding-left: 15px;">Values Education</li></ul></td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr class="table-information">
						<td style="border: 1px solid #000;">MOTHER TONGUE</td>
						<td style="border: 1px solid #000;">1</td>
						<td style="border: 1px solid #000;">2</td>
						<td style="border: 1px solid #000;">3</td>
						<td style="border: 1px solid #000;">4</td>
						<td style="border: 1px solid #000;">00</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					<tr class="table-information">
						<td style="border: 1px solid #666; border-right: 0;">General Average</td>
						<td style="border: 1px solid #666; border-left: 0; text-align: right;" colspan="6">88%</td>
					</tr>
					<tr>						
						<td style="text-align: center;" colspan="7">Eligible for Admission to Grade <u>Two</u></td>
					</tr>
				</table>