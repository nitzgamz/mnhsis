<div style="text-align: center; padding: 10px 0; font: bold 14px arial, heveltica, sans-serif;">
KATIBAYAN SA PAGLIPAT<br />
(CERTIFICATE OF TRANSFER)
</div>
<div style="text-align: left; padding: 10px 0; font: bold 12px arial, heveltica, sans-serif;">
TO WHOM IT MAY CONCERN:
<br /><br />
This is to certify that this is a true record of the Elementary Record of ____________________________. He/She is eligible for transfer and admission to Grade/Year __________.								
</div>
<table class="table" style="font: bold 12px arial, heveltica, sans-serif; text-align: center; margin-top: 20px;" cellspacing="0" width="100%">
	<tr>
		<td>SHARON PEARL O. ALMUCERA <br /> Class Adviser </td>
		<td>LOUELLA LUISA A. MILLAN <br /> Principal I </td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">DEMOSTHENES J. QUINAL <br /> PESS Supervisor </td>		
	</tr>
</table>