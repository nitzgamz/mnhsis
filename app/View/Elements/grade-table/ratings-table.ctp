<?php
	$ratings = $this->requestAction(array('controller' => 'ratings', 'action' => 'getallratings'));
?>
<table class="table" style="font: bold 11px arial, heveltica, sans-serif; margin-top: 20px;" cellspacing="0" width="<?php echo $width; ?>%">
					
					<tr>
						<td colspan="16" style="font-size: 18px;">II. RATINGS IN CHARACTER TRAITS</td>						
					</tr>
					
					
					<tr>
						<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
						<td colspan="5" style="text-align: center; vertical-align: middle; border: 1px solid #000;">Grade I Sch. Year 2012</td>						
						<td colspan="5" style="text-align: center; vertical-align: middle; border: 1px solid #000;">Grade II Sch. Year 2012</td>						
						<td colspan="5" style="text-align: center; vertical-align: middle; border: 1px solid #000;">Grade III Sch. Year 2012</td>						
					</tr>
					
					<tr>
						<td style="border: 1px solid #000; border-top: 0;">&nbsp;</td>
						<?php echo $this->element('grade-table/ratings-grade-table', array()); ?>														
						<?php echo $this->element('grade-table/ratings-grade-table', array()); ?>														
						<?php echo $this->element('grade-table/ratings-grade-table', array()); ?>														
						
						
					</tr>
					
					<?php
						$i=0;
						foreach($ratings as $rating):
						$i++;
					?>
						<tr class="table-information">
							<td style="border: 1px solid #000;"><?php echo $i.'.'. $rating['Rating']['name']; ?></td>
							
							
							<?php echo $this->element('grade-table/ratings-grade-table-val', array()); ?>
							<?php echo $this->element('grade-table/ratings-grade-table-val', array()); ?>
							<?php echo $this->element('grade-table/ratings-grade-table-val', array()); ?>
							
							
						</tr>
					<?php
							
						endforeach;
					?>
					
					
					
										
				</table>
				
				
<table class="table" style="font: bold 11px arial, heveltica, sans-serif; margin-top: 10px;" cellspacing="0" width="<?php echo $width; ?>%">
									
					<tr>
						<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
						<td colspan="5" style="text-align: center; vertical-align: middle; border: 1px solid #000;">Grade I Sch. Year 2012</td>						
						<td colspan="5" style="text-align: center; vertical-align: middle; border: 1px solid #000;">Grade II Sch. Year 2012</td>						
						<td colspan="5" style="text-align: center; vertical-align: middle; border: 1px solid #000;">Grade III Sch. Year 2012</td>						
					</tr>
					
					<tr>
						<td style="border: 1px solid #000; border-top: 0;">&nbsp;</td>
						<?php echo $this->element('grade-table/ratings-grade-table', array()); ?>														
						<?php echo $this->element('grade-table/ratings-grade-table', array()); ?>														
						<?php echo $this->element('grade-table/ratings-grade-table', array()); ?>														
						
						
					</tr>
					
					<?php
						$i=0;
						foreach($ratings as $rating):
						$i++;
					?>
						<tr class="table-information">
							<td style="border: 1px solid #000;"><?php echo $i.'.'. $rating['Rating']['name']; ?></td>
							
							
							<?php echo $this->element('grade-table/ratings-grade-table-val', array()); ?>
							<?php echo $this->element('grade-table/ratings-grade-table-val', array()); ?>
							<?php echo $this->element('grade-table/ratings-grade-table-val', array()); ?>
							
							
						</tr>
					<?php
							
						endforeach;
					?>
					
					
					
										
				</table>
<div style="text-align: center; padding: 20px 0; font: bold 14px arial, heveltica, sans-serif;">
	A - Outstanding	B - Very Satisfactory	 C - Satisfactory      D - Needs Improvement
</div>

<table class="table" style="font: bold 11px arial, heveltica, sans-serif; margin-top: 20px;" cellspacing="0" width="<?php echo $width; ?>%">
					
					<tr>
						<td colspan="7" style="font-size: 18px;">III.</td>						
					</tr>
					
					
					<tr style="text-align: center;">
						<td style="border: 1px solid #000; border-bottom: 0;">Baitang <br /> ( Grade )</td>
						<td style="border: 1px solid #000; border-bottom: 0;">Bilang ng <br /> Araw ng may <br /> pasok <br /> ( No. of School days )</td>
						<td style="border: 1px solid #000; border-bottom: 0;">Bilang ng <br /> Araw na <br /> lumiban sa <br /> klase <br /> ( No. of School days absent )</td>
						<td style="border: 1px solid #000; border-bottom: 0;">Pangunahing dahilan <br /> ( Chief Cause's )</td>
						<td style="border: 1px solid #000; border-bottom: 0;">Bilang nahuli ng <br /> Pagpasok <br /> ( No. of Times tardy )</td>
						<td style="border: 1px solid #000; border-bottom: 0;">Pangunahing Dahilan <br /> ( Cheif Cause's )</td>
						<td style="border: 1px solid #000; border-bottom: 0;">Bilang ng <br /> Araw na <br /> Pagpasok <br /> ( No. of School days present )</td>
						
					</tr>
					
					<tr>
						<td style="border: 1px solid #000;">One</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr>
						<td style="border: 1px solid #000;">Two</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr>
						<td style="border: 1px solid #000;">Three</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr>
						<td style="border: 1px solid #000;">Four</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr>
						<td style="border: 1px solid #000;">Five</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
					<tr>
						<td style="border: 1px solid #000;">Six</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
						<td style="border: 1px solid #000;">&nbsp;</td>
					</tr>
					
</table>
<?php echo $this->element('grade-table/ratings-footer'); ?>