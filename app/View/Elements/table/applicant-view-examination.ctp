<div class="result-box">
	<h2 style="margin: 0;"><?php echo __('Examination History'); ?></h2>
	<div class="inner">
	<?php echo $this->element('table/table-header', array(
		'headers' => array(
			'Region / Area',
			'Date of Examination',
			'Banned Until',
			'Status',
			'Actions'), 'navigation' => false)); 
		?>
	<?php if (!empty($examinations)){ ?>
	<?php 
		$i=0; foreach ($examinations as $exam): $i++;
		echo $this->element('table/table-information', array('i' => $i));
	?>			
		<td><?php echo $this->Externalfunction->regioninformation($exam['Examination']['region_id'], 'name') . __(' / ').$this->Externalfunction->getareainformation($exam['Examination']['area_id'], 'name') .__(' '); ?></td>
		<?php // $this->Externalfunction->getareamanager($exam['Examination']['region_id'], $exam['Examination']['area_id']); ?>							
		<td><?php echo date('d M Y', strtotime($exam['Examination']['examination_date'])); ?></td>		
		<td><?php echo date('d M Y', strtotime($exam['Examination']['banned_until'])); ?></td>		
		<td><?php echo __('( ') . $this->Externalfunction->examinationstatus($exam['Examination']['examinationstatus_id']) .__(' )'); ?></td>		
		<td class="actions">
			<?php echo $this->Html->link('Details', array('controller' => 'examinations', 'action' => 'view', $exam['Examination']['id']), array('class' => 'view')); ?>
		</td>
	<?php endforeach; ?>
	<?php }else{ ?>
		<?php echo $this->element('table/empty-related', array('outputext' => 'No Assigned Examination', 'colspan' => 6)); ?>
	<?php } ?>
	<?php echo $this->element('table/table-footer', array('navigation' => false)); ?>
</td>
</td>