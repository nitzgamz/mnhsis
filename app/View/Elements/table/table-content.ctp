<?php	
	if(!empty($fields) && !empty($model) && !empty($controller)){
		foreach($fields as $fld):
			switch($fld){
				case "region_id": $output_field = $controller['Region']['name']; break;				
				case "area_id": $output_field = $controller['Area']['name']; break;				
				case "exam_start": $output_field = date('F d, Y', strtotime($controller[$model][$fld])); break;				
				default: $output_field = $controller[$model][$fld]; break;
			}
			echo '<td>'.$output_field.'</td>';			
		endforeach;		
		echo '<td>';
					if(!empty($edit) && ($edit)){						
						echo $this->Html->link('Edit', array('action' => 'edit',  $controller[$model]['id']));
						echo __('&nbsp;|&nbsp;');
					}
					
					if(!empty($update) && ($update)){						
						echo $this->Html->link('Update', array('action' => 'edit',  $controller[$model]['id']));
						echo __('&nbsp;|&nbsp;');
					}
					
					if(!empty($delete) && ($delete)){						
						echo $this->Html->link('Delete', array('action' => 'edit',  $controller[$model]['id']));
						echo __('&nbsp;|&nbsp;');
					}
					
					if(!empty($view) && ($view)){						
						echo $this->Html->link('View', array('action' => 'view', 'controller' => $controller_url,  $controller[$model]['id']));
						echo __('&nbsp;|&nbsp;');
					}
					
					if(!empty($reschedule) && ($reschedule)){						
						echo $this->Html->link('Re-schedule', array('action' => 'reschedule', 'controller' => $controller_url, $controller[$model]['id']), array('class' => 'thickbox'));
					}
								
		echo '</td>';
	}	
?>