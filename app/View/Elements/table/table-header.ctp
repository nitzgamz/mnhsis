<?php
	if(!empty($searchtype)){
		$searchtype = $searchtype;
	}else{
		$searchtype = '';
	}
	if($navigation){
		if(!empty($model) && !empty($controller) && !empty($action)){
			$model = $model;
			$controller = $controller;
			$action = $action;
		}else{
			$model = null;
			$controller = null;
			$action = null;
		}
		echo $this->element('navigation/page.up', array('model' => $model, 'controller' => $controller, 'action' => $action, 'searchtype' => $searchtype));
	}
	echo '<table>';	
	if(!empty($headers)){		
		echo '<tr class="table-header">';
			foreach($headers as $header):
				echo '<th>'.$header.'</th>';
			endforeach;
		echo '</tr>';
	}
?>