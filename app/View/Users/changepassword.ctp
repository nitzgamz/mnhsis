<?php echo $group==1 ? $this->element('submenu/settings') : ''; ?>
<div class="col-md-4 nopadding">
	<div class="panel panel-danger col-md-12 nopadding">
		<div class="panel-heading">Change Password</div>
		<div class="panel-body">
<?php 
	echo $this->Form->create('User');	

	echo $this->Form->input('oldpassword', array('label' => 'Old Password', 'class' => 'form-control input-sm', 'type' => 'password'));
	echo $this->Form->input('newpassword', array('label' => 'New Password', 'class' => 'form-control input-sm', 'type' => 'password'));
	echo $this->Form->input('conpassword', array('label' => 'Confirm Password', 'class' => 'form-control input-sm', 'type' => 'password'));
					
	echo $this->Form->end(array('class' => 'btn btn-success btn-sm', 'label' => 'Save Changes'));
?>
	</div>
	</div>
</div>
<?php $this->App->clear(); ?>