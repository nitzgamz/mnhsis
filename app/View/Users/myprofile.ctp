<?php echo $user['User']['group_id']==1 ? $this->element('submenu/settings') : ''; ?>
<?php if($user['User']['group_id']==1){ ?>
<div class="col-md-4 nopadding">
	<div class="panel panel-success col-md-12 nopadding">
		<div class="panel-heading">My Profile</div>
		<div class="panel-body">
			<table class="table table-condensed table-bordered">
				<tr><td>Name</td><td><?php echo $user['User']['firstname'].' '.$user['User']['middlename'].' '.$user['User']['lastname']; ?></td></tr>
				<tr><td>Email</td><td><?php echo $user['User']['email']; ?></td></tr>
				<tr><td>Last Login</td><td><?php echo date('d M Y', strtotime($user['User']['lastlogin'])); ?></td></tr>
			</table>
		</div>
	</div>
</div>
<?php }else{ ?>
<div class="col-md-4 nopadding-left">
	<div class="panel panel-success col-md-12 nopadding">
		<div class="panel-heading">My Profile</div>
		<div class="panel-body">
			<table class="table table-condensed table-bordered">
				<tr><td>Name</td><td class="bolder"><?php echo $teacher['Teacher']['name']; ?></td></tr>
				<tr><td>Sex</td><td class="bolder"><?php echo $teacher['Teacher']['sex']; ?></td></tr>
				<tr><td>Date of Birth</td><td class="bolder"><?php echo date('d M Y', strtotime($teacher['Teacher']['dob'])); ?></td></tr>
				<tr><td>Place of Birth</td><td class="bolder"><?php echo $teacher['Teacher']['pob']; ?></td></tr>
				<tr><td>Address</td><td class="bolder"><?php echo $teacher['Teacher']['address']; ?></td></tr>
				<tr><td>Email</td><td class="bolder"><?php echo $teacher['Teacher']['email']; ?></td></tr>
				<tr><td>Contact</td><td class="bolder"><?php echo $teacher['Teacher']['contact']; ?></td></tr>
				<tr><td>Employment Status</td><td class="bolder"><?php echo $teacher['Teacher']['employment_status']; ?></td></tr>
				<tr><td>Other Information</td><td class="bolder"><?php echo $teacher['Teacher']['others']; ?></td></tr>
			</table>
		</div>
	</div>
</div>

<div class="col-md-4 nopadding-left">
	<div class="panel panel-success col-md-12 nopadding">
		<div class="panel-heading">Other Details</div>
		<div class="panel-body">
			<table class="table table-condensed table-bordered">
				<tr><td>Grade Level</td><td class="bolder"><?php echo $teacher['Gradelevel']['name']; ?></td></tr>
				<tr><td>Secion</td><td class="bolder"><?php echo $teacher['Gradesection']['name']; ?></td></tr>
				<tr><td>Total Students</td><td class="bolder"><?php echo count($teacher['Student']); ?></td></tr>
			</table>
		</div>
	</div>
</div>

<div class="col-md-3 nopadding-left nodisplay">
	<div class="panel panel-success col-md-12 nopadding">
		<div class="panel-heading">Subjects</div>
		<div class="panel-body">
			
				<?php //if (!empty($teacher['Teachersubject'])):?>	
				<ul>
				<?php
					//foreach ($teacher['Teachersubject'] as $teachersubject): 											
							//echo '<li>'.$this->Externalfunction->getnamebyid($teachersubject['schoolsubject_id'], 'schoolsubjects', 'getnamebyid').'</li>';				
					//endforeach; ?>		
				<?php //endif; ?>
				</ul>
					
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<?php } ?>
<?php $this->App->clear(); ?>