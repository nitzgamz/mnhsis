<div class="schoolsubjects view">
<h2><?php  echo __('Schoolsubject');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($schoolsubject['Schoolsubject']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($schoolsubject['Schoolsubject']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($schoolsubject['Schoolsubject']['code']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Schoolsubject'), array('action' => 'edit', $schoolsubject['Schoolsubject']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Schoolsubject'), array('action' => 'delete', $schoolsubject['Schoolsubject']['id']), null, __('Are you sure you want to delete # %s?', $schoolsubject['Schoolsubject']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolsubjects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolsubject'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolgrades'), array('controller' => 'schoolgrades', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolgrade'), array('controller' => 'schoolgrades', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Schoolgrades');?></h3>
	<?php if (!empty($schoolsubject['Schoolgrade'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Student Id'); ?></th>
		<th><?php echo __('Schoolsubject Id'); ?></th>
		<th><?php echo __('First Quarter'); ?></th>
		<th><?php echo __('Second Quarter'); ?></th>
		<th><?php echo __('Third Quarter'); ?></th>
		<th><?php echo __('Fourth Quarter'); ?></th>
		<th><?php echo __('Quarter Tgrade'); ?></th>
		<th><?php echo __('Tgrade'); ?></th>
		<th><?php echo __('Ggrade'); ?></th>
		<th><?php echo __('Sy'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Added'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($schoolsubject['Schoolgrade'] as $schoolgrade): ?>
		<tr>
			<td><?php echo $schoolgrade['id'];?></td>
			<td><?php echo $schoolgrade['student_id'];?></td>
			<td><?php echo $schoolgrade['schoolsubject_id'];?></td>
			<td><?php echo $schoolgrade['first_quarter'];?></td>
			<td><?php echo $schoolgrade['second_quarter'];?></td>
			<td><?php echo $schoolgrade['third_quarter'];?></td>
			<td><?php echo $schoolgrade['fourth_quarter'];?></td>
			<td><?php echo $schoolgrade['quarter_tgrade'];?></td>
			<td><?php echo $schoolgrade['tgrade'];?></td>
			<td><?php echo $schoolgrade['ggrade'];?></td>
			<td><?php echo $schoolgrade['sy'];?></td>
			<td><?php echo $schoolgrade['user_id'];?></td>
			<td><?php echo $schoolgrade['added'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'schoolgrades', 'action' => 'view', $schoolgrade['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'schoolgrades', 'action' => 'edit', $schoolgrade['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'schoolgrades', 'action' => 'delete', $schoolgrade['id']), null, __('Are you sure you want to delete # %s?', $schoolgrade['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Schoolgrade'), array('controller' => 'schoolgrades', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
