<div class="periodicothers view">
<h2><?php echo __('Periodicother'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($periodicother['Periodicother']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Student'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicother['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $periodicother['Student']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gradelevel'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicother['Gradelevel']['name'], array('controller' => 'gradelevels', 'action' => 'view', $periodicother['Gradelevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gradesection'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicother['Gradesection']['name'], array('controller' => 'gradesections', 'action' => 'view', $periodicother['Gradesection']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Schoolyear'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicother['Schoolyear']['name'], array('controller' => 'schoolyears', 'action' => 'view', $periodicother['Schoolyear']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Teacher'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicother['Teacher']['name'], array('controller' => 'teachers', 'action' => 'view', $periodicother['Teacher']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Advance Units'); ?></dt>
		<dd>
			<?php echo h($periodicother['Periodicother']['advance_units']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lacks Units'); ?></dt>
		<dd>
			<?php echo h($periodicother['Periodicother']['lacks_units']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Classified As'); ?></dt>
		<dd>
			<?php echo h($periodicother['Periodicother']['classified_as']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Added'); ?></dt>
		<dd>
			<?php echo h($periodicother['Periodicother']['added']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Periodicother'), array('action' => 'edit', $periodicother['Periodicother']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Periodicother'), array('action' => 'delete', $periodicother['Periodicother']['id']), array(), __('Are you sure you want to delete # %s?', $periodicother['Periodicother']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Periodicothers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Periodicother'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradelevels'), array('controller' => 'gradelevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradelevel'), array('controller' => 'gradelevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradesections'), array('controller' => 'gradesections', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradesection'), array('controller' => 'gradesections', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolyears'), array('controller' => 'schoolyears', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolyear'), array('controller' => 'schoolyears', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
	</ul>
</div>
