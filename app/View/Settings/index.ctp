<?php echo $this->element('submenu/settings'); ?>
<div class="settings index">
	<?php $headers = array("School Name", "Address", "Other Details"); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>
	
	<?php
	$i = 0;
	foreach ($settings as $setting): ?>
	<tr>
		<td><?php echo h($setting['Setting']['school_name']); ?>&nbsp;</td>
		<td><?php echo h($setting['Setting']['address']); ?>&nbsp;</td>
		<td><?php echo h($setting['Setting']['others']); ?>&nbsp;</td>
		<td class="actions">
			<?php $this->App->linkbtn('Change', 'settings', 'edit', $setting['Setting']['id']); ?>
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $setting['Setting']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $setting['Setting']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $setting['Setting']['id']), null, __('Are you sure you want to delete # %s?', $setting['Setting']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
</div>
<?php (count($settings) <=0 ? $this->App->linkbtn('Create', 'settings', 'add') : ''); ?>
