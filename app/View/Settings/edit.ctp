
<div class="settings form col-md-6 nopadding">
<?php echo $this->Form->create('Setting');?>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('school_name', array('label' => 'School Name', 'rows' => 1, 'class' => 'form-control input-sm'));
		echo $this->Form->input('address', array('label' => 'Complete Address', 'rows' => 1, 'class' => 'form-control input-sm'));
		echo $this->Form->input('others', array('label' => 'Other Details', 'rows' => 1, 'class' => 'form-control input-sm'));
		
	?>

<?php echo $this->App->formbtn('Save Changes'); ?>
</div>

