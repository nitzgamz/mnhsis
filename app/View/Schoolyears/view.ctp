<div class="schoolyears view">
<h2><?php  echo __('Schoolyear');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($schoolyear['Schoolyear']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sy From'); ?></dt>
		<dd>
			<?php echo h($schoolyear['Schoolyear']['sy_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sy To'); ?></dt>
		<dd>
			<?php echo h($schoolyear['Schoolyear']['sy_to']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Schoolyear'), array('action' => 'edit', $schoolyear['Schoolyear']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Schoolyear'), array('action' => 'delete', $schoolyear['Schoolyear']['id']), null, __('Are you sure you want to delete # %s?', $schoolyear['Schoolyear']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolyears'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolyear'), array('action' => 'add')); ?> </li>
	</ul>
</div>
