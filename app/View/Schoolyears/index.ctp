<div class="schoolyears index col-md-4 nopadding">
	<?php echo $this->element('submenu/subjects'); ?>	
	<?php $headers = array("S.Y."); ?>
	<?php echo $this->element('table-header2', array('headers' => $headers, 'disable_sort' => array())); ?>

	<?php
	foreach ($schoolyears as $schoolyear): ?>
	<tr>		
		<td><?php echo $schoolyear['Schoolyear']['sy_from'].' - '.$schoolyear['Schoolyear']['sy_to']; ?>&nbsp;</td>		
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $schoolyear['Schoolyear']['id'])); ?>
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $schoolyear['Schoolyear']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false)); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $schoolyear['Schoolyear']['id']), null, __('Are you sure you want to delete # %s?', $schoolyear['Schoolyear']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<?php //echo $this->element('bottom.navigation'); ?>
</div>

