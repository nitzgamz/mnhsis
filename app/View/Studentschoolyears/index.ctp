<div class="students index">
	<?php echo $this->element('submenu/student'); ?>
	<div class="col-md-12 col-lg-12 col-xs-12" style="padding: 2px 8px 10px 8px; border: 1px solid #ccc;  margin: 5px 0 10px 0; background: #f4f2f2;">
	<div class="col-md-12 col-lg-12 col-xs-12 pull-left" style="padding-left: 0;">
		<h3 style="padding-bottom: 0; margin-bottom: 0;">Student's Masterlist</h3>
	</div>
	
	
			<?php
				if($search){
					echo '<div class="col-md-12 col-lg-12 col-xs-12 pull-left" style="padding-left: 0; border-left: 5px solid green; background: #fff; padding: 2px 5px;">';
					echo __('Search Results for - ');
					echo __('Keyword: ');
					echo __('&nbsp;'). $student_keyword . __('&nbsp;');
					echo __('LRN: ');
					echo $student_lrn;									
					echo '</div>';
				}
				
				if($filter){
					echo '<div class="col-md-12 col-lg-12 col-xs-12 pull-left" style="padding-left: 0; border-left: 5px solid green; background: #fff; padding: 2px 5px;">';
					echo __('Filter Results for - ');
					echo __('S.Y. ');
					echo __('&nbsp;'). (!empty($sy) ? $sy.'-'.($sy + 1) : '_') . __(' | &nbsp;');
					echo __('Grade Level: ');
					echo __('&nbsp;'). (!empty($grade['Gradelevel']['name']) ? $grade['Gradelevel']['name'] : '_') . __(' | &nbsp;');	
					echo __('Section: ');
					echo (!empty($section['Gradesection']['name']) ? $section['Gradesection']['name'] : '_');
					echo '</div>';
				}
			?>			
	
	
	
	
	
	
	<div class="clear"></div>
	
	<div class="col-md-5 col-lg-5 col-xs-5 pull-left no-padding-left" style="background: #ccc; padding-bottom: 10px;">
		<?php
			echo $this->Form->create('Search', array('type' => 'get'));					
			echo '<div class="col-md-5 col-lg-5 col-xs-5" style="padding-right: 0;">';
				echo '<label for="">Keyword</label>';
				echo $this->Form->input('keyword', array('type' => 'text', 'label' => '', 'placeholder' => 'lastname / firstname ....'));
			echo '</div>';
			echo '<div class="col-md-5 col-lg-5 col-xs-5" style="padding-left: 0; padding-right: 0;">';
				echo '<label for="">LRN</label>';
				echo $this->Form->input('lrnkeyword', array('type' => 'text', 'label' => '', 'placeholder' => 'Lrn ....'));
			echo '</div>';
			echo '<div class="col-md-2 col-lg-2 col-xs-2">';
				echo '<br />';
				echo $this->Form->submit('Search', array('class' => 'btn btn-primary')); 
			echo '</div>';
		?>
	</div>
	
	<div class="col-md-6 col-lg-6 col-xs-6 no-padding-right no-padding-left" style="margin-left: 5px; background: #ccc; padding: 5px 20px 10px 20px;">
		<?php
			echo $this->Form->create('Filter', array('type' => 'get'));
			echo '<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left no-padding-right" style="padding-right: 0;">';				
				echo '<label for="">S.Y.</label><br />';
				//echo $this->Form->input('schoolyear', array('options' => $schoolyears, 'label' => '', 'class' => 'normalinput2'));
				//echo $this->Form->input('schoolyear', array('type' => 'date', 'dateFormat' => 'Y', 'label' => '', 'minYear' => date('Y')-10, 'maxYear' => date('Y')+1, 'class' => 'normalinput2'));	
				echo '<select name="schoolyear" class="normalinput2">';
				foreach($schoolyears as $sy):
					echo '<option value="'.$sy['Studentschoolyear']['sy'].'">'.$sy['Studentschoolyear']['sy'].' - '.($sy['Studentschoolyear']['sy'] + 1).'</option>';
				endforeach;
				echo '</select>';
			echo '</div>';
				echo '<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left no-padding-right" style="padding-right: 0;">';				
				echo '<label for="">Grade Level</label>';
				echo $this->Form->input('gradelevel_id', array('label' => '', 'class' => 'normalinput2', 'id' => 'gradelevel_id'));
			echo '</div>';
				echo '<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left no-padding-right">';				
				echo '<label for="">Section</label>';
				echo $this->Form->input('gradesection_id', array('id' => 'gradesection_id', 'label' => '', 'class' => 'normalinput2', 'empty' => '--Select--'));
			echo '</div>';
			echo '<div class="col-md-1 col-lg-1 col-xs-1 no-padding-left no-padding-right" style="margin-left: 5px;">';
				echo '<br />';
				echo $this->Form->submit('Filter Search', array('class' => 'btn btn-primary')); 
			echo '</div>';
		?>
	</div>
	
	<div class="clear"></div>
	</div>
	
	<?php //echo $this->element('top.navigation'); ?>
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th><?php echo $this->Paginator->sort('lrn');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('firstname');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('MI');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('lastname');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('gender');?><span class="caret"></span></th>
			<th><?php echo $this->Paginator->sort('birthdate');?><span class="caret"></span></th>	
			<th class=""><?php echo __('Age');?></th>			
			
			<th class="actions"><?php echo __('S.Y.');?></th>
			
			
			<th class="actions"><?php echo __('Grade');?></th>
			<th><?php echo $this->Paginator->sort('section_id');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($students as $student): ?>
	<tr>		
		<td><?php echo h($student['Student']['lrn']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['firstname']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['middlename']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['lastname']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['gender']); ?>&nbsp;</td>
		<td><?php echo h($student['Student']['birthdate']); ?>&nbsp;</td>		
		<td><?php 
			echo floor((time() - strtotime($student['Student']['birthdate'])) / 31556926);
		?></td>				
		<!--<td>
			<?php echo $student['Student']['indicator']; ?>
		</td>-->
		<td><?php echo h($student['Studentschoolyear']['sy']); ?>&nbsp;</td>		
		<!--<td>		
			<?php echo $this->Externalfunction->teachername($student['Gradesection']['id']); ?>
		</td>-->
		
		
		
		<td>
			<?php echo $student['Gradelevel']['name']; ?>
		</td>
		<td>
			<?php echo $student['Gradesection']['name']; ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('controller' => 'students', 'action' => 'view', $student['Student']['id'], $student['Studentschoolyear']['gradelevel_id'], $student['Studentschoolyear']['gradesection_id'], $student['Studentschoolyear']['sy'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('controller' => 'students', 'action' => 'edit', $student['Student']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $student['Student']['id']), null, __('Are you sure you want to delete # %s?', $student['Student']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>	
</div>

<?php
	
	$this->Js->Buffer('
		$("#gradelevel_id").bind("change", function(){
			var selected = $(this).find("option:selected").val();
			var section  = $("#gradesection_id");
			$.ajax({	
					type 	: "POST",	
									url 	: "'.$this->Html->url('/', true).'gradesections/getsectionlists_ajax",
									data 	: {"gradelevel" : selected},
									dataType: "json",
									beforeSend: function() {
										section.prop("disabled", true);
									},				
									success: function(data){	
										$("option", section).remove();
										section.append(data);
									},
									complete: function(){
										section.prop("disabled", false);
									}
								});
								
		});
	');
?>