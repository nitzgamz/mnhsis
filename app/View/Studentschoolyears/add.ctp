<div class="studentschoolyears form">
<?php echo $this->Form->create('Studentschoolyear');?>
	<fieldset>
		<legend><?php echo __('Add Studentschoolyear'); ?></legend>
	<?php
		echo $this->Form->input('student_id');
		echo $this->Form->input('schoolyear_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Studentschoolyears'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolyears'), array('controller' => 'schoolyears', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolyear'), array('controller' => 'schoolyears', 'action' => 'add')); ?> </li>
	</ul>
</div>
