<div class="schoolyears index">
	<?php echo $this->element('submenu/student'); ?>
	<h3 class="default"><?php echo __('School Year');?></h3>	
	<table class="table table-striped table-hover table-condensed">
	<tr>			
			<th class="actions"><?php echo __('S.Y.');?></th>		
	</tr>
	<?php
	$i = 0;
	foreach ($schoolyears as $schoolyear): ?>
	<tr>		
		<td><?php echo $schoolyear['Studentschoolyear']['sy'].' - '.($schoolyear['Studentschoolyear']['sy']+1); ?></td>		
				
		
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('bottom.navigation'); ?>
</div>

