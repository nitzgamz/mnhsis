<div class="studentschoolyears view">
<h2><?php  echo __('Studentschoolyear');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($studentschoolyear['Studentschoolyear']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Student'); ?></dt>
		<dd>
			<?php echo $this->Html->link($studentschoolyear['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $studentschoolyear['Student']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Schoolyear'); ?></dt>
		<dd>
			<?php echo $this->Html->link($studentschoolyear['Schoolyear']['name'], array('controller' => 'schoolyears', 'action' => 'view', $studentschoolyear['Schoolyear']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Studentschoolyear'), array('action' => 'edit', $studentschoolyear['Studentschoolyear']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Studentschoolyear'), array('action' => 'delete', $studentschoolyear['Studentschoolyear']['id']), null, __('Are you sure you want to delete # %s?', $studentschoolyear['Studentschoolyear']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Studentschoolyears'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Studentschoolyear'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolyears'), array('controller' => 'schoolyears', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolyear'), array('controller' => 'schoolyears', 'action' => 'add')); ?> </li>
	</ul>
</div>
