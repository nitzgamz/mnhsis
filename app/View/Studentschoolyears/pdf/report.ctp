<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
	<title>School Form 1</title>
	<meta name="dompdf.view" content="Fit"/>
	<meta name="Author" content="S&amp;S Enterprises"/>
	<meta name="description" content="Wish List Quote Request" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style>
		html, body{margin: 30px 30px;}		
		/*table {page-break-before: always; page-break-after: always;}*/
		@page{
			margin-top: 10px;
			margin-left: 30px;
			margin-bottom: 10px;
			margin-right: 30px;
		}
		table{border-collapse:collapse;}
		table,tbody,tr,th,td{margin:0;padding:0;}
		/*th,td{white-space:nowrap;}*/
		 #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; background-color: lightblue; }
    #footer .page:after { content: counter(page, upper-roman); }
	</style>
<body>	
<?php
	$male = 0;
	$female = 0;
	$total = 0;
?>
<div id="header">
    <h1>ibmphp.blogspot.com</h1>
  </div>
  <div id="footer">
    
  </div>
<script type="text/php">
        /*if ( isset($pdf) ) {
            $font = Font_Metrics::get_font("helvetica", "bold");
            $pdf->page_text(30, 18, "Page {PAGE_NUM} out of {PAGE_COUNT}", $font, 8, array(255,0,0));

        }*/
    </script>
	
<h2 style="text-align: center; font: bold 18px arial, heveltica, sans-serif; padding-top: 0; margin-top: 0;">
	School Form 5 ( SF 5 ) Report on Promotion & Level of Profeciency
	<div style="font-size: 10px; font-style: italic;">( This replaced Forms 18-E1, 18-E2 and List of Graduates )</div>
</h2>


<table style="font: bold 12px arial, heveltica, sans-serif;" cellpadding="3" cellspacing="2" width="100%">
	
	<tr>
		<td rowspan="4" style="width: 150px; vertical-align: top;">
			<img src="<?php echo APP; ?>/webroot/img/1.png" style="width: 80%;"/>
		</td>
		<td style="text-align: right;">Region</td><td style="border: 1px solid #666; text-align: left;">CARAGA</td>
		<td style="text-align: right;">Division</td><td style="border: 1px solid #666; text-align: left;">BISLIG CITY</td>
		<td style="text-align: right;">District</td><td style="border: 1px solid #666; text-align: left;">BISLIG I</td>
		<td rowspan="4" style="width: 240px; vertical-align: top; text-align: right;">
			<img src="<?php echo APP; ?>/webroot/img/2.png" style="width: 80%;"/>
		</td>
	</tr>
	<tr>
		<td style="text-align: right;">School ID</td><td style="border: 1px solid #666;">132609</td>
		<td style="text-align: right;">School Year</td><td style="border: 1px solid #666;"><?php echo $sy.'-'.($sy+1); ?></td>
		<td style="text-align: right;">Curriculum</td><td style="border: 1px solid #666; text-transform: uppercase;">RBEC</td>			
	</tr>
	<tr>
		<td style="text-align: right;">School Name</td><td colspan="5" style="border: 1px solid #666;">SAN FERNANDO ELEMENTARY SCHOOL</td>
	</tr>
	<tr>
		<td style="text-align: right;">Grade Level</td><td colspan="2"style="border: 1px solid #666; text-transform: uppercase;"><?php echo $gradelevel['Gradelevel']['name']; ?></td>			
		<td style="text-align: right;">Section</td><td colspan="2" style="border: 1px solid #666; text-transform: uppercase;"><?php echo $section['Gradesection']['name']; ?></td>
		
	</tr>
	
</table>

<table width="100%" cellspacing="0" cellpadding="0" style="margin-top: 20px;">
	<tr>
		<td style="vertical-align: top; width: 80%; padding-right: 10px;">

			<table style="font: bold 11px arial, heveltica, sans-serif; " cellpadding="3" cellspacing="0" width="100%">
				
				<tr>
					<td style="border: 1px solid #000; text-align: center; border-bottom: 0;">LRN</td>
					<td style="border: 1px solid #000; text-align: center;  border-bottom: 0;">LEARNERS NAME <br /> ( Last Name, First Name, Middle Name )</td>
					<td style="border: 1px solid #000; text-align: center;  border-bottom: 0;">GENERAL AVERAGE <br />(Numerical Value in 3 decimal places for honor learner, 2 for non-honor & Descriptive Letter)</td>
					<td style="border: 1px solid #000; text-align: center;  border-bottom: 0;">ACTION TAKEN: PROMOTED, *IRREGULAR or RETAINED  </td>
					<td style="border: 1px solid #000; text-align: center;" colspan="2">
						INCOMPLETE SUBJECT/S<br />(This column is for K to 12 Curriculum and remaining RBEC in High School. Elementary grades level that still implementing RBEC need not to fill up this column)			
					</td>
					
				</tr>
				<tr>
					<td style="border: 1px solid #000; text-align: center; border-top: 0;">&nbsp;</td>
					<td style="border: 1px solid #000; text-align: center; border-top: 0;">&nbsp;</td>
					<td style="border: 1px solid #000; text-align: center; border-top: 0;">&nbsp;</td>
					<td style="border: 1px solid #000; text-align: center; border-top: 0;">&nbsp;</td>
					<td style="border: 1px solid #000; text-align: center;">Completed as of end of current SY</td>
					<td style="border: 1px solid #000; text-align: center;">as of end of the current SY</td>
				</tr>
				
				<?php
					
					if(!empty($students)){
						foreach($students as $student):
						if($student['Student']['gender']=="M"){
							$male++;				
				?>
						<tr>
							<td style="border: 1px solid #666; border-left: 1px solid #666;"><?php echo $student['Student']['lrn']; ?></td>
							<td style="border: 1px solid #666;"><?php echo $student['Student']['lastname'].', '.$student['Student']['firstname'].' '.$student['Student']['middlename']; ?></td>
							<td style="border: 1px solid #666;">&nbsp;</td>
							<td style="border: 1px solid #666;">PROMOTED</td>
							<td style="border: 1px solid #666; border-right: 1px solid #666;">&nbsp;</td>
							<td style="border: 1px solid #666; border-right: 1px solid #666;">&nbsp;</td>
						</tr>
				<?php
						}
						endforeach;
				?>
						<tr><td colspan="6" style="border: 2px solid #000; border-top: 2px solid #000;">TOTAL MALE : <?php echo $male; ?></td></tr>
				
				<?php } ?>
			</table>
		</td>
		<td style="vertical-align: top; width: 20%; padding-left: 10px;">
			<table style="font: bold 9px arial, heveltica, sans-serif; border-top: 2px solid #000; border: 1px solid #000;" cellpadding="5" cellspacing="2" width="100%">
				<tr>
					<td>SUMMARY TABLE</td>
				</tr>
				<tr>
					<td style="border-bottom: 1px solid #666;">STATUS</td>
					<td style="border-bottom: 1px solid #666;">MALE</td>
					<td style="border-bottom: 1px solid #666;">FEMALE</td>
					<td style="border-bottom: 1px solid #666;">TOTAL</td>
				</tr>
				<tr>
					<td>PROMOTED</td>
					<td><?php echo $male; ?></td>
					<td><?php echo $female; ?></td>
					<td><?php echo $male + $female; ?></td>
				</tr>
				<tr>
					<td>IRREGULAR</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>RATAINED</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			
			<table style="font: bold 9px arial, heveltica, sans-serif; border-top: 2px solid #000; margin-top: 10px; border: 1px solid #000;" cellpadding="5" cellspacing="2" width="100%">
					<tr>
						<td>LEVEL OF <br />PROFECIENCY</td>
					</tr>
					<tr>
						<td style="border-bottom: 1px solid #666;">&nbsp;</td>
						<td style="border-bottom: 1px solid #666;">MALE</td>
						<td style="border-bottom: 1px solid #666;">FEMALE</td>
						<td style="border-bottom: 1px solid #666;">TOTAL</td>
					</tr>
					<tr>
						<td>BEGINNNING <br /> (B: 74% and below)</td>
						<?php
							$male74 = $this->Externalfunction->studentprofeciency('74', '<=', 'M', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '1');
							$female74 = $this->Externalfunction->studentprofeciency('74', '<=', 'M', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '1');
						?>
						<td><?php echo $male74; ?></td>
						<td><?php echo $female74; ?></td>
						<td><?php echo $male74 + $female74; ?></td>
					</tr>
					<tr>
						<td>DEVELOPING <br /> (D: 75%-79%)</td>
						<?php
							$male75 = $this->Externalfunction->studentprofeciencylevel2('75', '79', 'M', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '1');
							$female75 = $this->Externalfunction->studentprofeciencylevel2('75', '79', 'F', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '1');
						?>
						<td><?php echo $male75; ?></td>
						<td><?php echo $female75; ?></td>
						<td><?php echo $male75 + $female75; ?></td>
					</tr>
					<tr>
						<td>APPROACHING <br />PROFICIENCY <br /> (AP: 80%-84%)</td>
						<?php
							$male80 = $this->Externalfunction->studentprofeciencylevel2('80', '84', 'M', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '1');
							$female80 = $this->Externalfunction->studentprofeciencylevel2('80', '84', 'F', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '1');
						?>
						<td><?php echo $male80; ?></td>
						<td><?php echo $female80; ?></td>
						<td><?php echo $male80 + $female80; ?></td>
					</tr>
					<tr>
						<td>PROFICIENT <br /> (P: 85% -89%)</td>
						<?php
							$male85 = $this->Externalfunction->studentprofeciencylevel2('85', '89', 'M', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '1');
							$female85 = $this->Externalfunction->studentprofeciencylevel2('85', '89', 'F', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '1');
						?>
						<td><?php echo $male85; ?></td>
						<td><?php echo $female85; ?></td>
						<td><?php echo $male85 + $female85; ?></td>
					</tr>
					<tr>
						<td>ADVANCED <br /> (A: 90%  and above)</td>
						<?php
							$male90 = $this->Externalfunction->studentprofeciency('90', '>=', 'M', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '');
							$female90 = $this->Externalfunction->studentprofeciency('90', '>=', 'M', $sy, $gradelevel['Gradelevel']['id'], $section['Gradesection']['id'], '');
						?>
						<td><?php echo $male90; ?></td>
						<td><?php echo $female90; ?></td>
						<td><?php echo $male90 + $female90; ?></td>
					</tr>
				</table>

		</td>
	</tr>
</table>





<table width="100%" cellspacing="0" cellpadding="0" style="margin-top: 20px;">
	<tr>
		<td style="vertical-align: top; width: 80%; padding-right: 10px;">

			<table style="font: bold 11px arial, heveltica, sans-serif; " cellpadding="3" cellspacing="0" width="100%">
				
				<tr>
					<td style="border: 1px solid #000; text-align: center; border-bottom: 0;">LRN</td>
					<td style="border: 1px solid #000; text-align: center;  border-bottom: 0;">LEARNERS NAME <br /> ( Last Name, First Name, Middle Name )</td>
					<td style="border: 1px solid #000; text-align: center;  border-bottom: 0;">GENERAL AVERAGE <br />(Numerical Value in 3 decimal places for honor learner, 2 for non-honor & Descriptive Letter)</td>
					<td style="border: 1px solid #000; text-align: center;  border-bottom: 0;">ACTION TAKEN: PROMOTED, *IRREGULAR or RETAINED  </td>
					<td style="border: 1px solid #000; text-align: center;" colspan="2">
						INCOMPLETE SUBJECT/S<br />(This column is for K to 12 Curriculum and remaining RBEC in High School. Elementary grades level that still implementing RBEC need not to fill up this column)			
					</td>
					
				</tr>
				<tr>
					<td style="border: 1px solid #000; text-align: center; border-top: 0;">&nbsp;</td>
					<td style="border: 1px solid #000; text-align: center; border-top: 0;">&nbsp;</td>
					<td style="border: 1px solid #000; text-align: center; border-top: 0;">&nbsp;</td>
					<td style="border: 1px solid #000; text-align: center; border-top: 0;">&nbsp;</td>
					<td style="border: 1px solid #000; text-align: center;">Completed as of end of current SY</td>
					<td style="border: 1px solid #000; text-align: center;">as of end of the current SY</td>
				</tr>
				
				<?php
					
					if(!empty($students)){							
						foreach($students as $student):
						if($student['Student']['gender']=="F"){
							$female++;
				?>
						<tr>
							<td style="border: 1px solid #666;"><?php echo $student['Student']['lrn']; ?></td>
							<td style="border: 1px solid #666;"><?php echo $student['Student']['lastname'].', '.$student['Student']['firstname'].' '.$student['Student']['middlename']; ?></td>
							<td style="border: 1px solid #666;">&nbsp;</td>
							<td style="border: 1px solid #666;">PROMOTED</td>
							<td style="border: 1px solid #666;">&nbsp;</td>
							<td style="border: 1px solid #666;">&nbsp;</td>
						</tr>
				<?php
						}
						endforeach;
				?>
						<tr><td colspan="6" style="border: 2px solid #000; border-top: 2px solid #000;">TOTAL FEMALE : <?php echo $female; ?></td></tr>
						<tr><td colspan="6" style="border: 2px solid #000; border-top: 2px solid #000;">COMBINED : <?php echo $male + $female; ?></td></tr>
				
				<?php } ?>
			</table>
		</td>
		<td style="vertical-align: top; width: 20%; padding-left: 10px;">
				
				<table style="font: bold 9px arial, heveltica, sans-serif;" cellpadding="0" cellspacing="0" width="100%">
					<tr><td>PREPARED BY:</td></tr>
					<tr><td style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
					<tr><td style="text-align: center;">CLASS ADVISER <br /> ( Name & Signature )</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>CERTIFIED CORRECT & SUBMITTED:</td></tr>
					<tr><td style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
					<tr><td style="text-align: center;">SCHOOL HEAD <br /> ( Name & Signature )</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>REVIEWED BY:</td></tr>
					<tr><td style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
					<tr><td style="text-align: center;">DIVISION REPRESENTATIVE <br /> ( Name & Signature )</td></tr>
				</table>
				<table style="font: bold 9px arial, heveltica, sans-serif; margin-top: 20px;" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>GUIDELINES: <br /><br />
							<ol>
								<li>For All Grade/Year Levels</li>
								<li>To be prepared by the Adviser.  Final rating per subject area should be taken from the record of subject teacher.  The class adviser should make the </li>
								<li>On the summary table, reflect the  total number of learners promoted, retained and irregular ( *for grade 7 onwards only) and the level of proficiency according to the individual general average</li>
								<li>Must tallied with the total enrollment report as of End of School Year GESP /GSSP (BEIS)</li>
								<li>Protocols of validation & submission will remain under the discretion of the Schools Division Superintendent</li>
							</ol>
						</td>
					</tr>
				</table>

		</td>
	</tr>
</table>




