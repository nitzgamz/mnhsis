<?php echo $this->element('submenu/student'); ?>
<div class="students form">
<?php echo $this->Form->create('Student');?>
	<?php
		echo '<div class="col-md-4 no-padding-left">';
		echo '<div class="panel panel-success col-md-12 nopadding">';
			echo '<div class="panel-heading">Student Information</div>';
			echo '<div class="panel-body">';
			
		?>
			<table class="table table-condensed smalltable">
				<tr>
					<td>Name</td><td><?php echo $this->data['Student']['full_name']; ?></td>
				</tr>
				<tr>
					<td>LRN</td><td><?php echo $this->data['Student']['lrn']; ?></td>
				</tr>
			</table>
		<?php
			echo $this->Form->input('id');
		
				echo '<div class="col-md-6 nopadding-left">';
						echo $this->Form->input('schoolyear_id', array('label' => 'S.Y.', 'empty' =>  false, 'disabled' => ($group > 1 ? true : false)));							
				echo '</div>';
				
				echo '<div class="col-md-6 nopadding-right">';
				echo '<label for="">Grade Level</label>';
					echo $this->Form->input('gradelevel_id', array('label' => false, 'empty' => $group==1 ? '-- Choose --' : false, 'id' => 'gradelevel_id', 'disabled' => ($group > 1 ? true : false)));
				echo '</div>';
				echo '<div class="col-md-6 nopadding-left">';
					echo '<label for="">Section</label>';
					echo $this->Form->input('gradesection_id', array('label' => false, 'empty' =>  $group==1 ? '-- Choose --' : false, 'id' => 'gradesection_id', 'disabled' => ($group > 1 ? true : false)));
				echo '</div>';
				
				echo '<div class="col-md-6 nopadding-right">';
					
					echo $this->Form->input('teacher_id', array('label' => 'Assign Teacher', 'empty' =>  $group==1 ? '-- Choose --' : false, 'id' => 'teacher_id', 'disabled' => ($group > 1 ? true : false)));
				echo '</div>';
			
				echo '<div class="col-md-12 nopadding">';
					echo '<label for="">Entrance Date</label>';
					echo $this->Form->input('date_of_entrance', array('label' => false), array('class' => 'birthdate'));
				echo '</div>';
			
			
			echo '</div>';
			echo '</div>';
		echo '</div>';
		
		$this->App->clear();

			echo $this->Form->submit(__('Save Changes'), array('class' => 'btn btn-primary'));
	
		echo '</div>';
		echo '</div>';
		echo '</div>';
	?>
	<div class="clear separator20"></div>

</div>

<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradelevel_id', 'update_id' => '#gradesection_id', 'controller_id' => 'gradesections', 'action_id' => 'getsectionlists', 'method' => 'post', 'form' => 'Student')); ?>
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradesection_id', 'update_id' => '#teacher_id', 'controller_id' => 'teachers', 'action_id' => 'listofteachers', 'method' => 'post', 'form' => 'Student')); ?>