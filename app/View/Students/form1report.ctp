<div class="col-md-5 col-lg-5 col-xs-5 " style="background: #ccc; border: 1px solid #ccc; border-radius: 10px 10px; margin-top: 30px;">	
<h3 class="default">School Form 1 ( SF 1 ) School Register</h3>
<div class="students form">
<?php echo $this->Form->create('Student');?>
	
	<?php
			
		/*echo '<label for="">Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => ''));
		echo '<label for="">Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => ''));
		echo '<label for="">School Year</label>';*/
		echo $this->Form->input('schoolyear_id', array('label' => 'S.Y.'));
		
		/*echo '<select name="data[Student][schoolyear]" class="normalinput2">';
			$schoolyears = array();
			for($i=date('Y')-10; $i<=date('Y')+1; $i++){			
				$schoolyears[] = $i;
			}
				foreach($schoolyears as $sy):
					echo '<option value="'.$sy.'">'.$sy.' - '.($sy + 1).'</option>';
				endforeach;
				echo '</select>';*/
		echo '<label for="">Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => '', 'empty' => false, 'id' => 'gradelevel_id'));
		echo '<label for="">Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => '', 'empty' => false, 'id' => 'gradesection_id'));
	
	?>
	<br />
	<div class="col-md-4 col-lg-4 col-xs-4 no-padding-left">
		<?php echo $this->Form->submit(__('Generate PDF Report'), array('class' => 'btn btn-success'));?>
	</div>	
	<?php 
		if($download){ 	
			
			echo '<div class="col-md-4 col-lg-4 col-xs-4" style="margin-top: 5px;">';
				if($details > 0){
					echo $this->Html->link('Download ( '.$details.' Found ) ', array('controller' => 'students', 'action' => 'registrareport', 'ext' => 'pdf', $this->data['Student']['gradelevel_id'], $this->data['Student']['gradesection_id'], $this->data['Student']['schoolyear_id'], date('YmdhiA')), array('class' => 'btn btn-primary', 'target' => '_blank'));
				}else{
					echo $this->Html->link('Download ( '.$details.' Found ) ', '#', array('class' => 'btn btn-danger', 'disabled' => true));
				}
			echo '</div>';
		}
	?>
	<div class="clear"></div>
	<br /><br />
</div>
</div>
<div class="clear"></div>
<br />
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradelevel_id', 'update_id' => '#gradesection_id', 'controller_id' => 'gradesections', 'action_id' => 'getsectionlists', 'method' => 'post', 'form' => 'Student')); ?>
