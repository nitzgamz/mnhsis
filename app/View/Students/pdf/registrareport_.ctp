<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
	<title>School Form 1</title>
	<meta name="dompdf.view" content="Fit"/>
	<meta name="Author" content="S&amp;S Enterprises"/>
	<meta name="description" content="Wish List Quote Request" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style>
		html, body{margin: 30px 30px;}		
		/*table {page-break-before: always; page-break-after: always;}*/
		@page{
			margin-top: 5px;
			margin-left: 20px;
			margin-bottom: 5px;
			margin-right: 20px;
		}
		table{border-collapse:collapse;}
		table,tbody,tr,th,td{margin:0;padding:0;}
		/*th,td{white-space:nowrap;}*/
		 #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; background-color: lightblue; }
    #footer .page:after { content: counter(page, upper-roman); }
	</style>
<body>	
<div id="header">
    <h1>ibmphp.blogspot.com</h1>
  </div>
  <div id="footer">
    
  </div>
<script type="text/php">
        if ( isset($pdf) ) {
            $font = Font_Metrics::get_font("helvetica", "bold");
            $pdf->page_text(30, 18, "Page {PAGE_NUM} out of {PAGE_COUNT}", $font, 8, array(255,0,0));

        }
    </script>
<h2 style="text-align: center; font: bold 18px arial, heveltica, sans-serif; padding-top: 0; margin-top: 0;"">
	School Form 1 ( SF 1 ) School Register
	<div style="font-size: 10px; font-style: italic;">( This replaced  Form 1, Master List & STS Form 2-Family Background and Profile )</div>
</h2>	


<table style="font: bold 12px arial, heveltica, sans-serif;" cellpadding="3" cellspacing="2" width="100%">
	
	<tr>
		<td rowspan="4" style="width: 150px; vertical-align: top;">
			<img src="<?php echo APP; ?>/webroot/img/1.png" style="width: 100%;"/>
		</td>
		<td style="text-align: right;">Region</td><td style="border: 1px solid #666; text-align: left;">CARAGA</td>
		<td style="text-align: right;">Division</td><td style="border: 1px solid #666; text-align: left;">BISLIG CITY</td>
		<td style="text-align: right;">District</td><td style="border: 1px solid #666; text-align: left;">BISLIG I</td>
		<td rowspan="4" style="width: 240px; vertical-align: top;">
			<img src="<?php echo APP; ?>/webroot/img/2.png" style="width: 100%;"/>
		</td>
	</tr>
	<tr>
		<td style="text-align: right;">School ID</td><td style="border: 1px solid #666;">132609</td>
		<td style="text-align: right;">School Year</td><td style="border: 1px solid #666;"><?php echo $sy['Schoolyear']['name']; ?></td>
		<td style="text-align: right;">Grade Level</td><td style="border: 1px solid #666; text-transform: uppercase;"><?php echo $gradelevel['Gradelevel']['name']; ?></td>	
	</tr>
	<tr>
		<td style="text-align: right;">School Name</td><td colspan="3" style="border: 1px solid #666;">BISLIG CENTRAL ELEMENTARY SCHOOL</td>
		<td style="text-align: right;">Section</td><td  style="border: 1px solid #666; text-transform: uppercase;"><?php echo $section['Gradesection']['name']; ?></td>
	</tr>
	
	
</table>

<div style="clear: both;"></div>
<div style="">
<table style="font: bold 12px arial, heveltica, sans-serif;" cellpadding="2" cellspacing="0" width="100%">
	<thead>
	<tr style="text-align: center; font-size: 10px;">
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>
		<td style="border: 1px solid #000;" colspan="4">ADDRESS</td>
		<td style="border: 1px solid #000;" colspan="2">NAME OF PARENTS</td>
		<td style="border: 1px solid #000;" colspan="2">GUARDIAN ( If not parent )</td>		
		<td style="border: 1px solid #000; border-bottom: 0;">&nbsp;</td>		
		<td style="border: 1px solid #000;">REMARKS</td>		
	</tr>
	
	<tr style="text-align: center; font-size: 10px;">
		<td style="border: 1px solid #000; border-top: 0;">LRN</td>
		<td style="border: 1px solid #000; border-top: 0;">NAME <br /> ( Last Name, First Name, Middle Name )</td>
		<td style="border: 1px solid #000; border-top: 0;">SEX (M/F) </td>
		<td style="border: 1px solid #000; border-top: 0;">BIRTHDATE <br /> (dd/dd/yy) </td>
		<td style="border: 1px solid #000; border-top: 0;">AGE as of 1st Friday of June <hr> (nos. of years as per last birthday) </td>
		<td style="border: 1px solid #000; border-top: 0;">BIRTHPALCE <br /> (Province)</td>
		<td style="border: 1px solid #000; border-top: 0;">IP <br /> (Specify Ethic Group)</td>
		<td style="border: 1px solid #000; border-top: 0;">RELIGION</td>
		<td style="border: 1px solid #000;">House # / Street / Sitio/ Purok</td>
		<td style="border: 1px solid #000;">Barangay</td>
		<td style="border: 1px solid #000;">Municipality / City</td>
		<td style="border: 1px solid #000;">Province</td>
		<td style="border: 1px solid #000;">Father <br /> (1st name only if family name identical to learner) </td>
		<td style="border: 1px solid #000;">Mother <br /> (Maiden: 1st Name, Middle & Last Name) </td>
		<td style="border: 1px solid #000;">Name</td>
		<td style="border: 1px solid #000;">Relationship</td>
		<td style="border: 1px solid #000; border-top: 0;">Contact Number (Parent /Guardian)</td>
		<td style="border: 1px solid #000;">(Please refer to the legend on last page)</td>
	</tr>	
	</thead>
	
	<tbody style="page-break-before: always;">	
	<?php $i=0; ?>
	<?php $male=0; ?>
	<?php $female=0; ?>
	<?php if(!empty($students)) { ?>
		<?php foreach($students as $student): ?>
		<?php $i++; ?>			
			<tr style="page-break-before: always;">
				<td style="border-bottom: 1px solid #000; border-left: 1px solid #000;"><?php echo $student['Student']['lrn']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['lastname'].', <br /> '.$student['Student']['firstname'].' '.$student['Student']['middlename']; ?></td>
				<td style="border-bottom: 1px solid #000; text-align: center;"><?php echo $student['Student']['gender']; ?></td>
				<td style="border-bottom: 1px solid #000; text-align: center;"><?php echo date('m-d-y', strtotime($student['Student']['birthdate'])); ?></td>
				<td style="border-bottom: 1px solid #000; text-align: center;"><?php echo floor((time() - strtotime($student['Student']['birthdate'])) / 31556926); ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['birthplace']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['ethnic_group']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['religion']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['address_house']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['address_brgy']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['address_municipal']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['address_province']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['father_fullname']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['mother_fullname']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['guardian_fullname']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['relationship']; ?></td>
				<td style="border-bottom: 1px solid #000;"><?php echo $student['Student']['contact_number']; ?></td>
				<td style="border-bottom: 1px solid #000; text-align: center; border-right: 1px solid #000;"><?php echo $student['Studentindicator']['code']; ?></td>
			</tr>
			
		<?php
			if($student['Student']['gender']=="M"){	
				$male++;
			}
			
			if($student['Student']['gender']=="F"){	
				$female++;
			}
		?>
		<?php endforeach; ?>
	<?php } ?>
	
	</tbody>
</table>
</div>
<div style="page-break-before:always;">
<table style="font: bold 12px arial, heveltica, sans-serif; margin-top: 10px;" cellpadding="2" cellspacing="0" width="100%">
	<tr style="text-align: center; font-size: 10px;">
		<td style="text-align: left;" colspan="18">&nbsp;</td>
	</tr>
	<tr style="text-align: center; font-size: 10px;">
		<td style="text-align: left;" colspan="18">List and code of Indicators under REMARK column</td>
	</tr>
	<tr style="text-align: center; font-size: 10px;">
		<td style="text-align: left;">Indicator</td>	
		<td>Code</td>	
		<td colspan="2" style="text-align: left;">Required Information</td>
		<td style="text-align: left;">Indicator</td>	
		<td>Code</td>	
		<td colspan="2" style="text-align: left;">Required Information</td>	
		<td>&nbsp;</td>	
		<td>&nbsp;</td>		
		
		<td style="border: 1px solid #000;">&nbsp;</td>
		<td style="border: 1px solid #000;">EoSY</td>
		<td style="border: 1px solid #000;" colspan="3">Prepared By:</td>
	
		<td style="border: 1px solid #000;" colspan="3">Certified Correct:</td>
	</tr>
	
	<tr style="text-align: center; font-size: 10px;">
		<td style="text-align: left;">Transferred Out</td>	
		<td>T/O</td>	
		<td colspan="2" style="text-align: left;">Name of  Public (P) Private (PR) School  & Effectivity Date</td>
		<td style="text-align: left;">CCT Recipient</td>	
		<td>CCT</td>	
		<td colspan="2" style="text-align: left;">CCT Control/reference number & Effectivity Date</td>	
		<td>&nbsp;</td>	
		<td>&nbsp;</td>		
		
		<td style="border: 1px solid #000;">MALE</td>
		<td style="border: 1px solid #000;"><?php echo $male; ?></td>
		<td style="border: 1px solid #000;" colspan="3">&nbsp;</td>

		<td style="border: 1px solid #000;" colspan="3">&nbsp;</td>
	</tr>
	
	<tr style="text-align: center; font-size: 10px;">
		<td style="text-align: left;">Transferred IN</td>	
		<td>T/I</td>	
		<td colspan="2" style="text-align: left;">Name of  Public (P) Private (PR) School  & Effectivity Date</td>
		<td style="text-align: left;">Balik-Aral</td>	
		<td>B/A</td>	
		<td colspan="2" style="text-align: left;">Name of school last attended & Year</td>	
		<td>&nbsp;</td>	
		<td>&nbsp;</td>		
		
		<td style="border: 1px solid #000;">FEMALE</td>
		<td style="border: 1px solid #000;"><?php echo $female; ?></td>
		<td style="border: 1px solid #000;" colspan="3">(Signature of Adviser over Printed Name)</td>
		
		<td style="border: 1px solid #000;" colspan="3">(Signature of School Head over Printed <br /> Name)</td>
	</tr>
	
	<tr style="text-align: center; font-size: 10px;">
		<td style="text-align: left;">Dropped</td>	
		<td>DRP</td>	
		<td colspan="2" style="text-align: left;">Reason  and Effectivity Date</td>
		<td style="text-align: left;">Learner With Dissability</td>	
		<td>LWD</td>	
		<td colspan="2" style="text-align: left;">Specify</td>	
		<td>&nbsp;</td>	
		<td>&nbsp;</td>		
		
		<td style="border: 1px solid #000;">TOTAL</td>
		<td style="border: 1px solid #000;"><?php echo $male + $female; ?></td>
		<td style="border: 1px solid #000;" colspan="3">BoSY Date:   EoSYDate:</td>		
		<td style="border: 1px solid #000;" colspan="3">BoSY Date:   EoSYDate:</td>
	</tr>
	
	<tr style="text-align: center; font-size: 10px;">
		<td style="text-align: left;">Late Enrollment</td>	
		<td>LE</td>	
		<td colspan="2" style="text-align: left;">Reason (Enrollment beyond 1st Friday of June)</td>
		<td style="text-align: left;">Accelarated</td>	
		<td>ACL</td>	
		<td colspan="2" style="text-align: left;">Specify Level & Effectivity Data</td>	
		<td>&nbsp;</td>	
		<td>&nbsp;</td>		
		<td>&nbsp;</td>	
		<td>&nbsp;</td>		
		<td>&nbsp;</td>	
		<td>&nbsp;</td>					
		<td>&nbsp;</td>	
		<td>&nbsp;</td>		
		<td>&nbsp;</td>	
		<td>&nbsp;</td>		
	</tr>
	
</table>
</div>
</body>
</html>