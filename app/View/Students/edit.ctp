<?php echo $this->element('submenu/student'); ?>
<div class="students form">
<?php echo $this->Form->create('Student');?>
	<?php
		echo '<div class="col-md-4 no-padding-left">';
		echo '<div class="panel panel-success col-md-12 nopadding">';
			echo '<div class="panel-heading">Student Information</div>';
			echo '<div class="panel-body">';
			echo '<label for="">LRN</label>';
			echo $this->Form->input('id');
			echo $this->Form->input('lrn', array('label' => ''));

				echo '<div class="col-md-6 nopadding-left">';
				
						echo $this->Form->input('schoolyear_id', array('label' => 'S.Y.', 'empty' =>  false, 'disabled' => true));	
				echo '</div>';
				
				echo '<div class="col-md-6 nopadding-right">';
				echo '<label for="">Grade Level</label>';
					echo $this->Form->input('gradelevel_id', array('label' => false, 'empty' => $group==1 ? '-- Choose --' : false, 'id' => 'gradelevel_id', 'disabled' => true));
				echo '</div>';
				echo '<div class="col-md-6 nopadding-left">';
					echo '<label for="">Section</label>';
					echo $this->Form->input('gradesection_id', array('label' => false, 'empty' =>  $group==1 ? '-- Choose --' : false, 'id' => 'gradesection_id', 'disabled' => true));
				echo '</div>';
				
				echo '<div class="col-md-6 nopadding-right">';
					
					echo $this->Form->input('teacher_id', array('label' => 'Assign Teacher', 'empty' =>  $group==1 ? '-- Choose --' : false, 'id' => 'teacher_id', 'disabled' => true));
				echo '</div>';
			
				echo '<div class="col-md-12 nopadding">';
					echo '<label for="">Entrance Date</label>';
					echo $this->Form->input('date_of_entrance', array('label' => false), array('class' => 'birthdate'));
				echo '</div>';
				echo $this->Form->input('gender', array('type' => 'select', 'options' => array('M' => 'M', 'F' => 'F'), 'empty' => false, 'label' => 'Sex', 'div' => 'col-md-12 nopadding'));
		

			echo $this->Form->input('firstname', array('label' => 'First Name', 'escape' => false, 'div' => 'col-md-12 nopadding'));
			echo $this->Form->input('middlename', array('label' => 'Middle Name', 'escape' => false, 'div' => 'col-md-12 nopadding'));
			echo $this->Form->input('lastname', array('label' => 'Last Name', 'escape' => false, 'div' => 'col-md-12 nopadding'));
			echo '<label for="">Date of Birth</label>';
			echo $this->Form->input('birthdate', array('label' => false, 'class' =>'birthdate', 'escape' => false));

			
			echo '</div>';
			echo '</div>';
		echo '</div>';
		
		
		echo '<div class="col-md-3 nopadding-left">';	
		
		echo '<div class="panel panel-success col-md-12 nopadding">';
			echo '<div class="panel-heading">Other Information</div>';
			echo '<div class="panel-body">';
			
				
					
				echo '<label for="">Birthplace</label>';
				echo $this->Form->input('birthplace', array('label' => ''));			
				
				echo '<label for="">Mother Tongue</label>';
				echo $this->Form->input('mother_tongue', array('label' => ''));
				echo '<label for="">IP (Specify Ethnic Group)</label>';
				echo $this->Form->input('ethnic_group', array('label' => ''));
				echo '<label for="">Religion</label>';
				echo $this->Form->input('religion', array('label' => ''));
				echo '<label for="">House # / Street/Sitio/ Purok</label>';
				echo $this->Form->input('address_house', array('label' => ''));
				//echo '<label for="">Barangay</label>';
				//echo $this->Form->input('address_brgy', array('label' => ''));
				//echo '<label for="">Municipality/ City </label>';
				//echo $this->Form->input('address_municipal', array('label' => ''));
				//echo '<label for="">Province</label>';
				echo $this->Form->input('province_id', array('label' => 'Province', 'id' => 'province_id'));
				echo $this->Form->input('city_id', array('label' => 'City / Municipality', 'id' => 'city_id'));
				echo $this->Form->input('barangay_id', array('label' => 'Barangay', 'id' => 'barangay_id'));
		echo '</div>';
			echo '</div>';
	echo '</div>';
		
		echo '<div class="col-md-4 nopadding-right">';
		
		echo '<div class="panel panel-success col-md-12 nopadding">';
			echo '<div class="panel-heading">Parent / Guardian Information</div>';
			echo '<div class="panel-body">';
			
		echo '<label for="">Father Name <br /> (1st name only if family name identical to learner)</label>';
		echo $this->Form->input('father_fullname', array('label' => ''));
		echo '<label for="">Mother Name <br /> (Maiden: 1st Name, Middle & Last Name)</label>';
		echo $this->Form->input('mother_fullname', array('label' => ''));
		echo '<label for="">Guardian Name</label>';
		echo $this->Form->input('guardian_fullname', array('label' => ''));
		
		echo '<div class="col-md-6 nopadding-left">';	
			echo '<label for="">Relationship</label>';
			echo $this->Form->input('relationship', array('label' => ''));
		echo '</div>';
		
		echo '<div class="col-md-6 nopadding-right">';	
			echo '<label for="">Contact Number</label>';
			echo $this->Form->input('contact_number', array('label' => ''));
		echo '</div>';		
		
		echo '<label for="">Address</label>';
		echo $this->Form->input('guardian_address', array('label' => ''));
		
		echo '<div class="col-md-6 nopadding-left">';	
			echo '<label for="">Occupation</label>';
			echo $this->Form->input('guardian_occupation', array('label' => ''));
		echo '</div>';
		
		echo '<div class="col-md-6 nopadding-right">';	
			echo '<label for="">Remarks</label>';
			echo $this->Form->input('studentindicator_id', array('label' => ''));	
		echo '</div>';
		
		
		echo $this->Form->input('added', array('default' => date('Y-m-d'), 'type' => 'hidden', 'label' => ''));	
		echo $this->Form->input('added_by', array('default' => $userid, 'type' => 'hidden', 'label' => ''));
		echo '<div class="clear separator20"></div>';
		if(!$view){
			echo $this->Form->submit(__('Save Changes'), array('class' => 'btn btn-primary btn-block'));
		}else{
			$this->App->linkbtn('Make Changes', 'students', 'edit', $this->data['Student']['id']);
		}
		echo '</div>';
		echo '</div>';
		echo '</div>';
	?>
	<div class="clear separator20"></div>

</div>

<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradelevel_id', 'update_id' => '#gradesection_id', 'controller_id' => 'gradesections', 'action_id' => 'getsectionlists', 'method' => 'post', 'form' => 'Student')); ?>
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradesection_id', 'update_id' => '#teacher_id', 'controller_id' => 'teachers', 'action_id' => 'listofteachers', 'method' => 'post', 'form' => 'Student')); ?>
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#province_id', 'update_id' => '#city_id', 'controller_id' => 'cities', 'action_id' => 'listofcities', 'method' => 'post', 'form' => 'Student')); ?>
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#city_id', 'update_id' => '#barangay_id', 'controller_id' => 'barangays', 'action_id' => 'listofbarangays', 'method' => 'post', 'form' => 'Student')); ?>
