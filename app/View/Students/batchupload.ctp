<?php echo $this->element('submenu/student'); ?>
<div class="students form">
<?php echo $this->Form->create('Student', array('enctype' => 'multipart/form-data'));?>	
	<?php
		echo '<div class="col-md-4 col-lg-4 col-xs-4 no-padding-left">';
		//echo '<label for="">School Year</label>';		
		//echo $this->Form->input('schoolyear', array('type' => 'date', 'dateFormat' => 'Y', 'label' => '', 'minYear' => date('Y')-10, 'maxYear' => date('Y')+1));	
		/*echo '<select name="data[Student][schoolyear]" class="normalinput2">';
			$schoolyears = array();
			for($i=date('Y')-10; $i<=date('Y')+1; $i++){			
				$schoolyears[] = $i;
			}
				foreach($schoolyears as $sy):
					echo '<option value="'.$sy.'">'.$sy.' - '.($sy + 1).'</option>';
				endforeach;
				echo '</select>';*/
		echo $this->Form->input('schoolyear_id', array('label' => 'S.Y.', 'empty' =>  false));	
		echo $this->Form->hidden('schoolyear', array('label' => 'S.Y.', 'default' =>  date('Y')));	
		echo '<label for="">Grade Level</label>';
		echo $this->Form->input('gradelevel_id', array('label' => '', 'empty' => '-- Choose --', 'id' => 'gradelevel_id'));
		echo '<label for="">Section</label>';
		echo $this->Form->input('gradesection_id', array('label' => '', 'empty' => '-- Choose --', 'id' => 'gradesection_id'));
		echo $this->Form->input('teacher_id', array('label' => 'Select Teacher', 'empty' =>  '-- Choose --', 'id' => 'teacher_id'));
		
		echo '</div>';
		echo '<div class="col-md-4 col-lg-4 col-xs-4">';
		//echo '<label for="">Select Teacher</label>';
		//echo $this->Form->input('teacher_id', array('label' => '', 'class' => ''));			
		echo '<label for="">Select file to upload ( xls, xlsx )</label>';
		echo $this->Form->file('filetoupload', array('label' => 'Select file to upload'));			
		echo '<div class="clear separator20"></div>';
		echo $this->Form->submit('Upload & Continue', array('class' => 'btn btn-primary'));
		echo '</div>';
		echo '<div class="clear"></div>';
	?>	
	<br /><br />
</div>

<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradelevel_id', 'update_id' => '#gradesection_id', 'controller_id' => 'gradesections', 'action_id' => 'getsectionlists', 'method' => 'post', 'form' => 'Student')); ?>
<?php echo $this->element('js/form.dropdown.script', array('event_id' => '#gradesection_id', 'update_id' => '#teacher_id', 'controller_id' => 'teachers', 'action_id' => 'listofteachers', 'method' => 'post', 'form' => 'Student')); ?>