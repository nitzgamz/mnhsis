<div class="col-md-11 nopadding">
	<table class="table table-condensed table-striped" id="dtable">
		<thead>
			<tr>
				<?php $headers = array('LRN', 'Lastname', 'Firstname', 'Middlename', 'S.Y.', 'Grade Level', 'Section', 'Adviser', 'Form - 138', 'Form - 137'); ?>
				<?php echo $this->element('table-header', array('headers' => $headers, 'disable_sort' => array())); ?>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($students)): ?>
				<?php foreach($students as $st): ?>
					<?php $reportCard = $this->App->StudentReportCard($st['Student']['id'], $st['Student']['gradelevel_id'], $teacher); ?>
					<?php $permanentCard = $this->App->StudentPermanentCard($st['Student']['id'], $st['Student']['gradelevel_id'], $teacher); ?>
					<tr>
						<td><?php echo $st['Student']['lrn']; ?></td>
						<td><?php echo $st['Student']['lastname']; ?></td>
						<td><?php echo $st['Student']['firstname']; ?></td>
						<td><?php echo $st['Student']['middlename']; ?></td>
						<td><?php echo $st['Schoolyear']['name']; ?></td>
						<td><?php echo $st['Gradelevel']['name']; ?></td>
						<td><?php echo $st['Gradesection']['name']; ?></td>
						<td><?php echo $st['Teacher']['name']; ?></td>
						<td class="actions1">
							<?php 
								
								if($reportCard > 0){
									//$this->App->modal_link_a('Report Card', 'Student Report Card', 'attendances', 'download_report', $st['Student']['id']); 
									
									$this->App->modal_link('Download', 'select_sy_rcard', 'download_rcard', $st['Student']['id']); 
									
									$this->App->modal_link_a('Edit', 'Update Student Permanent Record ( Form 137 )', 'periodicratings', 'edit', $st['Student']['id'], $st['Student']['gradelevel_id']); 
								}else{
									//if($group > 1){ 
									$this->App->modal_link_a('Add', 'Student Report Card ( Form 138 )', 'attendances', 'add', $st['Student']['id'], $st['Schoolyear']['id']); 
									//echo $this->App->linkbtn('Report Card', 'attendances', 'add', $st['Student']['id'], $st['Schoolyear']['id']); 
									//}
								}
							?> 
						</td>
						<td class="actions1">
							<?php 
								if($permanentCard > 0){
									$this->App->modal_link('Download', 'select_sy_pcard', 'download_pcard', $st['Student']['id']);
									$this->App->modal_link_a('Edit', '( Form 137 ) | '.$st['Student']['full_name'].' &raquo; '.$st['Gradelevel']['name'].' &raquo; '.$st['Gradesection']['name'], 'periodicratings', 'edit', $st['Student']['id'], $st['Student']['gradelevel_id']); 
								}else{
									//if($group > 1){ 
									$this->App->modal_link_a('Add', 'Student Permanent Record ( Form 137 )', 'periodicratings', 'add', $st['Student']['id'], $st['Schoolyear']['id']); 
									
									//echo $this->App->linkbtn('Permanent Record', 'periodicratings', 'add', $st['Student']['id'], $st['Schoolyear']['id']); 
									//}
								}
							?> 
						</td>
						
						<!--td><?php echo $st['Schoolyear']['sy_from'].' - '.$st['Schoolyear']['sy_to']; ?></td-->
						
						
						<td>
							<?php //echo  $this->Html->link('Edit', array('controller' => '', 'action' => $view, $param1, $param2), array('class' => 'btn btn-xs btn-primary', 'escape' => false)); ?>
							<?php echo $this->App->linkbtn('Edit', 'students', 'edit', $st['Student']['id']); ?> 
							<?php echo $this->App->linkbtn('View', 'students', 'edit', $st['Student']['id'], 1); ?> 
							<?php echo $this->App->linkbtn('Enroll to Another Grade', 'students', 'newenroll', $st['Student']['id']); ?> 
							
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
</div>

	
	

<!---Select S.Y. Form-->
		<div class="modal fade select_sy_pcard" tabindex="-1" id="addnewform" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
			  <div class="modal-dialog modal-xs" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="gridSystemModalLabel"></h4>
					</div>
					<div class="modal-body col-md-12 center">
						<?php echo $this->Form->create('Periodicrating', array('method' => 'post', 'class' => 'formgenpcard')); ?>
						<?php echo $this->Form->hidden('studentid', array('id' => 'student_idpcard')); ?>
						<?php //echo $this->Form->input('sy', array('label' => false, 'type' => 'select', 'options' => $sy, 'class' => 'form-control input-lg')); ?>
						
							<?php echo $this->App->formbtn('Generate Report', 'lg', 'success btn-block btngenpcard'); ?>
							<a  href="<?php echo $this->webroot; ?>app/webroot/files/form-137.xls" class=" btn btn-primary btn-xs downloadpcard" download disabled><span class="glyphicon glyphicon-download"></span> Download File (.xls)</a>
						
						<div class="clear separator20"></div>
					</div>
					<div class="clear"></div>
				</div>
			  </div>
			</div>

	<!---Load Content-->
	

<!---Select S.Y. Form-->
	<div class="modal fade select_sy_rcard" tabindex="-1" id="addnewform" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" data-keyboard="false">
			  <div class="modal-dialog modal-xs" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="gridSystemModalLabel"></h4>
					</div>
					<div class="modal-body col-md-12 center">
						<?php echo $this->Form->create('Attendance', array('method' => 'post', 'class' => 'formgenrcard')); ?>
						<?php echo $this->Form->hidden('studentid', array('id' => 'student_idrcard')); ?>
						<?php echo $this->Form->input('sy', array('label' => false, 'type' => 'select', 'options' => $sy, 'class' => 'form-control input-lg', 'empty' => '-- Select S.Y. --')); ?>
						
							<?php echo $this->App->formbtn('Generate Report', 'lg', 'success btn-block btngenrcard'); ?>
							<a  href="<?php echo $this->webroot; ?>app/webroot/files/form-138.xls" class="btn btn-primary btn-xs downloadrcard" download disabled><span class="glyphicon glyphicon-download"></span> Download File (.xls)</a>
						
						<div class="clear separator20"></div>
					</div>
					<div class="clear"></div>
				</div>
			  </div>
			</div>

	<!---Load Content-->
	
	<?php
	$this->Js->Buffer('
		$(document).ready( function(){
		
		$("#dtable").on( "click", ".download_rcard, .download_pcard", function () {
			var param1 = $(this).attr("param1");
			$(".modal-title").html("");
			$("#student_idrcard").val(param1);
			$("#student_idpcard").val(param1);
		});
		
		/*$("#dtable").on( "click", ".download_pcard", function () {
			var param1 = $(this).attr("param1");
			$("#student_idpcard").val(param1);
		));*/
		
		/*
		$(".download_rcard").on("click", "function(){
			var param1 = $(this).attr("param1");
			//$(".select_sy_rcard").on("shown.bs.modal", function(){
				//$("#student_idrcard").val(param1);
			//});
			alert("asdasd");
		});*/
		
		/*$(".download_pcard").bind("click", function(){
			var param1 = $(this).attr("param1");
			$("#student_idpcard").val(param1);
		});*/
		
		
		$(".btngenrcard").bind("click", function(e){
			e.preventDefault();
			var btn = $(this);
			var formdata = $(".formgenrcard").serialize();
			
			$.ajax({
							type		: 	"POST",
							url			:  "'.$this->Html->url('/', true).'attendances/download_report",
							data		: formdata,
							dataType	: "html", 
							beforeSend	: function(){	
									btn.prop("disabled", true);
									btn.val("Generating Report....");
									$(".downloadrcard").attr("disabled", "");
							},
							success		: function(){
									$(".downloadrcard").removeAttr("disabled");
							},	
							error : function(){
								alert("Opps something is not right, please try again.");
							},
							complete	: function(){
									btn.prop("disabled", false);
									btn.val("Generate Report");
							}
						});
						
		});
		
		
		$(".btngenpcard").bind("click", function(e){
			e.preventDefault();
			var btn = $(this);
			var formdata = $(".formgenpcard").serialize();
			
			$.ajax({
							type		: 	"POST",
							url			:  "'.$this->Html->url('/', true).'periodicratings/download_report",
							data		: formdata,
							dataType	: "html", 
							beforeSend	: function(){	
									btn.prop("disabled", true);
									btn.val("Generating Report....");
									$(".downloadpcard").attr("disabled", "");
							},
							success		: function(){
									$(".downloadpcard").removeAttr("disabled");
							},	
							error : function(){
								alert("Opps something is not right, please try again.");
							},
							complete	: function(){
									btn.prop("disabled", false);
									btn.val("Generate Report");
							}
						});
						
		});
		});
		
		
	');

?>

