<div class="teachersubjects view">
<h2><?php  echo __('Teachersubject');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($teachersubject['Teachersubject']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Teacher'); ?></dt>
		<dd>
			<?php echo $this->Html->link($teachersubject['Teacher']['title'], array('controller' => 'teachers', 'action' => 'view', $teachersubject['Teacher']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Schoolsubject'); ?></dt>
		<dd>
			<?php echo $this->Html->link($teachersubject['Schoolsubject']['name'], array('controller' => 'schoolsubjects', 'action' => 'view', $teachersubject['Schoolsubject']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Teachersubject'), array('action' => 'edit', $teachersubject['Teachersubject']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Teachersubject'), array('action' => 'delete', $teachersubject['Teachersubject']['id']), null, __('Are you sure you want to delete # %s?', $teachersubject['Teachersubject']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Teachersubjects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Teachersubject'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolsubjects'), array('controller' => 'schoolsubjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolsubject'), array('controller' => 'schoolsubjects', 'action' => 'add')); ?> </li>
	</ul>
</div>
