<?php echo $this->element('submenu/teachers'); ?>
<div class="teachersubjects form">
<?php echo $this->Form->create('Teachersubject');?>
	<fieldset>
		<legend><?php echo __('Edit Teachersubject'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('teacher_id');
		echo $this->Form->input('schoolsubject_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Teachersubject.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Teachersubject.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Teachersubjects'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolsubjects'), array('controller' => 'schoolsubjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolsubject'), array('controller' => 'schoolsubjects', 'action' => 'add')); ?> </li>
	</ul>
</div>
