<?php echo $this->element('submenu/teachers'); ?>
<h3 class="default"><?php echo __('Tag / Assign Subject'); ?></h3>
<div class="teachersubjects form">
<?php echo $this->Form->create('Teachersubject');?>
	<fieldset>
		
	<?php
		echo '<div class="col-md-3 col-lg-3 col-xs-3 no-padding-left">';
		echo '<label for="">Teacher</label>';
		echo $this->Form->input('teacher_id', array('label' => ''));
		echo '<label for="">Select Subeject to assign</label>';
		echo $this->Form->input('schoolsubject_id', array('label' => ''));
		echo '</div>';
	?>
	<div class="clear"></div>
	<br />
<?php echo $this->Form->submit(__('Save Information'), array('class' => 'btn btn-primary'));?>
</div>
<br /><br />

