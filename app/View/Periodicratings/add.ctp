<div class="periodicratings form col-md-12 nopadding">
<?php echo $this->Form->create('Periodicrating'); ?>
	<?php
		echo $this->Form->input('student_id', array('label' => 'Student', 'class' => 'form-control input-lg', 'div' => 'col-md-6 nopadding-left'));
		echo $this->Form->input('schoolyear_id', array('label' => 'School Year', 'class' => 'form-control input-lg', 'div' => 'col-md-6 nopadding-right'));
		echo $this->Form->input('gradelevel_id', array('label' => 'Grade Level', 'class' => 'form-control input-lg', 'div' => 'col-md-6 nopadding-left'));
		
		echo $this->Form->input('gradesection_id', array('label' => 'Section', 'class' => 'form-control input-lg', 'div' => 'col-md-6 nopadding-right'));
		echo $this->Form->hidden('teacher_id');
	?>
	<table class="table table-condensed smalltable table-stripped">
		<thead>
			<tr>
				<th class="center">Subjects</th>
				<th class="center">PERIODIC RATINGS <br /> Grading Plan Use </br >Averaging / Cumulative</th>
				<th class="center">FINAL <br /> Rating</th>
				<th class="center">ACTION <br /> TAKEN</th>
				<th class="center">UNITS <br /> EARNED</th>
			</tr>
		</thead>
		<thead>
			<tr>
				<th class="center">&nbsp;</th>
				<th class="center">
					<div class="col-md-12 nopadding">
					<div class="col-md-3 center">1</div>
					<div class="col-md-3 center">2</div>
					<div class="col-md-3 center">3</div>
					<div class="col-md-3 center">4</div>
					</div>
				</th>
				<th class="center" colspan="3">&nbsp;</th>
			</tr>
		</thead>
	<?php $i=0; ?>
	<?php foreach($subjects as $sb): ?>
	<?php $i++; ?>
		<?php if($i > 13): ?>
			<?php if($glevel['Gradelevel']['id']==1 || $glevel['Gradelevel']['name']=="Grade 7"): ?>
				<?php echo $this->element('periodicratings', array('sb' => $sb)); ?>
			<?php endif; ?>
		<?php else: ?>
				<?php echo $this->element('periodicratings', array('sb' => $sb)); ?>
		<?php endif; ?>
	<?php endforeach; ?>
	</table>
<?php echo $this->App->formbtn('Save Information', 'btn-lg btn-block btn-primary'); ?>
</div>
<?php $this->App->clear(); ?>
