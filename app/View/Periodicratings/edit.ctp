<div class="periodicratings form col-md-12 nopadding">
<?php echo $this->Form->create('Periodicrating'); ?>
	<table class="table table-condensed smalltable table-stripped">
		<thead>
			<tr>
				<th class="center">Subjects</th>
				<th class="center">PERIODIC RATINGS <br /> Grading Plan Use </br >Averaging / Cumulative</th>
				<th class="center">FINAL <br /> Rating</th>
				<th class="center">ACTION <br /> TAKEN</th>
				<th class="center">UNITS <br /> EARNED</th>
			</tr>
		</thead>
		<thead>
			<tr>
				<th class="center">&nbsp;</th>
				<th class="center">
					<div class="col-md-12 nopadding">
					<div class="col-md-3 center">1</div>
					<div class="col-md-3 center">2</div>
					<div class="col-md-3 center">3</div>
					<div class="col-md-3 center">4</div>
					</div>
				</th>
				<th class="center" colspan="3">&nbsp;</th>
			</tr>
		</thead>
	<?php $i=0; ?>
	<?php foreach($datas as $sb): ?>
	<?php $i++; ?>
		<?php echo $this->element('periodicratings-edit', array('sb' => $sb)); ?>	
	<?php endforeach; ?>
	</table>
<?php echo $this->App->formbtn('Save Changes', 'btn-lg btn-block btn-primary'); ?>
</div>
<?php $this->App->clear(); ?>
