<div class="periodicratings view">
<h2><?php echo __('Periodicrating'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Student'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicrating['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $periodicrating['Student']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gradelevel'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicrating['Gradelevel']['name'], array('controller' => 'gradelevels', 'action' => 'view', $periodicrating['Gradelevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gradesection'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicrating['Gradesection']['name'], array('controller' => 'gradesections', 'action' => 'view', $periodicrating['Gradesection']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Schoolyear'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicrating['Schoolyear']['name'], array('controller' => 'schoolyears', 'action' => 'view', $periodicrating['Schoolyear']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Teacher'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicrating['Teacher']['name'], array('controller' => 'teachers', 'action' => 'view', $periodicrating['Teacher']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subject'); ?></dt>
		<dd>
			<?php echo $this->Html->link($periodicrating['Subject']['name'], array('controller' => 'subjects', 'action' => 'view', $periodicrating['Subject']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('1st Qtr'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['1st_qtr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('2nd Qtr'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['2nd_qtr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('3rd Qtr'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['3rd_qtr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('4th Qtr'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['4th_qtr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rating'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['rating']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['action']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Earned'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['earned']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Added'); ?></dt>
		<dd>
			<?php echo h($periodicrating['Periodicrating']['added']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Periodicrating'), array('action' => 'edit', $periodicrating['Periodicrating']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Periodicrating'), array('action' => 'delete', $periodicrating['Periodicrating']['id']), array(), __('Are you sure you want to delete # %s?', $periodicrating['Periodicrating']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Periodicratings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Periodicrating'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradelevels'), array('controller' => 'gradelevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradelevel'), array('controller' => 'gradelevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradesections'), array('controller' => 'gradesections', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradesection'), array('controller' => 'gradesections', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Schoolyears'), array('controller' => 'schoolyears', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Schoolyear'), array('controller' => 'schoolyears', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); ?> </li>
	</ul>
</div>
