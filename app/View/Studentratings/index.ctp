<div class="studentratings index">
	<h2><?php echo __('Studentratings');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('student_id');?></th>
			<th><?php echo $this->Paginator->sort('rating_id');?></th>
			<th><?php echo $this->Paginator->sort('gradelevel_id');?></th>
			<th><?php echo $this->Paginator->sort('gradesection_id');?></th>
			<th><?php echo $this->Paginator->sort('quarter');?></th>
			<th><?php echo $this->Paginator->sort('rating');?></th>
			<th><?php echo $this->Paginator->sort('added');?></th>
			<th><?php echo $this->Paginator->sort('sy');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($studentratings as $studentrating): ?>
	<tr>
		<td><?php echo h($studentrating['Studentrating']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($studentrating['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $studentrating['Student']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($studentrating['Rating']['name'], array('controller' => 'ratings', 'action' => 'view', $studentrating['Rating']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($studentrating['Gradelevel']['name'], array('controller' => 'gradelevels', 'action' => 'view', $studentrating['Gradelevel']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($studentrating['Gradesection']['name'], array('controller' => 'gradesections', 'action' => 'view', $studentrating['Gradesection']['id'])); ?>
		</td>
		<td><?php echo h($studentrating['Studentrating']['quarter']); ?>&nbsp;</td>
		<td><?php echo h($studentrating['Studentrating']['rating']); ?>&nbsp;</td>
		<td><?php echo h($studentrating['Studentrating']['added']); ?>&nbsp;</td>
		<td><?php echo h($studentrating['Studentrating']['sy']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $studentrating['Studentrating']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $studentrating['Studentrating']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $studentrating['Studentrating']['id']), null, __('Are you sure you want to delete # %s?', $studentrating['Studentrating']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Studentrating'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ratings'), array('controller' => 'ratings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rating'), array('controller' => 'ratings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradelevels'), array('controller' => 'gradelevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradelevel'), array('controller' => 'gradelevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradesections'), array('controller' => 'gradesections', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradesection'), array('controller' => 'gradesections', 'action' => 'add')); ?> </li>
	</ul>
</div>
