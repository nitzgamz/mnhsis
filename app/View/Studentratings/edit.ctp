<div class="studentratings form">
<?php echo $this->Form->create('Studentrating');?>
	<fieldset>
		<legend><?php echo __('Edit Studentrating'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('student_id');
		echo $this->Form->input('rating_id');
		echo $this->Form->input('gradelevel_id');
		echo $this->Form->input('gradesection_id');
		echo $this->Form->input('quarter');
		echo $this->Form->input('rating');
		echo $this->Form->input('added');
		echo $this->Form->input('sy');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Studentrating.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Studentrating.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Studentratings'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ratings'), array('controller' => 'ratings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rating'), array('controller' => 'ratings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradelevels'), array('controller' => 'gradelevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradelevel'), array('controller' => 'gradelevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradesections'), array('controller' => 'gradesections', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradesection'), array('controller' => 'gradesections', 'action' => 'add')); ?> </li>
	</ul>
</div>
