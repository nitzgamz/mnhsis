<div class="studentratings view">
<h2><?php  echo __('Studentrating');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($studentrating['Studentrating']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Student'); ?></dt>
		<dd>
			<?php echo $this->Html->link($studentrating['Student']['full_name'], array('controller' => 'students', 'action' => 'view', $studentrating['Student']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rating'); ?></dt>
		<dd>
			<?php echo $this->Html->link($studentrating['Rating']['name'], array('controller' => 'ratings', 'action' => 'view', $studentrating['Rating']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gradelevel'); ?></dt>
		<dd>
			<?php echo $this->Html->link($studentrating['Gradelevel']['name'], array('controller' => 'gradelevels', 'action' => 'view', $studentrating['Gradelevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gradesection'); ?></dt>
		<dd>
			<?php echo $this->Html->link($studentrating['Gradesection']['name'], array('controller' => 'gradesections', 'action' => 'view', $studentrating['Gradesection']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quarter'); ?></dt>
		<dd>
			<?php echo h($studentrating['Studentrating']['quarter']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rating'); ?></dt>
		<dd>
			<?php echo h($studentrating['Studentrating']['rating']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Added'); ?></dt>
		<dd>
			<?php echo h($studentrating['Studentrating']['added']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sy'); ?></dt>
		<dd>
			<?php echo h($studentrating['Studentrating']['sy']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Studentrating'), array('action' => 'edit', $studentrating['Studentrating']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Studentrating'), array('action' => 'delete', $studentrating['Studentrating']['id']), null, __('Are you sure you want to delete # %s?', $studentrating['Studentrating']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Studentratings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Studentrating'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ratings'), array('controller' => 'ratings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Rating'), array('controller' => 'ratings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradelevels'), array('controller' => 'gradelevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradelevel'), array('controller' => 'gradelevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Gradesections'), array('controller' => 'gradesections', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gradesection'), array('controller' => 'gradesections', 'action' => 'add')); ?> </li>
	</ul>
</div>
