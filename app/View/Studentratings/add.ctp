<?php echo $this->element('submenu/student'); ?>
<div class="col-md-5 col-lg-5 col-xs-5 " style="background: #ccc; border: 1px solid #ccc; border-radius: 10px 10px; margin-top: 30px;">	
<h3 class="default">Student Ratings for :<br /> <br />
	<div style="text-align: center; font-size: 20px; padding: 10px 0 20px 0;">
		<?php echo $student['Student']['firstname'].' '.$student['Student']['lastname']; ?><br />
		<div style="font-size: 14px;"><?php echo $student['Gradelevel']['name']; ?> ( <?php echo $student['Gradesection']['name']; ?> )</div>
		<div style="font-size: 14px;">S.Y. <?php echo $student['Studentschoolyear']['sy'].' - '.($student['Studentschoolyear']['sy']+1); ?> </div>
	</div>
</h3>
<div class="studentratings form">
<?php echo $this->Form->create('Studentrating');?>
	<fieldset>		
	<?php
		$qtr = array("1" => "1", "2" => "2", "3" => "3", "4" => "4");
		$str = array("1" => "1", "2" => "2", "3" => "3", "4" => "4");
		
		
		echo $this->Form->input('student_id', array('type' => 'hidden', 'default' => $student['Student']['id']));
		echo $this->Form->input('rating_id', array('label' => 'Rating Category'));
		echo $this->Form->input('gradelevel_id', array('type' => 'hidden', 'default' => $student['Studentschoolyear']['gradelevel_id']));
		echo $this->Form->input('sy', array('type' => 'hidden', 'default' => $student['Studentschoolyear']['sy']));
		echo $this->Form->input('gradesection_id', array('type' => 'hidden', 'default' => $student['Studentschoolyear']['gradesection_id']));
		echo $this->Form->input('quarter', array('options' => $qtr, 'class' => 'normalinput1', 'label' => 'Select Quarter'));
		echo $this->Form->input('stars', array('options' => $str, 'class' => 'normalinput1', 'label' => 'Rating Star'));				
	?>
	</fieldset>
	<br />
	<div class="col-md-4 no-padding-left">
		<?php echo $this->Form->submit(__('Save Information'), array('class' => 'btn btn-primary'));?>
	</div>
	<div class="col-md-3 no-padding-left" style="margin-top: 5px; margin-left: -20px;">
		<?php echo $this->Html->link('Candel', array('controller' => 'students', 'action' => 'view', $student['Student']['id'], $student['Studentschoolyear']['gradelevel_id'], $student['Studentschoolyear']['gradesection_id'], $student['Studentschoolyear']['sy']), array('class' => 'btn btn-primary')); ?>
	</div>
	<div class="clear"></div>
<br />
</div>
</div>
<div class="clear"></div>