$(document).ready(function() {
		$('.mainmenu > li').bind('mouseover', openSubMenu);
		$('.mainmenu > li').bind('mouseout', closeSubMenu);
		
		function openSubMenu() {
			$(this).css({'background' : 'transparent'});
			$(this).find('ul').css('visibility', 'visible');
		};
		
		function closeSubMenu() {
			$(this).css({'background' : 'f4f2f2'});			
			$(this).find('ul').css('visibility', 'hidden');	
		};
								
			
});

