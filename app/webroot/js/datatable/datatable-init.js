﻿jQuery(document).ready(function() {
	jQuery('#contents').DataTable({
		/* Disable initial sort */
        "bProcessing": true,
        "bServerSide": true,
        "bJQueryUI": true,
        "bStateSave": true,
		"aaSorting": [],
		 /*"paginate": false,
	    "sort": false*/
		 "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
	});
} );