<?php
App::uses('LoantablepaymentsController', 'Controller');

/**
 * LoantablepaymentsController Test Case
 *
 */
class LoantablepaymentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.loantablepayment',
		'app.loan',
		'app.firstinterest',
		'app.loanjoinedpayment',
		'app.secondinterest'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
