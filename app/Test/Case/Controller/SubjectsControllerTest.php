<?php
App::uses('SubjectsController', 'Controller');

/**
 * SubjectsController Test Case
 *
 */
class SubjectsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.subject',
		'app.periodicrating',
		'app.student',
		'app.studentindicator',
		'app.gradelevel',
		'app.teacher',
		'app.gradesection',
		'app.schoolgrade',
		'app.schoolsubject',
		'app.schoolyear',
		'app.mo',
		'app.user',
		'app.group',
		'app.studentrating',
		'app.rating',
		'app.otherdetail',
		'app.teachersubject',
		'app.studentschoolyear'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
