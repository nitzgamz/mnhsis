<?php
App::uses('AttendancesController', 'Controller');

/**
 * AttendancesController Test Case
 *
 */
class AttendancesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.attendance',
		'app.user',
		'app.group',
		'app.teacher',
		'app.gradelevel',
		'app.student',
		'app.studentindicator',
		'app.gradesection',
		'app.schoolgrade',
		'app.schoolsubject',
		'app.schoolyear',
		'app.studentrating',
		'app.rating',
		'app.otherdetail',
		'app.studentschoolyear',
		'app.teachersubject',
		'app.mos'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
