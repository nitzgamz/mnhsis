<?php
App::uses('Secondinterest', 'Model');

/**
 * Secondinterest Test Case
 *
 */
class SecondinterestTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.secondinterest',
		'app.loan',
		'app.firstinterest',
		'app.loanjoinedpayment',
		'app.loantablepayment'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Secondinterest = ClassRegistry::init('Secondinterest');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Secondinterest);

		parent::tearDown();
	}

}
