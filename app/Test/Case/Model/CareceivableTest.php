<?php
App::uses('Careceivable', 'Model');

/**
 * Careceivable Test Case
 *
 */
class CareceivableTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.careceivable'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Careceivable = ClassRegistry::init('Careceivable');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Careceivable);

		parent::tearDown();
	}

}
