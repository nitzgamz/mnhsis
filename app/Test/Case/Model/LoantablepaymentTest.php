<?php
App::uses('Loantablepayment', 'Model');

/**
 * Loantablepayment Test Case
 *
 */
class LoantablepaymentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.loantablepayment',
		'app.loan',
		'app.firstinterest',
		'app.loanjoinedpayment',
		'app.secondinterest'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Loantablepayment = ClassRegistry::init('Loantablepayment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Loantablepayment);

		parent::tearDown();
	}

}
