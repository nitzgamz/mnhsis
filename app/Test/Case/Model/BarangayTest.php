<?php
App::uses('Barangay', 'Model');

/**
 * Barangay Test Case
 *
 */
class BarangayTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.barangay',
		'app.province',
		'app.city'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Barangay = ClassRegistry::init('Barangay');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Barangay);

		parent::tearDown();
	}

}
