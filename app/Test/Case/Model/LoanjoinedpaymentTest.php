<?php
App::uses('Loanjoinedpayment', 'Model');

/**
 * Loanjoinedpayment Test Case
 *
 */
class LoanjoinedpaymentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.loanjoinedpayment',
		'app.loan'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Loanjoinedpayment = ClassRegistry::init('Loanjoinedpayment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Loanjoinedpayment);

		parent::tearDown();
	}

}
