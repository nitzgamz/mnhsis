<?php
App::uses('Saving', 'Model');

/**
 * Saving Test Case
 *
 */
class SavingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.saving'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Saving = ClassRegistry::init('Saving');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Saving);

		parent::tearDown();
	}

}
