<?php
App::uses('Livelihood', 'Model');

/**
 * Livelihood Test Case
 *
 */
class LivelihoodTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.livelihood'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Livelihood = ClassRegistry::init('Livelihood');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Livelihood);

		parent::tearDown();
	}

}
