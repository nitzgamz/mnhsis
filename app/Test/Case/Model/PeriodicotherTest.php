<?php
App::uses('Periodicother', 'Model');

/**
 * Periodicother Test Case
 *
 */
class PeriodicotherTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.periodicother',
		'app.student',
		'app.studentindicator',
		'app.gradelevel',
		'app.teacher',
		'app.gradesection',
		'app.schoolgrade',
		'app.schoolsubject',
		'app.schoolyear',
		'app.mo',
		'app.user',
		'app.group',
		'app.studentrating',
		'app.rating',
		'app.otherdetail',
		'app.teachersubject',
		'app.studentschoolyear'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Periodicother = ClassRegistry::init('Periodicother');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Periodicother);

		parent::tearDown();
	}

}
