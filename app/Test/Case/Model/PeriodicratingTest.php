<?php
App::uses('Periodicrating', 'Model');

/**
 * Periodicrating Test Case
 *
 */
class PeriodicratingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.periodicrating',
		'app.student',
		'app.studentindicator',
		'app.gradelevel',
		'app.teacher',
		'app.gradesection',
		'app.schoolgrade',
		'app.schoolsubject',
		'app.schoolyear',
		'app.mo',
		'app.user',
		'app.group',
		'app.studentrating',
		'app.rating',
		'app.otherdetail',
		'app.teachersubject',
		'app.studentschoolyear',
		'app.subject'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Periodicrating = ClassRegistry::init('Periodicrating');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Periodicrating);

		parent::tearDown();
	}

}
