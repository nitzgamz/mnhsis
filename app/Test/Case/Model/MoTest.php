<?php
App::uses('Mo', 'Model');

/**
 * Mo Test Case
 *
 */
class MoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mo',
		'app.schoolyear',
		'app.schoolgrade',
		'app.student',
		'app.studentindicator',
		'app.gradelevel',
		'app.teacher',
		'app.gradesection',
		'app.studentrating',
		'app.rating',
		'app.otherdetail',
		'app.user',
		'app.group',
		'app.teachersubject',
		'app.schoolsubject',
		'app.studentschoolyear'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mo = ClassRegistry::init('Mo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mo);

		parent::tearDown();
	}

}
