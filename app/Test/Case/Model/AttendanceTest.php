<?php
App::uses('Attendance', 'Model');

/**
 * Attendance Test Case
 *
 */
class AttendanceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.attendance',
		'app.user',
		'app.group',
		'app.teacher',
		'app.gradelevel',
		'app.student',
		'app.studentindicator',
		'app.gradesection',
		'app.schoolgrade',
		'app.schoolsubject',
		'app.schoolyear',
		'app.studentrating',
		'app.rating',
		'app.otherdetail',
		'app.studentschoolyear',
		'app.teachersubject',
		'app.mos'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Attendance = ClassRegistry::init('Attendance');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Attendance);

		parent::tearDown();
	}

}
