<?php
/**
 * AttendanceFixture
 *
 */
class AttendanceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'student_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'teacher_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'schoolyear_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'mos_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'present' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'total_present' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'absent' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'total_absent' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'added' => array('type' => 'date', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'student_id' => 1,
			'teacher_id' => 1,
			'schoolyear_id' => 1,
			'mos_id' => 1,
			'present' => 1,
			'total_present' => 1,
			'absent' => 1,
			'total_absent' => 1,
			'added' => '2017-01-12'
		),
	);

}
