<?php
/**
 * PeriodicotherFixture
 *
 */
class PeriodicotherFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'student_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'gradelevel_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'gradesection_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'schoolyear_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'teacher_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'advance_units' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 35, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'lacks_units' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 35, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'classified_as' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 35, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'added' => array('type' => 'date', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'student_id' => 1,
			'gradelevel_id' => 1,
			'gradesection_id' => 1,
			'schoolyear_id' => 1,
			'teacher_id' => 1,
			'advance_units' => 'Lorem ipsum dolor sit amet',
			'lacks_units' => 'Lorem ipsum dolor sit amet',
			'classified_as' => 'Lorem ipsum dolor sit amet',
			'added' => '2017-01-13'
		),
	);

}
