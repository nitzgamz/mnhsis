<?php
/**
 * PeriodicratingFixture
 *
 */
class PeriodicratingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'student_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'gradelevel_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'gradesection_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'schoolyear_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'teacher_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'subject_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'1st_qtr' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'2nd_qtr' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'3rd_qtr' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'4th_qtr' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'rating' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'action' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'earned' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'added' => array('type' => 'date', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'student_id' => 1,
			'gradelevel_id' => 1,
			'gradesection_id' => 1,
			'schoolyear_id' => 1,
			'teacher_id' => 1,
			'subject_id' => 1,
			'1st_qtr' => 'Lor',
			'2nd_qtr' => 'Lor',
			'3rd_qtr' => 'Lor',
			'4th_qtr' => 'Lor',
			'rating' => 'Lorem ipsum d',
			'action' => 'Lorem ipsum d',
			'earned' => 'Lorem ipsum d',
			'added' => '2017-01-13'
		),
	);

}
