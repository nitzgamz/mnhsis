<?php
/**
 * LoanjoinedpaymentFixture
 *
 */
class LoanjoinedpaymentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'member' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'loan_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 18, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'trans_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'prin_payment' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'int_payment' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'loan_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'loan_amt' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'term' => array('type' => 'integer', 'null' => false, 'default' => null),
		'loan_vchno' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'member' => 'Lorem ipsum d',
			'loan_id' => 'Lorem ipsum dolo',
			'trans_date' => '2014-02-27 18:44:23',
			'prin_payment' => 1,
			'int_payment' => 1,
			'loan_type' => 'Lorem ips',
			'loan_amt' => 1,
			'term' => 1,
			'loan_vchno' => 'Lorem ipsum d'
		),
	);

}
