<?php
/**
 * LoanFixture
 *
 */
class LoanFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'loan_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'member_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'loan_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'loan_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'loan_amt' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '12,2'),
		'term' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'loan_vchno' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'loan_id' => 'Lorem ipsum dolor sit a',
			'member_no' => 'Lorem ipsum d',
			'loan_date' => '2014-02-27 18:44:29',
			'loan_type' => 'Lorem ipsum d',
			'loan_amt' => 1,
			'term' => 'Lorem ips',
			'loan_vchno' => 'Lorem ipsum dolor sit a'
		),
	);

}
