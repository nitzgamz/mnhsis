<?php
/* Schoolgrade Fixture generated on: 2015-02-14 18:03:31 : 1423933411 */

/**
 * SchoolgradeFixture
 *
 */
class SchoolgradeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary', 'collate' => NULL, 'comment' => ''),
		'student_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'schoolsubject_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'first_quarter' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '12,2', 'collate' => NULL, 'comment' => ''),
		'second_quarter' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '12,2', 'collate' => NULL, 'comment' => ''),
		'third_quarter' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '12,2', 'collate' => NULL, 'comment' => ''),
		'fourth_quarter' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '12,2', 'collate' => NULL, 'comment' => ''),
		'quarter_tgrade' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '12,2', 'collate' => NULL, 'comment' => ''),
		'tgrade' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '12,2', 'collate' => NULL, 'comment' => ''),
		'ggrade' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '12,2', 'collate' => NULL, 'comment' => ''),
		'sy' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'charset' => 'latin1'),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'added' => array('type' => 'date', 'null' => true, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'student_id' => 1,
			'schoolsubject_id' => 1,
			'first_quarter' => 1,
			'second_quarter' => 1,
			'third_quarter' => 1,
			'fourth_quarter' => 1,
			'quarter_tgrade' => 1,
			'tgrade' => 1,
			'ggrade' => 1,
			'sy' => 'Lorem ipsum d',
			'user_id' => 1,
			'added' => '2015-02-14'
		),
	);
}
