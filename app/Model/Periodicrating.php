<?php
App::uses('AppModel', 'Model');
/**
 * Periodicrating Model
 *
 * @property Student $Student
 * @property Gradelevel $Gradelevel
 * @property Gradesection $Gradesection
 * @property Schoolyear $Schoolyear
 * @property Teacher $Teacher
 * @property Subject $Subject
 */
class Periodicrating extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	
	public function beforeSave(){
		parent::beforeSave();
		
		$this->data['Periodicrating']['added'] = date('Y-m-d');
		
		return true;
	}
	
	public function checkUnique($ignoredData, $fields, $or = true) {
        return $this->isUnique($fields, $or);
    }
	
	public $validate = array(
		'student_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
			'unique' => array(
				'rule' => array(
						'checkUnique', array(
								'student_id', 
								'schoolyear_id',
								'gradelevel_id'
						), false
				), 
				'message' => 'Periodic already added from the selected S.Y.'
			)
		)
	);
	
	
	public $belongsTo = array(
		'Student' => array(
			'className' => 'Student',
			'foreignKey' => 'student_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradelevel' => array(
			'className' => 'Gradelevel',
			'foreignKey' => 'gradelevel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradesection' => array(
			'className' => 'Gradesection',
			'foreignKey' => 'gradesection_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Schoolyear' => array(
			'className' => 'Schoolyear',
			'foreignKey' => 'schoolyear_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Teacher' => array(
			'className' => 'Teacher',
			'foreignKey' => 'teacher_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Subject' => array(
			'className' => 'Subject',
			'foreignKey' => 'subject_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
