<?php
App::uses('AppModel', 'Model');
/**
 * Student Model
 *
 * @property Studentindicator $Studentindicator
 * @property Gradelevel $Gradelevel
 * @property Schoolgrade $Schoolgrade
 */
class Student extends AppModel {
	
	public $inserted_ids = array();

    function afterSave($created) {
        if($created) {
            $this->inserted_ids[] = $this->getInsertID();
        }
        return true;
    }
	
	public $virtualFields = array('full_name' => 'CONCAT(Student.lastname, ", " ,Student.firstname, " ", Student.middlename)');
	public $displayField = 'full_name';
	
	
	public function beforeSave($options = array()){
		parent::beforeSave();
		
		if(isset($this->data['Student']['firstname']) && !empty($this->data['Student']['firstname'])){
		$this->data['Student']['firstname'] 	= strtoupper($this->data['Student']['firstname']);
		}
		
		if(isset($this->data['Student']['middlename']) && !empty($this->data['Student']['middlename'])){
		$this->data['Student']['middlename']	= strtoupper($this->data['Student']['middlename']);
		}
		
		if(isset($this->data['Student']['lastname']) && !empty($this->data['Student']['lastname'])){
		$this->data['Student']['lastname'] 		= strtoupper($this->data['Student']['lastname']);
		}
		
		return true;
	}
	
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
	public $validate = array(
		'lrn'	=> array(
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message'	=> 'LRN already taken.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),	
		),		
		'firstname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message's => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),			
		),
		'lastname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message's => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			
		),
		'middlename' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message's => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			
		),
		'birthdate' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message's => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			
		),
	);
		

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Studentindicator' => array(
			'className' => 'Studentindicator',
			'foreignKey' => 'studentindicator_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradelevel' => array(
			'className' => 'Gradelevel',
			'foreignKey' => 'gradelevel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradesection' => array(
			'className' => 'Gradesection',
			'foreignKey' => 'gradesection_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Teacher' => array(
			'className' => 'Teacher',
			'foreignKey' => 'teacher_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Schoolyear' => array(
			'className' => 'Schoolyear',
			'foreignKey' => 'schoolyear_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Province' => array(
			'className' => 'Province',
			'foreignKey' => 'province_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Barangay' => array(
			'className' => 'Barangay',
			'foreignKey' => 'barangay_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Schoolgrade' => array(
			'className' => 'Schoolgrade',
			'foreignKey' => 'student_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Attendance' => array(
			'className' => 'Attendance',
			'foreignKey' => 'student_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Studentschoolyear' => array(
			'className' => 'Studentschoolyear',
			'foreignKey' => 'student_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Studentrating' => array(
			'className' => 'Studentrating',
			'foreignKey' => 'student_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Otherdetail' => array(
			'className' => 'Otherdetail',
			'foreignKey' => 'student_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Periodicrating' => array(
			'className' => 'Periodicrating',
			'foreignKey' => 'student_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Periodicother' => array(
			'className' => 'Periodicother',
			'foreignKey' => 'student_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
