<?php
App::uses('AppModel', 'Model');
/**
 * Schoolsubject Model
 *
 * @property Schoolgrade $Schoolgrade
 */
class Schoolsubject extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
	public $validate = array(
		'name'	=> array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Subject already exists.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),			
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Subject already exists.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),	
		),
	);
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Schoolgrade' => array(
			'className' => 'Schoolgrade',
			'foreignKey' => 'schoolsubject_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
