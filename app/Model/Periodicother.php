<?php
App::uses('AppModel', 'Model');
/**
 * Periodicother Model
 *
 * @property Student $Student
 * @property Gradelevel $Gradelevel
 * @property Gradesection $Gradesection
 * @property Schoolyear $Schoolyear
 * @property Teacher $Teacher
 */
class Periodicother extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Student' => array(
			'className' => 'Student',
			'foreignKey' => 'student_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradelevel' => array(
			'className' => 'Gradelevel',
			'foreignKey' => 'gradelevel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Gradesection' => array(
			'className' => 'Gradesection',
			'foreignKey' => 'gradesection_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Schoolyear' => array(
			'className' => 'Schoolyear',
			'foreignKey' => 'schoolyear_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Teacher' => array(
			'className' => 'Teacher',
			'foreignKey' => 'teacher_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
